module.exports = {
  images: {
    domains: ['firebasestorage.googleapis.com'],
},
  devIndicators: {
    autoPrerender: false,
  },
  publicRuntimeConfig: {
    // Will be available on both server and client
    theme: "DEFAULT",
  },
};
