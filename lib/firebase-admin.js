const  admin = require("firebase-admin");
// const  "../../shirua2-firebase-adminsdk-k49l7-74bd6ba5ce.json";
const   serviceAccount = require("../shirua2-firebase-adminsdk-k49l7-74bd6ba5ce.json");


if(!admin.apps.length){
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),

      });
}
export default admin
