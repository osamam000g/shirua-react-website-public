/* eslint-disable react-hooks/exhaustive-deps */
import { Button, Divider, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React, { useCallback ,useState } from "react";
import { useDropzone } from "react-dropzone";
import { H5, Small } from "./Typography";

const DropZone = ({
  onChange,
  AcceptedFilesLen,
  maxFiles
}) => {
  const [errors, setErrors] = useState("");
console.log(AcceptedFilesLen , "AcceptedFilesLen")
  const onDrop = useCallback((acceptedFiles , fileRejections ) => {
    if (onChange){ 
    
    onChange(acceptedFiles.map((file)=>  Object.assign(file,{preview:URL.createObjectURL(file)})  ));
    fileRejections.forEach((file) => {
      file.errors.forEach((err) => {
        if (err.code === "file-too-large") {
          setErrors(`Error: Max Image size 600 KB`);
        }

        if (err.code === "file-invalid-type") {
          setErrors(`Error: ${err.message}`);
        } if(err.code === "too-many-files"){
          // console.log(err.code)
          setErrors(`Error: ${err.message} Max Images 5 Image`);
        }
      });
    });
                  }

  }, []);
  const {
    getRootProps,
    getInputProps,
    isDragActive
  } = useDropzone({
    onDrop,
    multiple: false,
    accept: ".jpeg,.jpg,.png,.gif",
    maxFiles: 4,
    maxSize:600000,
    disabled:AcceptedFilesLen < maxFiles ? false :true
  });
  return <Box display="flex" flexDirection="column" justifyContent="center" alignItems="center" minHeight="200px" border="1px dashed" borderColor="grey.400" borderRadius="10px" bgcolor={isDragActive ? "grey.200" : "none"} sx={{
    transition: "all 250ms ease-in-out",
    outline: "none"
  }}

   {...getRootProps()}>
      <input {...getInputProps()} />
      <H5 mb={2} color="grey.600">
        Drag & drop product image here
      </H5>

      <Divider sx={{
      width: "200px",
      mx: "auto"
    }} />

      <Typography color="grey.600" bgcolor={isDragActive ? "grey.200" : "background.paper"} lineHeight="1" px={2} mt={-1.25} mb={2}>
        on
      </Typography>

      <Button color="primary" type="button" sx={{
      bgcolor: "primary.light",
      px: "2rem",
      mb: "22px"
    }}>
        Select files
      </Button>

      <Small color="grey.600">Upload 280*280 image</Small>
  
   <Typography color="primary" bgcolor={"primary.light"} lineHeight="1" px={2} mt={ 1.25} mb={2}>
   {errors}
 </Typography>
    </Box>;
};

export default DropZone;