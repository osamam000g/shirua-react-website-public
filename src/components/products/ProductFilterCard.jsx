import Accordion from "components/accordion/Accordion";
import AccordionHeader from "components/accordion/AccordionHeader";
import FlexBox from "components/FlexBox";
import { H5, H6, Paragraph, Span } from "components/Typography";
import { Card, Checkbox, Divider, FormControlLabel, Link, Rating, TextField } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";

const ProductFilterCard = ({categoriesF , brands}) => {

  return <Card sx={{
    p: "18px 27px",
    overflow: "auto"
  }} elevation={1}>
      <H6 mb={1.25}>Categories</H6>

      {categoriesF.map(item => item.children ? <Accordion key={item.name} expanded>
            <AccordionHeader px={0} py={0.75} color="grey.600">
              <Span sx={{
          cursor: "pointer",
          mr: "9px"
        }}>{item.name}</Span>
            </AccordionHeader>
            {item.children.map(name => <Paragraph fontSize="14px" color="grey.600" pl="22px" py={0.75} key={name.name} sx={{
        cursor: "pointer" 
      }}>
                {name.name}
              </Paragraph>)}
          </Accordion> : <Paragraph className="cursor-pointer" fontSize="14px" color="grey.600" py={0.75} key={item.name}>
            {item.name}
          </Paragraph>)}

      <Divider sx={{
      mt: "18px",
      mb: "24px"
    }} />

      {/* <H6 mb={2}>Price Range</H6>
      <FlexBox justifyContent="space-between" alignItems="center">
        <TextField placeholder="0" type="number" size="small" fullWidth />
        <H5 color="grey.600" px={1}>
          -
        </H5>
        <TextField placeholder="250" type="number" size="small" fullWidth />
      </FlexBox> */}

      {/* <Divider sx={{
      my: "1.5rem"
    }} /> */}

      {/* <H6 mb={2}>Brands</H6>
      {brands.map(item => <FormControlLabel control={<Checkbox size="small" color="secondary" />} label={<Span color="inherit">{item}</Span>} sx={{
      display: "flex"
    }} key={item} />)} */}
{/* 
      <Divider sx={{
      my: "1.5rem"
    }} /> */}
{/* 
      {otherOptions.map(item => <FormControlLabel control={<Checkbox size="small" color="secondary" />} label={<Span color="inherit">{item}</Span>} sx={{
      display: "flex"
    }} key={item} />)}

      <Divider sx={{
      my: "1.5rem"
    }} /> */}

      {/* <H6 mb={2}>Ratings</H6>
      {[5, 4, 3, 2, 1].map(item => <FormControlLabel control={<Checkbox size="small" color="secondary" />} label={<Rating size="small" value={item} color="warn" readOnly />} sx={{
      display: "flex"
    }} key={item} />)}

      <Divider sx={{
      my: "1.5rem"
    }} /> */}

      <H6 mb={2}>Price Range</H6>
        {priceOptions.map(item => 
        <div>
          <Link
            mb={5}
              sx={{ marginBottom:"10px"}}
                  component="button"
                  variant="body2"
                  onClick={() => {
                    console.info("I'm a button.");
                  }}
                >
                  {item.name}
                </Link>
          </div>
          )}
 
    </Card>;
};

const categroyList = [{
  title: "Bath Preparations",
  subCategories: ["Bubble Bath", "Bath Capsules", "Others"]
}, {
  title: "Eye Makeup Preparations"
}, {
  title: "Fragrance"
}, {
  title: "Hair Preparations"
}];
const priceOptions = [
  {name:"Up to 50 EGP" , nameinarabic:"حتي 50 جنيه",slug1:"0" ,slug2:"50",url:""},
{name:"50 to 100 EGP" , nameinarabic:"من 50جنيه حتي 100جنيه",slug1:"50" ,slug2:"100",url:""},
{name:"100 to 300 EGP" , nameinarabic:"من 100 جنيه حتي 300جنيه",slug1:"100" ,slug2:"300",url:""},
{name:"700 to 1500 EGP" , nameinarabic:"من 700 جنيه حتي 1500 جنيه",slug1:"700" ,slug2:"1500",url:""},
{name:"1500 to 2500 EGP" , nameinarabic:"من 1500 جنيه حتي 2500 جنيه",slug1:"1500" ,slug2:"2500",url:""},
{name:"2500 EGP & above" , nameinarabic:"اعلي من 2500 جنيه",slug1:"2500" ,slug2:"0",url:""},]

const otherOptions = ["On Sale", "In Stock", "Featured"];
const colorList = ["#1C1C1C", "#FF7A7A", "#FFC672", "#84FFB5", "#70F6FF", "#6B7AFF"];
export default ProductFilterCard;
