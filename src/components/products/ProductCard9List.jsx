import FlexBox from "components/FlexBox";
import ProductCard9 from "components/product-cards/ProductCard9";
import productDatabase from "data/product-database";
import { Pagination } from "@mui/material";
import React, { useEffect, useState } from "react";
import { Span } from "../Typography";
import usePagination from "components/0data/Pagenation";

const ProductCard9List = (props) => {
  const [language, setLanguage] = useState("");
  let [page, setPage] = useState(1);
  const PER_PAGE = 25;
  const count = Math.ceil(props.filterdProduct.length / PER_PAGE);
  const _DATA = usePagination(props.filterdProduct, PER_PAGE);

  const handleChangepage = (e, p) => {
    setPage(p);
    _DATA.jump(p);
  };
  
  useEffect(() => {
    const lango = localStorage.getItem("language")
    setLanguage(lango)
  // console.log("r",UserMetaData.role)
  })


  return <div>
      { props.filterdProduct  ? _DATA.currentData().map((item, ind) => 
      <ProductCard9 id={item.idDoc} imgUrl={item.images && item.images[0]?item.images[0]:"/" } title={language == "AR" ?item.nameinarabic :item.name} rating={item.rating?item.rating:4} price={parseInt(item.saleprice != 0 ?item.saleprice : item.regularprice)} key={ind} {...item} />)
      :
      <>
        <Grid item lg={4} sm={6} xs={12} >
            <h3>
            {language == "AR" ?" لاتوجد نتائج للبحث" :"No Resaults"}
            </h3>
      </Grid>
    </>
      }

      <FlexBox flexWrap="wrap" justifyContent="space-between" alignItems="center" mt={4}>
        <Span color="grey.600">{_DATA.currentData().length} Product</Span>
        <Pagination 
         count={count}
        page={page}
        onChange={handleChangepage}
         variant="outlined" color="primary" />
      </FlexBox>
    </div>;
};

export default ProductCard9List;