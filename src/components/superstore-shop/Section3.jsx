import BazarCard from "components/BazarCard";
import Carousel from "components/carousel/Carousel";
import Category from "components/icons/Category";
import { collection, getDocs, limit, orderBy, query } from "firebase/firestore";
import useWindowSize from "hooks/useWindowSize";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { db } from "../../../firebase";
import CategorySectionCreator from "../CategorySectionCreator";
import ProductCard6 from "../product-cards/ProductCard6";

const Section3 = ({
  categoryList
}) => {
  const topCategoryList = [{
    title: "Electronic",
    niche: "Electronic",
    nameinarabic: "الكترونيات", 
    // subtitle: "3k orders this week",
    categoryUrl: "/",
    imgUrl: "/assets/images/banners/category-1.png"
  }, {
    title: "Camera & Photo",
    niche: "Camera & Photo",
    nameinarabic: "كاميرات",
    categoryUrl: "/",
    imgUrl: "/assets/images/banners/camera.jpg"
  }, {
    title: "Home Applications",
    niche: "Home Applications",
    nameinarabic: "اجهزه منزليه ",
    categoryUrl: "/",
    imgUrl: "/assets/images/banners/Home-appli.jpg"
  }, {
    title: "Computers & Accessories",
    niche: "Computers & Accessories",
    nameinarabic: "الكمبيوتر و لابتوب",
    categoryUrl: "/",
    imgUrl: "/assets/images/banners/pcs.jpg"
  }, {
    title: "Mobile Phones",
    niche: "Mobile Phones & Communication",
    nameinarabic: "الهواتف و الاكسسوارات",
    categoryUrl: "/",
    imgUrl: "/assets/images/banners/phones.jpg"
  }];

  const [categories, setcategories] = useState([]);
  const getProducts = async ()=>{
    const q = query(collection(db,"categories"), orderBy("name", "desc"), limit(5));
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      setcategories((doc1)=>[...doc1 , doc.data()])
    });
  }
  useEffect(()=>{
    getProducts()
  },[])
  useEffect(()=>{
    console.log(categories)
  })




  const [language, setLanguage] = useState("");
  useEffect(() => {
    const lango = localStorage.getItem("language")
    setLanguage(lango)
  })
  const [visibleSlides, setVisibleSlides] = useState(3);
  const width = useWindowSize();
  useEffect(() => {
    if (width < 650) setVisibleSlides(1);else if (width < 950) setVisibleSlides(2);else setVisibleSlides(3);
  }, [width]);
  return <CategorySectionCreator icon={<Category color="primary" />} title={language == "AR" ? "افضل التصنيفات" :"Top Categories" } seeMoreLink="#">
      <Carousel totalSlides={5} visibleSlides={visibleSlides}>
        {topCategoryList.map((item, ind) => <Link href={`/product/search/%20/${item.niche}/0/0/`} key={ind}>
            <a>
              <BazarCard sx={{
            p: "1rem"
          }} elevation={0}>
                <ProductCard6 title={language == "AR" ? item.nameinarabic :item.title} subtitle={item.subtitle} imgUrl={item.imgUrl} />
              </BazarCard>
            </a>
          </Link>)}
      </Carousel>
    </CategorySectionCreator>;
};

export default Section3;