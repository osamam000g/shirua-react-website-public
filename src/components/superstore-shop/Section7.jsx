/* eslint-disable react-hooks/exhaustive-deps */
import BazarCard from "components/BazarCard";
import { H3 } from "components/Typography";
import { Box, Container, Grid } from "@mui/material";
import React, { useEffect, useState } from "react";
import CategorySectionHeader from "../CategorySectionHeader";
import FlexBox from "../FlexBox";
import ProductCard1 from "../product-cards/ProductCard1";
import ProductCategoryItem from "./ProductCategoryItem";
import { collection, getDocs, limit, orderBy, query, where } from "firebase/firestore";
import { db } from "../../../firebase";
import { useRouter } from 'next/router'

const Section7 = props => {
  const router = useRouter()

const  BrandsList =  [
 {name: 'Apple',url:"/assets/images/brands/apple.png"},
 {name: 'Xiaomi',url:"/assets/images/brands/xiaomi.png"},
 {name: 'Huawei',url:"/assets/images/brands/Huawei.png"},
 {name: 'Nokia',url:"/assets/images/brands/Nokia.png"},
 {name: 'Samsung',url:"/assets/images/brands/Samsung.png"},
 {name: 'Infinix',url:"/assets/images/brands/Infinix.png"},
 {name: 'Oppo',url:"/assets/images/brands/Oppo.png"}
]
  const {
    // mobileList,
    mobileShops,
    mobileBrands
  } = props;

  // const mobileList =[]

  const [mobileList, setmobileList] = useState([]);
  // const [BrandsList, setBrandsList] = useState([]);
  const getProducts = async ()=>{
    const q = query(collection(db,"products") , where("phoneSection", "==", true) );
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      // doc.data() is never undefined for query doc snapshots
      // console.log(doc.id, " => ", doc.data());
      setmobileList((doc1)=>[  doc.data()])
    });
  }
  useEffect(()=>{
    getProducts()
  },[])

  const [type, setType] = useState("brands");
  const [selected, setSelected] = useState("");
  const [list, setList] = useState([]);

 const handleBrandClick = (e ,url) =>{
  e.preventDefault()
   return router.push(url);

  }
  const handleCategoryClick = brand => () => {
    if (selected.match(brand)) {
      setSelected("");
    } else setSelected(brand);
  };

  useEffect(() => {
    if (type === "brands") setList(mobileBrands);else setList(mobileShops);
  }, [type]);



  const [language, setLanguage] = useState("");
  useEffect(() => {
    const lango = localStorage.getItem("language")
    setLanguage(lango)
  // console.log("r",UserMetaData.role)
  })


  return <Container sx={{
    mb: "70px"
  }}>
      <FlexBox>
        <BazarCard sx={{
        display: {
          xs: "none",
          md: "block"
        },
        borderRadius: "10px",
        padding: "1.25rem",
        mr: [language =="AR" ? "" :"1.75rem"],
        ml: [language =="AR" ? "1.75rem" :"0"],
        height: "100%"
      }}>
          <FlexBox mt={-1} mb={1}>
            <H3 fontWeight="600" fontSize="20px" padding="0.5rem 1rem" style={{
            cursor: "pointer"
          }} color={type === "brands" ? "grey.900" : "grey.600"} onClick={() => setType("brands")}>
                {language =="AR" ? "براند" :"Brands"}
            </H3>
            <H3 fontWeight="600" fontSize="20px" paddingTop="0.5rem" lineHeight="1.3" color="grey.400">
              |
            </H3>
            {/* <H3 fontWeight="600" fontSize="20px" padding="0.5rem 1rem" color={type === "shops" ? "grey.900" : "grey.600"} style={{
            cursor: "pointer"
          }} onClick={() => setType("shops")}>
               {language =="AR" ? "متجر" :"Shops"}
            </H3> */}
          </FlexBox>

          {BrandsList.map(brand =>
          <a  href="#" key={brand.name} onClick={(e)=>handleBrandClick(e ,`/product/search/%20/All%20Categories/0/0/${brand.name}`)}>

              <ProductCategoryItem  title={brand.name} imgUrl={brand.url} isSelected={!!selected.match(brand)} sx={{
              mb: "0.75rem",
              bgcolor: selected.match(brand) ? "white" : "grey.100"
            }} />
          </a>
        )}
        <a  href="#"  onClick={(e)=>handleBrandClick(e ,`/product/search/%20/Mobile%20Phones%20&%20Communication/0/0/0`)}>

          <ProductCategoryItem title={language =="AR" ? "تسوق الان":`Shop Now`} isSelected={!!selected.match(`all-${type}`)}  sx={{
          mt: "2rem",
          bgcolor: selected.match(`all-${type}`)
        }} />
      </a>
        </BazarCard>

        <Box flex="1 1 0" minWidth="0px">
          <CategorySectionHeader title={language =="AR" ? "هواتف " :"Mobile Phones"} seeMoreLink="#" />

          <Grid container spacing={3}>
            {mobileList.map((item, ind) => <Grid item lg={4} sm={6} xs={12} key={ind}>
                <ProductCard1 off={25} hoverEffect id={item.idDoc} imgUrl={item.images?item.images[0]?item.images[0]:"/":"/"} title={language == "AR" ?item.nameinarabic :item.name} rating={item.rating?item.rating:4} price={parseInt(item.saleprice != 0 ?item.saleprice : item.regularprice)} 
 />
              </Grid>)}
          </Grid>
        </Box>
      </FlexBox>
    </Container>;
};

export default Section7;