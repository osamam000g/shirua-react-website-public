import BazarCard from "components/BazarCard";
import Category from "components/icons/Category";
import LazyImage from "components/LazyImage";
import { Box, Container, Grid, styled } from "@mui/material";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import CategorySectionHeader from "../CategorySectionHeader";
const StyledBazarCard = styled(BazarCard)(({
  theme
}) => ({
  display: "flex",
  alignItems: "center",
  padding: "0.75rem",
  borderRadius: 8,
  transition: "all 250ms ease-in-out",
  "&:hover": {
    boxShadow: theme.shadows[3]
  }
}));

const Section10 = ({
  categories
}) => {
  const [language, setLanguage] = useState("");
  useEffect(() => {
    const lango = localStorage.getItem("language")
    setLanguage(lango)
    console.log(categories , "categories")
  // console.log("r",UserMetaData.role)
  })
  const cat = [
      { name: 'Electronic', slug: 'Electronic',imgUrl: '/assets/images/brands/Electronic.jpg' ,nameinarabic:"الكترونيات" }
     ,{ name: 'Washers & Dryers',       slug: 'Washers & Dryers', imgUrl: '/assets/images/brands/Washers & Dryers.jpg'  ,nameinarabic:"غساله ملابس"}
    , { name: 'Small Appliances',      slug: 'Small Appliances',imgUrl: '/assets/images/brands/Small Appliances.jpg'  ,nameinarabic:"اجهزه صغيره"}
    , { name: 'Refrigerators',      slug: 'Refrigerators, Freezers',imgUrl: '/assets/images/brands/Refrigerators, Freezers.jpg' ,nameinarabic:"ثلاجات" }
    , { name: 'Microwaves ',imgUrl: '/assets/images/brands/Microwaves & Countertop Ovens.jpg' ,nameinarabic:"ميكرويف" }
    , { name: 'Heating & Cooling',      slug: 'Heating & Cooling',imgUrl: '/assets/images/brands/Heating & Cooling.jpg'  ,nameinarabic:"تكيفات"}
    , { name: 'Dishwashers',       slug: 'Dishwashers & Dish Dryers', imgUrl: '/assets/images/brands/Dishwashers & Dish Dryers.jpg'  ,nameinarabic:"غسالات اطباق"}
    , { name: 'TV & Video',      slug: 'Home Theater, TV & Video',imgUrl: '/assets/images/brands/TV & Video.jpg' ,nameinarabic:"تلفزيونات" }
    , { name: 'Headphones',      slug: 'Headphones, Earbuds & Accessoriesectronics',imgUrl: '/assets/images/brands/Headphones.jpg'  ,nameinarabic:"سماعات"}
    , { name: 'Computers',      slug: 'Computers & Accessories',imgUrl: '/assets/images/brands/Computers & Accessories.jpg'  ,nameinarabic:"كمبيوتر"}
    , { name: 'Camera',       slug: 'Camera & Photo', imgUrl: '/assets/images/brands/Camera & Photo.jpg' ,nameinarabic:"كاميرات" }
    // , { name: 'Electronics',      slug: 'Electronics',imgUrl: '/assets/images/products/Fashion/Accessories/12.Xiaomimiband2.png' }
  ]
  return <Container sx={{
    mb: "70px"
  }}>
      <CategorySectionHeader title={language == "AR" ? "التصنيفات" :"Categories"} icon={<Category color="primary" />} seeMoreLink="#" />

      <Grid container spacing={3}>
        {cat.map((item, ind) => <Grid item lg={2} md={3} sm={4} xs={12} key={ind}>
            <Link href={`/product/search/0/${item.slug}/0/0/0`}>
              <a>
                <StyledBazarCard elevation={1}>
                  <LazyImage src={item.imgUrl} alt="fashion" height={52} width={52} objectFit="contain" borderRadius="8px" />
                  <Box fontWeight="600" ml={language == "AR" ? 0:1.25} mr={language == "AR" ? 1.25:0}>
                    {language == "AR" ?item.nameinarabic :item.name}
                  </Box>
                </StyledBazarCard>
              </a>
            </Link>
          </Grid>)}
      </Grid>
    </Container>;
};

export default Section10;