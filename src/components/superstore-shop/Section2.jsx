import Light from "components/icons/Light";
import useWindowSize from "hooks/useWindowSize";
import { Box } from "@mui/material";
import React, { useEffect, useState } from "react";
import Carousel from "../carousel/Carousel";
import CategorySectionCreator from "../CategorySectionCreator";
import ProductCard1 from "../product-cards/ProductCard1";
import { collection, getDocs, limit, orderBy, query } from "firebase/firestore";
import { db } from "../../../firebase";

const Section2 = ({
  flashDeals
}) =>   {
  const [categories, setcategories] = useState([]);
  const getProducts = async ()=>{
    const q = query(collection(db,"products"), orderBy("name", "desc"), limit(10));
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      setcategories((doc1)=>[...doc1 , doc.data()])
    });
  }
  useEffect(()=>{
    getProducts()
  },[])
  useEffect(()=>{
    console.log(categories)
  })
  const [language, setLanguage] = useState("");
  useEffect(() => {
    const lango = localStorage.getItem("language")
    setLanguage(lango)
  })

  const [visibleSlides, setVisibleSlides] = useState(4);
  const width = useWindowSize();
  useEffect(() => {
    if (width < 500) setVisibleSlides(1);else if (width < 650) setVisibleSlides(2);else if (width < 950) setVisibleSlides(3);else setVisibleSlides(4);
  }, [width]);
  return <CategorySectionCreator icon={<Light color="primary" />} title={language == "AR" ? "تخفيضات كبيره" :"Flash Deals" } seeMoreLink="#">
      <Box mt={-0.5} mb={-0.5}>
        <Carousel totalSlides={categories.length} visibleSlides={visibleSlides} infinite={true}>
          {categories.map((item, ind) => <Box py={0.5} key={ind}>
              <ProductCard1 id={item.idDoc} imgUrl={item.images?item.images[0]?item.images[0]:"/":"/"} title={language == "AR" ?item.nameinarabic :item.name} rating={item.rating?item.rating:4} price={parseInt(item.saleprice != 0 ?item.saleprice : item.regularprice)} off={5} />
             {/* { console.log(item.images[0] , "item")} */}
            </Box>)}
            
        </Carousel>
      </Box>
    </CategorySectionCreator>;
};

export default Section2;