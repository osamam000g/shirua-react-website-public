import CarouselCard1 from "components/carousel-cards/CarouselCard1";
import Carousel from "components/carousel/Carousel";
import Navbar from "components/navbar/Navbar";
import { Box, Container } from "@mui/material";
import { Fragment } from "react";

const Section1 = ({
  carouselData
}) => {
  const mainCarouselData = [
    {
    title: "50% Off For Your First Shopping",
    titleAr: "50% خصم علي اول طلب",
    photoUrl: "/assets/images/products/nike-black.png",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis lobortis consequat eu, quam etiam at quis ut convalliss.",
    descriptionAr: "خصم علي اول طلبخصم علي اول طلبخصم علي اول طلبخصم علي اول طلبخصم علي اول طلب",
    buttonText: "Shop Now",
    buttonTextAr: "تسوق الان ",
    buttonLik: "/product/search/%20/All%20Categories/0/0/0#"
  },
  {
    title: "50% Off For Your First Shopping",
    titleAr: "50% خصم علي اول طلب",
    photoUrl: "/assets/images/products/nike-black.png",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis lobortis consequat eu, quam etiam at quis ut convalliss.",
    descriptionAr: "خصم علي اول طلبخصم علي اول طلبخصم علي اول طلبخصم علي اول طلبخصم علي اول طلب",
    buttonText: "Shop Now",
    buttonTextAr: "تسوق الان ",
    buttonLik: "/product/search/%20/All%20Categories/0/0/0"
  }
  ]

  return <Fragment>
      <Navbar />
      <Box bgcolor="white" mb={7.5}>
        <Container sx={{
        py: "2rem"
      }}>
          <Carousel totalSlides={2} visibleSlides={1} infinite={true} autoPlay={false} showDots={true} showArrow={false} spacing="0px">
            {mainCarouselData && mainCarouselData.map((data, ind) => <CarouselCard1 carousel={data} key={ind} />)}
          </Carousel>
        </Container>
      </Box>

    </Fragment>;
};

export default Section1;