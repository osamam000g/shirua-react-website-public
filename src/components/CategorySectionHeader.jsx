import { ArrowLeft } from "@mui/icons-material";
import ArrowRight from "@mui/icons-material/ArrowRight";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import FlexBox from "./FlexBox";
import { H2 } from "./Typography";

const CategorySectionHeader = ({
  title,
  seeMoreLink,
  icon
}) => {

  const [language, setLanguage] = useState("");
  useEffect(() => {
    const lango = localStorage.getItem("language")
    setLanguage(lango)
  // console.log("r",UserMetaData.role)
  })

  return <FlexBox justifyContent="space-between" alignItems="center" mb={3}>
      <FlexBox alignItems="center">
        {icon && <FlexBox ml={language == "AR" ? 1:0} mr={language == "AR" ? 0:1} alignItems="center">
            {icon}
          </FlexBox>}
        <H2 fontWeight="bold" lineHeight="1">
          {title}
        </H2>
      </FlexBox>

      {seeMoreLink && <Link href={seeMoreLink}>
          <a>
            <FlexBox alignItems="center" ml={1} color="grey.600">
            {language =="AR" ?"عرض المزيد" :"View all"}
             {language =="AR" ?<ArrowLeft fontSize="small" color="inherit" />:<ArrowRight fontSize="small" color="inherit" />}
              
            </FlexBox>
          </a>
        </Link>}
    </FlexBox>;
};

export default CategorySectionHeader;