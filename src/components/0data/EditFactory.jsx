import DeliveryBox from "components/icons/DeliveryBox";
import DashboardPageHeader from "components/layout/DashboardPageHeader";
import VendorDashboardLayout from "components/layout/VendorDashboardLayout";
import { Alert, Button, Card, Checkbox, CircularProgress, FormControlLabel, Grid, MenuItem, TextField, Typography } from "@mui/material";
import { Formik } from "formik";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import * as yup from "yup";
import { collection, connectFirestoreEmulator, deleteDoc, doc, onSnapshot, updateDoc } from "firebase/firestore";
import { db } from "../../../firebase";
 
import DeleteIcon from '@mui/icons-material/Delete';
import SendIcon from '@mui/icons-material/Send';
import DropZone from "components/DropZone";
import { getStorage, ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { DatePicker, LocalizationProvider } from "@mui/lab";
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { Box } from "@mui/system";
// import { date } from "yup/lib/locale";

const EditFactory = (props) => {

  const [showLoading, setshowLoading] = useState(true);
  const [showSuccessMessage, setshowSuccessMessage] = useState(false);
  const [categories, setcategories] = useState([]);
  const [thisItem, setthisItem] = useState([]);
  const [MessageWord, setMessageWord] = useState([]);
  const [AcceptedFiles, setAcceptedFiles] = useState([]);
  const [showLoadingUpload, setshowLoadingUpload] = useState(false);
  const [SavedImage, setSavedImage] = useState([]);
  const storage = getStorage();
  const [discountType, setdiscountType] = useState("");

  const [ExpireValue, setExpireValue] = useState("");
  const [checked2, setChecked2] = useState([]);
  const [checked, setChecked] = useState([false, false]);

  

  const router = useRouter();

const id =props.id  // get id 
const collectionType =props.collectionType
const collectionLink =props.collectionLink
const itemsFieldsValues =props.itemsFieldsValues


// useEffect(()=>{
//   setChecked2(arr);
// },[])
const handleChange2 = (event , i , itemo) => {
  // console.log(event.target.checked , "i")
  // console.log(i, "itemo")
  let arr =[]
  const obj = {};

  obj[itemo.name] = event.target.checked 
  obj["name"] = itemo.name
  // console.log(arr,"arr")
  arr.push(obj)
  setChecked2(arr);
};



const getYupValues = () => //Get Yup  ===========
{
  let arr = [];
  const obj = {};
  itemsFieldsValues.map((item , i)=> 
  {
    // console.log(i,"p")
    if(item.yupType=="string" && item.yupReqired == true)
    {
      obj[item.name] = yup.string().required("required")
    }
    else if(item.yupType=="number" && item.yupReqired == true)
    {
      obj[item.name] = yup.number().required("required")
    }
    else if(item.yupType=="selectone" && item.yupReqired == true)
    {
      obj[item.name] = yup.string().required("required")
      item.options.map((option)=>{
        obj[option] = yup.number()
      })
    }
     arr.push(obj)

  })
   return yup.object().shape(arr[0])
}// Get Yup ends here -------------------
const handleFormSubmit = async (values , accepted1) => {
  setshowLoading(true)

  if(collectionType == "userd"){
    console.log(collectionType)
    setshowLoading(true)
    console.log(JSON.stringify({ "uid":id}));
  
    const response = await fetch('/api/users',{
      method:'PUT',
      body: JSON.stringify({ uid: props.UserId , id: id , values:values }),
      headers:{'Content-Type':'application/json'},
    })
    const collections = await response.json();
    console.log(collections , "data from api   ")
    // if(collections.error){
    //   settoastopen(true)
    //   setToastMessage(collections.error)
    //   setshowLoading(false)
  
    // }else{
    //   settoastopen(true)
    //   setToastMessage("user Deleted Successfully")
    //     setshowLoading(false)
    // }


  }else{
    setAcceptedFiles(accepted1)
    const itemRef = doc(db, collectionType , id); //change 5 collection
    const payloadSet = getitemValueArray(values)
    await updateDoc(itemRef, payloadSet)
    .then(()=>{
      setMessageWord("item updated successfully")
      setshowLoading(false)
      setshowSuccessMessage(true)
    setAcceptedFiles(accepted1)
    // setChecked2([])

    }).catch((error)=>console.log(error.message))
  }

};

const removeFile = (i)=>{// Remove  Image File from state ======================
  let valueToRemove = [AcceptedFiles[i]];
  setAcceptedFiles(AcceptedFiles.filter(element => !valueToRemove.includes(element)))
}
// console.log("file.id" , id)

const files1 =  AcceptedFiles.map((file , i) => { //view Images Function =================
  return (
  <li key={i}>
     <img src={file.url } style={{width:"100px"}}alt="Logo" />
  <Typography onClick={()=>removeFile(i)} color="primary" bgcolor={"primary.light"} lineHeight="1" px={2} mt={ 1.25} mb={2}>
  Remove Image
</Typography>
  </li>
)}
);
const getSavedImageView = SavedImage.map((file , i) => { //view Images Function =================
      return (
      <li key={i}>
      <img src={file[i]} style={{width:"100px"}}alt="Logo" />
      <Typography onClick={()=>removeFile(i)} color="primary" bgcolor={"primary.light"} lineHeight="1" px={2} mt={ 1.25} mb={2}>
      Remove Image
    </Typography>
      </li>
    )}
    );


 // Upload Image Function ===========================
 const uploadImageFun = (file) =>{
  if(file.length == 0)
  {
      //  console.log("unAccepted file")
  }else
  {

        setshowLoadingUpload(true)
        const metadata = {
            contentType: 'image/jpeg'
          };
        const storageRef = ref(storage, 'images/' + file[0].name);
        const uploadTask = uploadBytesResumable(storageRef, file[0], metadata);

        uploadTask.on('state_changed',
        (snapshot) => {

          const progress1 = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log('Upload is ' + progress1 + '% done');
          switch (snapshot.state) {
            case 'paused':
              console.log('Upload is paused');
              break;
            case 'running':
              console.log('Upload is running');
              break;
          }
        }, 
        (error) => {
          switch (error.code) {
            case 'storage/unauthorized':
              console.log(error.code)
              break;
            case 'storage/canceled':
              console.log(error.code)
              break;
            case 'storage/unknown':
              console.log(error.code)
              break;
          }
        }, 
        () => {
          setshowLoadingUpload(false)

          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
              // console.log('File available at', downloadURL);
              // console.log('file', file);
              const thisFile = file
              Object.assign(thisFile[0],{url:downloadURL})
              setAcceptedFiles( prevArray => [...prevArray, {url:downloadURL}])  
          });
        }
                    ); // on upload task fun
  }//else 
}// upload fun ends here ==============================

useEffect(()=>{
    onSnapshot(collection(db,collectionType),(categories)=> setcategories(categories.docs)) 
},[])
  useEffect(()=>{
    
    for (var i = 0; i < categories.length; i++) {
      if(i + 1 === categories.length){ // IF LAST ITEM 

        if( categories[i].data().idDoc != id || categories[i].data().userid != id  ){
          setTimeout(()=>{
            router.push(collectionLink)
          },[1000])
          console.log(thisItem  , " redirect")           
        }else{
          setthisItem(categories[i].data())

          if(typeof(categories[i].data().images ) !== 'undefined' && categories[i].data().images != null  )
          {
            console.log("added here 1")

            if(typeof(categories[i].data().images[0] ) !== 'undefined' && categories[i].data().images[0] != null  && Object.keys(categories[i].data().images).length !=  AcceptedFiles.length ){
              const thisFile0 = []
              Object.assign(thisFile0,{url:categories[i].data().images[0]})
              // console.log(thisFile0 , "thisFile0")
              setAcceptedFiles( prevArray => [...prevArray, thisFile0])  
            }

            if(typeof(categories[i].data().images[1] ) !== 'undefined' && categories[i].data().images[1] != null  && Object.keys(categories[i].data().images).length !=  AcceptedFiles.length ){
              const thisFile1 = []
              Object.assign(thisFile1,{url:categories[i].data().images[1]})
              // console.log(thisFile1 , "thisFile1")
              setAcceptedFiles( prevArray => [...prevArray, thisFile1])  
            }
            if(typeof(categories[i].data().images[2] ) !== 'undefined' && categories[i].data().images[2] != null  && Object.keys(categories[i].data().images).length !=  AcceptedFiles.length ){
              const thisFile2 = []
              Object.assign(thisFile2,{url:categories[i].data().images[2]})
              // console.log(thisFile2 , "thisFile2")
              setAcceptedFiles( prevArray => [...prevArray, thisFile2])  
            }
            if(typeof(categories[i].data().images[3] ) !== 'undefined' && categories[i].data().images[3] != null  && Object.keys(categories[i].data().images).length !=  AcceptedFiles.length ){
              const thisFile3 = []
              Object.assign(thisFile3,{url:categories[i].data().images[3]})
              // console.log(thisFile3 , "thisFile3")
              setAcceptedFiles( prevArray => [...prevArray, thisFile3])  
            }
            
          }
          setTimeout(()=>{
              setshowLoading(false)
          },[1000])
          // console.log("LAST item found", i+1)
          break;

        }
      }else{
        console.log(i+1,"i"+i+1)
        if( categories[i].data().idDoc == id  || categories[i].data().userid == id){
          setthisItem(categories[i].data())
          if(typeof(categories[i].data().images ) !== 'undefined' && categories[i].data().images != null  )
          {


            if(typeof(categories[i].data().images[0] ) !== 'undefined' && categories[i].data().images[0] != null && Object.keys(categories[i].data().images).length !=  AcceptedFiles.length){
              const thisFile0 = []
              Object.assign(thisFile0,{url:categories[i].data().images[0]})
              // console.log(thisFile0 , "thisFile0")
            console.log("added here 2" ,  Object.keys(categories[i].data().images).length)
            console.log("added here 2" ,  AcceptedFiles.length)

              setAcceptedFiles( prevArray => [...prevArray, thisFile0])  
            }

            if(typeof(categories[i].data().images[1] ) !== 'undefined' && categories[i].data().images[1] != null  && Object.keys(categories[i].data().images).length !=  AcceptedFiles.length ){
              const thisFile1 = []
              Object.assign(thisFile1,{url:categories[i].data().images[1]})
              // console.log(thisFile1 , "thisFile1")
              setAcceptedFiles( prevArray => [...prevArray, thisFile1])  
            }
            if(typeof(categories[i].data().images[2] ) !== 'undefined' && categories[i].data().images[2] != null   && Object.keys(categories[i].data().images).length !=  AcceptedFiles.length){
              const thisFile2 = []
              Object.assign(thisFile2,{url:categories[i].data().images[2]})
              // console.log(thisFile2 , "thisFile2")
              setAcceptedFiles( prevArray => [...prevArray, thisFile2])  
            }


            if(typeof(categories[i].data().images[3] ) !== 'undefined' && categories[i].data().images[3] != null   && Object.keys(categories[i].data().images).length !=  AcceptedFiles.length){
              const thisFile3 = []
              Object.assign(thisFile3,{url:categories[i].data().images[3]})
              // console.log(thisFile3 , "thisFile3")
              setAcceptedFiles( prevArray => [...prevArray, thisFile3])  
            }
            
          }


          // categories[i].data().images.map((image)=>{
          //   const thisFile = []
          //   Object.assign(thisFile,{url:image})
          //   console.log(thisFile , "thisFile")
          //   setAcceptedFiles( prevArray => [...prevArray, thisFile])  

          // })

          setTimeout(()=>{
              setshowLoading(false)
          },[1000])
          // console.log("first item", i)
          break;

        }
      }
      // console.log(thisItem,"this")
    }   
  },[categories])
const deleteUSer = async () =>{
  setshowLoading(true)
  console.log(JSON.stringify({ "uid":id}));

  const response = await fetch('/api/users',{
    method:'DELETE',
    body: JSON.stringify({ uid: props.UserId , id: id }),
    headers:{
      'Content-Type':'application/json'
    },
  })
  const collections = await response.json();
  console.log(collections , "data from api   ")
  if(collections.error){
    settoastopen(true)
    setToastMessage(collections.error)
    setshowLoading(false)

  }else{
    settoastopen(true)
    setToastMessage("user Deleted Successfully")
      setshowLoading(false)
  }
  
}
const deleteItem = async ()=> {
  setshowLoading(true)

  const itemRef = doc(db, collectionType, id); 
  await deleteDoc(itemRef).then(()=>{
    setMessageWord("item deleted successfully")
    setshowLoading(false)
    setshowSuccessMessage(true)
    setTimeout(()=>{
      // router.push(collectionLink) 
    },[2000])
  })

 
  }
  //get image from thisitem  and categories 
  useEffect(()=>{
    if(typeof(thisItem.expire) !== 'undefined' && thisItem.expire != null  )
    {
      let  der = thisItem.expire.seconds * 1000
      let cDate = new Date(der)
      let DDate = cDate.toDateString()
      console.log("thisiteme0" ,DDate)
      console.log("der11" ,der)
      setExpireValue(DDate)
    }
    console.log("thisItem" , thisItem.category)
    setChecked2((prev)=>[thisItem.category , ...prev])

  },[thisItem])

  useEffect(()=>{
    // console.log(AcceptedFiles,"AcceptedFiles")
  },[AcceptedFiles])
 

  const getitemValueArray =(values) =>{

    let arr = [];
    const obj = {};
    const objImage = {};
    itemsFieldsValues.map((item , i)=> 
    {
      if(item.yupType == "string" || item.yupType == "number" )
      {
        if(item.name == "isVendorActive" ){
          console.log("is active value ", thisItem[item.name]  )
          if(values[item.name]){
              obj[item.name] = true
              arr.push(obj)
          }else{
            obj[item.name] = false
            arr.push(obj)
          }
         }else{
          obj[item.name] = values[item.name]
          arr.push(obj)
         }


      }if(item.yupType == "date"   ){
        obj[item.name] = ExpireValue
        arr.push(obj)
      }
      if(item.yupType == "selectone") 
      {
        obj[item.name] = values[item.name]
        item.options.map((option)=>{
          obj[option] =values[option]
        })
        arr.push(obj)
      }
      if(item.yupType == "selectBox") 
      {
        
        obj[item.name] = checked2[0]
  
        arr.push(obj)
      }
  
    })
    AcceptedFiles.map((item ,i )=>{
      console.log("image"+i , item.url)
      objImage[i] = item.url
      
      })
    obj["images"] = objImage
    arr.push(obj)
  
    return arr[0] 
  }
  const getInitialValues  = () => {
    let arr = [];
    const obj = {};
    // {[item.name]:values[item.name] }
  itemsFieldsValues.map((item , i)=> {
    // if isVendorActive and if  collectionType == userd in select
     if(item.name == "isVendorActive" ){
      console.log("is active value ", thisItem[item.name]  )
      if(thisItem[item.name] ){
          obj[item.name] = "true"
          arr.push(obj)
      }else{
        obj[item.name] = "false"
        arr.push(obj)

      }
     }else{
      obj[item.name] = thisItem[item.name]
      arr.push(obj)
     }
   
  })
     return arr[0] 
};
 

  return <div>
  {/* change item link 7 */}
      <DashboardPageHeader title={"Edit "+collectionType} icon={DeliveryBox} button={<Link href={collectionLink} passHref> 
      <Button variant="outlined" endIcon={<SendIcon />}>
           Back  to items list
            </Button>
          </Link>} />
          <DashboardPageHeader title=""   button={
              <Button onClick={collectionType == "userd" ? deleteUSer : deleteItem } color="primary" sx={{bgcolor: "primary.light",px: "2rem"}} endIcon={<DeleteIcon />}>
                Delete
              </Button>
           } />

          { showLoading ?  <CircularProgress /> :<span></span>}
      {  showSuccessMessage ?  <Alert severity="success" onClose={() => {setshowSuccessMessage(false)}}> {MessageWord} </Alert> :<span></span>}


      <Card sx={{
      p: "30px"
    }}>
        <Formik enableReinitialize  initialValues={getInitialValues()
} validationSchema={getYupValues()} onSubmit={(values)=> handleFormSubmit(values , AcceptedFiles)}>
          {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit
        }) => <form onSubmit={handleSubmit}>
              <Grid container spacing={3}>
              {/* map fields Values Here  9 */}
             { itemsFieldsValues.map((item , i)=> {
                    if(item.type=="text" || item.type=="number")
                    {
                        return(
                        <Grid key={i} item sm={6} xs={12}>
                            <TextField name={item.name} label={item.label}  placeholder={item.placeholder}  fullWidth onBlur={handleBlur} onChange={handleChange} value={values[item.name] || ""} error={!!touched[item.name] && !!errors[item.name]} helperText={touched[item.name] && errors[item.name]} />
                        </Grid>
                        )
                    } if(item.type=="devider")
                    {
                        return(
                       <Grid key={i} item  xs={12}>
                          <hr />
                        </Grid>
                        )
                    }if(item.type=="header")
                    {
                        return(
                       <Grid key={i} item  xs={12}>
                          <h2>{item.name}</h2>
                        </Grid>
                        )
                    }else if(item.type=="select" )
                    {
                      return (
                       <Grid key={i} item sm={6} xs={12}>
                            <TextField name={item.name} label={item.label} placeholder={item.placeholder} fullWidth select onBlur={handleBlur} onChange={handleChange} value={values[item.name] || ""} error={!!touched[item.name] && !!errors[item.name]} helperText={touched[item.name] && errors[item.name]}>
                            {item.values.map((val ,i)=>{return (<MenuItem key={i} value={val}>{val}</MenuItem>)})}
                            </TextField>
                        </Grid>
                      )
                    }else if(item.type == "textArea")
                              {
                                return(
                                <Grid key={i} item xs={12}>
                                  <TextField name={item.name} label={item.label} placeholder={item.placeholder} rows={6} multiline fullWidth onBlur={handleBlur} onChange={handleChange} value={values[item.name] || ""} error={!!touched[item.name] && !!errors[item.name]} helperText={touched[item.name] && errors[item.name]} />
                                </Grid>
                                      )
                              }
                              else if(item.type == "images")
                              {
                                return(
                                  <Grid key={i} item xs={12}>
                                    <DropZone AcceptedFilesLen={AcceptedFiles.length} maxFiles={4}
                                    onChange={files => {uploadImageFun(files) }}  />
                            
                                    {files1}
                                    {getSavedImageView}
                                    {/* { AcceptedFiles.length > 0 ? proccess+"%" : ""} */}
                                    { showLoadingUpload ?  <CircularProgress /> :<span></span>}
                                    { AcceptedFiles.length < 2 ? "" :"Max Upload image per product is 4 images"}
                                
                                  </Grid>
                                      )
                              }else if(item.type == "date")
                              {
                                return(
                                  <Grid key={i} item xs={6}>
                                  <LocalizationProvider dateAdapter={AdapterDateFns}>
                                    <DatePicker
                                      label="Expire Date"
                                      value={ExpireValue}
                                      onChange={(newValue) => {
                                        setExpireValue(newValue);
                                      }}
                                      renderInput={(params) => <TextField {...params} />}
                                    />
                                  </LocalizationProvider>  
                                
                                  </Grid>
                                      )
                              }else if(item.type == "selectone")
                              {
                                console.log(item.options.length , "options")
                             return   (
                              <Grid key={i} item xs={12}>
                           
                                      <TextField name={item.name} label={item.label} placeholder={item.placeholder} fullWidth select onBlur={handleBlur} onChange={(ev)=>{handleChange(ev) ; setdiscountType(ev.target.value)}} value={values[item.name] || ""} error={!!touched[item.name] && !!errors[item.name]} helperText={touched[item.name] && errors[item.name]}>
                                          {item.options.map((val,i)=>{return (<MenuItem key={i} value={val}>{val}</MenuItem>)})}
                                  </TextField>

                               {item.options.map((option , i)=>{
                                console.log(option, "inside")

                                  return(
                                  <Grid key={i} item xs={12}>
                                 <br />
                                      <Grid   item sm={6} xs={12}>
                                        <TextField style={{display:discountType === option ?"block" : "none"}} name={option} label={option}  placeholder={option}  fullWidth onBlur={handleBlur} onChange={handleChange} value={values[option] || ""} error={!!touched[option] && !!errors[option]} helperText={touched[item.name] && errors[option]} />
                                    </Grid>
                                  </Grid>
                                      )
                                })}

                              </Grid>
                                )
                                
                              }else if (item.type == 'selectBox')//one
                              {
                                {/* console.log(item.values[0].children , "val") */}
                               

                                const getChild = (itemo ) =>{
                                  console.log(itemo , "itemo")
                                // setChecked((old)=>[...old, {idDoc:itemo.idDoc ,state:false}])
                                  return itemo.children.map((itemo , i)=>(
                                       <Box key={i} sx={{ display: 'flex', flexDirection: 'column', ml: 3 }}>     
                                        <FormControlLabel
                                        label={itemo.name}
                                        control={<Checkbox
                                          checked={  typeof(checked2[0]) !== 'undefined' && checked2[0] != null && typeof(checked2[0][itemo.name]) !== 'undefined' && checked2[0][itemo.name] != null ? checked2[0][itemo.name] : checked[0]}
                                          onChange={(ev)=>handleChange2(ev , i,itemo)} />}
                                        />
                                             {typeof(itemo.children ) !== 'undefined' && itemo.children != null ?getChild(itemo):(<></>)}
                                      </Box>
                                      ))
                                  
                                }                              
                                    return (
                                          item.values.map((itemo , i )=>{
                                            console.log(checked2[0] ,"dsdsdsdsqwe3edsdvskmdflkwmflksmlkdfmskldmvl")

                                          return<Grid key={i} item sm={12} xs={12}>
                                              
                                            <div>
                                              <FormControlLabel
                                                label={itemo.name}
                                                control={
                                                  <Checkbox
                                                  checked={ typeof(checked2[0]) !== 'undefined' && checked2[0] != null && typeof(checked2[0][itemo.name]) !== 'undefined' && checked2[0][itemo.name] != null ? checked2[0][itemo.name] : checked[0]}
                                                  // indeterminate={checked2[i] !== checked2[i]}
                                                    onChange={(ev)=>handleChange2(ev , i,itemo)}
                                                  />
                                                }
                                              />
                                             {typeof(itemo.children ) !== 'undefined' && itemo.children != null ?getChild(itemo):(<></>)}
                                                
                                            </div>
                                        </Grid>}
                                   ) 
                                 )
                              }
              })}
                
             
          
              </Grid>
              <Button variant="contained" color="primary" type="submit" sx={{
            mt: "25px"
          }}>
                Save item
              </Button>
            </form>}
        </Formik>
      </Card>
    </div>;
};

// const getYupValue = () =>{
//     let arr = [];
//     const obj = {};
//     // {[item.name]:values[item.name] }
//   itemsFieldsValues.map((item , i)=> {
//       console.log(i,"p")
//       if(item.yupType == "string" && item.yupType == true){
//         obj[item.name] = yup.string().required("required")
//       }
//     //    obj[item.name] = thisItem[item.name]
//        arr.push(obj)

//   })
//      return arr[0] 
// }
const checkoutSchema = yup.object().shape({
  name: yup.string().required("required"),
  parent: yup.string().required("required"),
});
export default EditFactory;