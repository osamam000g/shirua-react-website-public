import DropZone from "components/DropZone";
import DeliveryBox from "components/icons/DeliveryBox";
import DashboardPageHeader from "components/layout/DashboardPageHeader";
import { Alert, Box, Button, Card, Checkbox, CircularProgress, FormControlLabel, Grid, MenuItem, TextField, Typography } from "@mui/material";
import { Formik } from "formik";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import * as yup from "yup";
import { db} from "../../../firebase"
import { addDoc, collection, updateDoc , serverTimestamp , setDoc ,doc  } from "firebase/firestore";
import { useRouter } from "next/router";
import { getStorage, ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { DatePicker, LocalizationProvider } from "@mui/lab";
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';


import DeleteIcon from '@mui/icons-material/Delete';

const EditCat = (props) => {
//idDoc
//item name 
    const collectionType =props.collectionType
    const collectionLink =props.collectionLink
    const itemsFieldsValues =props.itemsFieldsValues
    const storage = getStorage();
   
    const [ExpireValue, setExpireValue] = useState(new Date());
    const [AcceptedFiles, setAcceptedFiles] = useState([]);
    const [CatArr, setCatArr] = useState([]);
    const [showLoading, setshowLoading] = useState(false);
    const [showLoadingUpload, setshowLoadingUpload] = useState(false);
    const [showSuccessMessage, setshowSuccessMessage] = useState(false);
    const [discountType, setdiscountType] = useState("Fixed Discount");

    const [checked, setChecked] = useState([false, false]);
    const [checked2, setChecked2] = useState([]);
    // const [CheckBoxArrayFromParent, setCheckBoxArrayFromParent] = useState([]);
    const [ModifyedARR, setModifyedARR] = useState([]);
  

    
    const router = useRouter()

    // useEffect(()=>{

    //   itemsFieldsValues.map((item , i)=> 
    //   {
    //   if(item.yupType == "selectBox"){
    //       // 
    //       setCheckBoxArrayFromParent(item.values)
    //     }
    //   })
  
    // },[])

    useEffect(()=>{
      let arr =[]
      const obj = {};
  
      // obj[itemo.name] = event.target.checked 
      obj["name"] = ""
      obj["nameinarabic"] = ""
   
      arr.push(obj)
      setChecked2(arr);
  
  
    },[])


// Upload Image Function ===========================
const uploadImageFun = (file) =>{
  if(file.length == 0)
  {
       console.log("unAccepted file")
  }else
  {

        setshowLoadingUpload(true)
        const metadata = {
            contentType: 'image/jpeg'
          };
        const storageRef = ref(storage, 'images/' + file[0].name);
        const uploadTask = uploadBytesResumable(storageRef, file[0], metadata);

        uploadTask.on('state_changed',
        (snapshot) => {

          const progress1 = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log('Upload is ' + progress1 + '% done');
          switch (snapshot.state) {
            case 'paused':
              console.log('Upload is paused');
              break;
            case 'running':
              console.log('Upload is running');
              break;
          }
        }, 
        (error) => {
          switch (error.code) {
            case 'storage/unauthorized':
              console.log(error.code)
              break;
            case 'storage/canceled':
              console.log(error.code)
              break;
            case 'storage/unknown':
              console.log(error.code)
              break;
          }
        }, 
        () => {
          setshowLoadingUpload(false)

          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
              console.log('File available at', downloadURL);
              const thisFile = file
              Object.assign(thisFile[0],{url:downloadURL})
              setAcceptedFiles( prevArray => [...prevArray, thisFile])  
          });
        }
                    ); // on upload task fun
  }//else 
}// upload fun ends here ==============================

const removeFile = (i)=>{// Remove  Image File from state ======================
  let valueToRemove = [AcceptedFiles[i]];
  setAcceptedFiles(AcceptedFiles.filter(element => !valueToRemove.includes(element)))
}

const files1 =  AcceptedFiles.map((file , i) => { //view Images Function =================
      return (
      <li key={i}>
      <img src={file[0].url} style={{width:"100px"}}alt="Logo" />
        {file[0].path}     - {file[0].size} bytes 
      <Typography onClick={()=>removeFile(i)} color="primary" bgcolor={"primary.light"} lineHeight="1" px={2} mt={ 1.25} mb={2}>
      Remove Image
    </Typography>
      </li>
)}
);

const getPayloadSet =(values) =>{ //Get Payload ===========
  let arr = [];
  const obj = {};
  const objImage = {};
  itemsFieldsValues.map((item , i)=> 
  {
    if(item.yupType == "string" || item.yupType == "number"  ) 
    {
     obj[item.name] = values[item.name]
     arr.push(obj)
    }if(item.yupType == "hidden"   )
    {
     obj[item.name] = item.values
     arr.push(obj)
    }if(item.yupType == "createdAt"   )
    {
     obj[item.name] = serverTimestamp()
     arr.push(obj)
    }if(item.yupType == "date"   ){
      obj[item.name] = ExpireValue
      arr.push(obj)
    }
    if(item.yupType == "selectone") 
    {
      obj[item.name] = values[item.name]
      item.options.map((option)=>{
        obj[option] =values[option]
      })
      arr.push(obj)
    }

  })
  AcceptedFiles.map((item ,i )=>{
    console.log("image"+i , item[0].url)
    objImage[i] = item[0].url
    
    })
  obj["images"] = objImage
  arr.push(obj)

  return arr[0] 
} //Get Payload Ends Here --------------------------

//  getInitialValues ===============================
const getInitialValues  = () =>
{
  let arr = [];
  const obj = {};
  itemsFieldsValues.map((item , i)=> 
  {
    if(item.yupType == "string" || item.yupType == "number" || item.yupType == "selectone")
    {
        
      if(item.placeholder == "category name"){
        obj[item.name] =   typeof(checked2[0]) !== 'undefined' && checked2[0] != null ?  checked2[0].name :""
        // console.log("category name")
      }else if(item.placeholder == "اسم التصنيف بالعربي"){
        // console.log("اسم التصنيف بالعربي")

        obj[item.name] = typeof(checked2[0]) !== 'undefined' && checked2[0] != null ?  checked2[0].nameinarabic :""
      }
    // console.log(checked2[0] ,"checked2")
    // console.log(checked2[0].nameinarabic ,"checked2")
     arr.push(obj)
    }
    if( item.yupType == "selectone")
    {
      obj[item.name] = ""
      item.options.map((option)=>{
        obj[option] =""
      })
      arr.push(obj)
      
    }if(item.yupType == "selectBox"){
      // 
      // setCheckBoxArrayFromParent(item.values)
    }
  })
   return arr[0] 
}; //  getInitialValues ends here -------------------

const getYupValues = () => //Get Yup  ===========
{
  let arr = [];
  const obj = {};
  itemsFieldsValues.map((item , i)=> 
  {
    // console.log(i,"p")
    if(item.yupType=="string" && item.yupReqired == true)
    {
      obj[item.name] = yup.string().required("required")
    }
    else if(item.yupType=="number" && item.yupReqired == true)
    {
      obj[item.name] = yup.number().required("required")
    } else if(item.yupType=="selectone" && item.yupReqired == true)
    {
      obj[item.name] = yup.string().required("required")
      item.options.map((option)=>{
        obj[option] = yup.number()
      })
    }

     arr.push(obj)

  })
   return yup.object().shape(arr[0])
}// Get Yup ends here -------------------

  const handleFormSubmit = async (values , resetForm) => 
  {
    setshowLoading(true)
    if(collectionType  == "categories"){


    // console.log("collection tyep" , collectionType)
 ModifyArr(values , resetForm )


    }else{
    const collectionSet = collection(db, collectionType)
    const payloadSet = getPayloadSet(values)
    const docRef = await addDoc( collectionSet ,payloadSet );

     if(docRef.id)
     {
      await updateDoc(docRef, 
       {
        "idDoc": docRef.id,
       });
        setshowLoading(false)
        setshowSuccessMessage(true)
        resetForm()
        setAcceptedFiles([])
     }
    }

  };
  useEffect(()=>{
// console.log(ModifyedARR, "ModifyedARR")
  },[ModifyedARR])


  useEffect(()=>{
  // console.log(CatArr , "CatArr")
}
  ,[CatArr])

 
  const handleChange1 = (event) => {
    setChecked([event.target.checked, event.target.checked]);
  };

  const handleChange2 = (event , i , itemo) => {
 
    let arr =[]
    const obj = {};

    obj[itemo.name] = event.target.checked 
    obj["name"] = itemo.name
    obj["nameinarabic"] = itemo.nameinarabic
 
    arr.push(obj)
    setChecked2(arr);
  };
  

  const CheckBoxArrayFromParent1 = () =>{
    let arr =[]
    itemsFieldsValues.map((item , i)=> 
    {
    if(item.yupType == "selectBox"){
        // 
        arr.push( item.values)
      }
    })
    return arr[0] 

  }

  const ModifyArr = async (values, resetForm) =>
  {

    const arr = checked2
    let newArr = []

    const getItsChild = (item , arrParent) =>
    {
      for (let i = 0; i < item.children.length; i++) 
      {
        if(arr[0].name == item.children[i].name )
        {


          console.log("found" , item.children[i].name )

            if(typeof(item.children[i].children ) !== 'undefined' && item.children[i].children != null){
              let childobj = []
              for (let i2 = 0; i2 < arrParent.children[i].children.length   ; i2++) {
                 const objk = arrParent.children[i].children[i2]
                 childobj.push(objk) 
              }
              console.log(childobj ,"shi")
              arrParent.children[i] = {name: values.name, nameinarabic:values.nameinarabic, children:childobj}

              // arrParent.children[i].children[arrParent.children[i].children.length] = arrParent.children[i].children[arrParent.children[i].children.length]
            }else{
              arrParent.children[i] = {name:values.name, nameinarabic:values.nameinarabic}
            }
           
            console.log( item  , "arrParent.children ")

        }else
        {
            if(typeof(item.children[i].children ) !== 'undefined' && item.children[i].children != null )
            {
                arrParent.children[i] = item.children[i]
                getItsChild(item.children[i] , arrParent.children[i])
            } 
        }
      }
    }



const CheckBoxArrayFromParent = CheckBoxArrayFromParent1()
    for (let i = 0; i < CheckBoxArrayFromParent.length; i++) 
    {

        if(arr[0].name == CheckBoxArrayFromParent[i].name )
        {
 
          newArr[i] = CheckBoxArrayFromParent[i]

        }else{
          if(typeof(CheckBoxArrayFromParent[i].children ) !== 'undefined' && CheckBoxArrayFromParent[i].children != null ){
               newArr[i] = CheckBoxArrayFromParent[i]
               getItsChild(CheckBoxArrayFromParent[i] , newArr[i])
          } 
          //search in child func
        }

    }
    console.log(CheckBoxArrayFromParent1() , arr[0] ,"newArr[i]")

    const collectionSetq = doc(db, "categories" , "8ACMKJCMEuweyzv2RAPj")
    const docRefq = await setDoc( collectionSetq ,newArr[0] );


        setshowLoading(false)
        setshowSuccessMessage(true)
        // resetForm()
        setChecked2([])
        setAcceptedFiles([])
    //  }
    console.log(newArr , "newarr")


  }

  const handleChange3 = (event) => {
    setChecked([checked[0], event.target.checked]);
  };

  const DeleteSelectedItem = () =>{
    const arr = checked2 // slected 
    let newArr = []

    const getItsChild = (item , arrParent) =>
    {
      for (let i = 0; i < item.children.length; i++) 
      {
        if(arr[0].name == item.children[i].name )
        {
          console.log("found" , item.children[i].name )

            if(typeof(item.children[i].children ) !== 'undefined' && item.children[i].children != null){

              let childobj = []
              for (let i2 = 0; i2 < arrParent.children[i].children.length   ; i2++) {
                 const objk = arrParent.children[i].children[i2]
                 childobj.push(objk) 
              }
           
              if(typeof(item.children[i+1]) !== 'undefined' && item.children[i+1] != null){
                for ( let i3 = i+1; i3 < arrParent.children.length   ; i3++) {
                      const objk2 = item.children[i3]
                      childobj.push(objk2) 
                    }
              }

              

              console.log(childobj ,"shi")
              arrParent.children = childobj
            }else{
              // arrParent.children[i] = {name:values.name, nameinarabic:values.nameinarabic}

              // let childobj = []
              // for (let i2 = 0; i2 < arrParent.children[i].children.length   ; i2++) {
              //    const objk = arrParent.children[i].children[i2]
              //    childobj.push(objk) 
              // }
              // console.log(childobj ,"shi")
              // arrParent.children = childobj[0]

              arrParent.children.splice(i ,1)

            }
           
            console.log( item  , "arrParent.children ")

        }else
        {
            if(typeof(item.children[i].children ) !== 'undefined' && item.children[i].children != null )
            {
                arrParent.children[i] = item.children[i]
                getItsChild(item.children[i] , arrParent.children[i])
            } 
        }
      }
    }

    const CheckBoxArrayFromParent = CheckBoxArrayFromParent1()

    for (let i = 0; i < CheckBoxArrayFromParent.length; i++) 
    {

        if(arr[0].name == CheckBoxArrayFromParent[i].name )
        {
 
          newArr[i] = CheckBoxArrayFromParent[i]

        }else{
          if(typeof(CheckBoxArrayFromParent[i].children ) !== 'undefined' && CheckBoxArrayFromParent[i].children != null ){
               newArr[i] = CheckBoxArrayFromParent[i]
               getItsChild(CheckBoxArrayFromParent[i] , newArr[i])
          } 
          //search in child func
        }

    }
    



    console.log("clicked" , arr)
    
    setshowLoading(false)
    setshowSuccessMessage(true)
    // resetForm()
    setChecked2([])
    setAcceptedFiles([])
//  }
console.log(newArr , "newarr")
  }




  return <div>
      <DashboardPageHeader title={"Add "+collectionType} icon={DeliveryBox} button={

          <Link href={collectionLink} passHref> 
            <Button color="primary" sx={{ bgcolor: "primary.light",px: "2rem"}}>
              Back to items List
            </Button>
          </Link>} />


          { showLoading ?  <CircularProgress /> :<span></span>}
          {  showSuccessMessage ?  <Alert severity="success" onClose={() => {setshowSuccessMessage(false)}}> Saved </Alert> :<span></span>}

      <Card sx={{p: "30px"}}>
      <Button onClick={()=>DeleteSelectedItem()} startIcon={<DeleteIcon />} style={{float:"right"}} variant="contained" color="primary"  sx={{mt: "25px"}}>Delete Item </Button>

        <Formik  enableReinitialize={true} initialValues={getInitialValues()} validationSchema={getYupValues()} onSubmit={(values ,  { resetForm })=>handleFormSubmit(values ,   resetForm )}>
          {({values,errors,touched,handleChange,handleBlur,handleSubmit,resetForm
              }) => <form onSubmit={handleSubmit}>
                       <Grid container spacing={3}>
                          { itemsFieldsValues.map((item , i)=> 
                          {
                              if(item.type=="text" || item.type == "number" )
                              {
                                return(
                                <Grid key={i} item sm={6} xs={12}>
                                    <TextField name={item.name} label={item.label}  placeholder={item.placeholder}  fullWidth onBlur={handleBlur} onChange={handleChange} value={values[item.name] || ""} error={!!touched[item.name] && !!errors[item.name]} helperText={touched[item.name] && errors[item.name]} />
                                </Grid>
                                      )
                              }
                              else if(item.type=="select")
                              {
                                return(
                                  <Grid key={i} item sm={6} xs={12}>
                                    <TextField name={item.name} label={item.label} placeholder={item.placeholder} fullWidth select onBlur={handleBlur} onChange={handleChange} value={values[item.name] || ""} error={!!touched[item.name] && !!errors[item.name]} helperText={touched[item.name] && errors[item.name]}>
                                          {item.values.map((val,i)=>{return (<MenuItem key={i} value={val}>{val}</MenuItem>)})}
                                                 
                                  </TextField>
                                  </Grid>
                                      )
                              }
                              else if(item.type == "textArea")
                              {
                                return(
                                <Grid key={i} item xs={12}>
                                  <TextField name={item.name} label={item.label} placeholder={item.placeholder} rows={6} multiline fullWidth onBlur={handleBlur} onChange={handleChange} value={values[item.name] || ""} error={!!touched[item.name] && !!errors[item.name]} helperText={touched[item.name] && errors[item.name]} />
                                </Grid>
                                      )
                              }
                              else if(item.type == "image")
                              {
                                return(
                                  <Grid key={i} item xs={12}>
                                    <DropZone AcceptedFilesLen={AcceptedFiles.length} maxFiles={4}
                                    onChange={files => {uploadImageFun(files) }}  />
                            
                                    {files1}
                                    {/* { AcceptedFiles.length > 0 ? proccess+"%" : ""} */}
                                    { showLoadingUpload ?  <CircularProgress /> :<span></span>}
                                    { AcceptedFiles.length < 2 ? "" :"Max Upload image per product is 4 images"}
                                
                                  </Grid>
                                      )
                              }else if(item.type == "date")
                              {
                                return(
                                  <Grid key={i} item xs={6}>
                                  <LocalizationProvider dateAdapter={AdapterDateFns}>
                                    <DatePicker
                                      label="Expire Date"
                                      value={ExpireValue}
                                      onChange={(newValue) => {
                                        setExpireValue(newValue);
                                        console.log(newValue);
                                      }}
                                      renderInput={(params) => <TextField {...params} />}
                                    />
                                  </LocalizationProvider>  
                                
                                  </Grid>
                                      )
                              }else if(item.type == "selectone")
                              {
                                console.log(item.options.length , "options")
                             return   (
                              <Grid key={i} item xs={12}>
                           
                                      <TextField name={item.name} label={item.label} placeholder={item.placeholder} fullWidth select onBlur={handleBlur} onChange={(ev)=>{handleChange(ev) ; setdiscountType(ev.target.value)}} value={values[item.name] || ""} error={!!touched[item.name] && !!errors[item.name]} helperText={touched[item.name] && errors[item.name]}>
                                          {item.options.map((val,i)=>{return (<MenuItem key={i} value={val}>{val}</MenuItem>)})}
                                  </TextField>

                               {item.options.map((option , i)=>{
                                console.log(option, "inside")

                                  return(
                                  <Grid key={i} item xs={12}>
                                 <br />
                                      <Grid   item sm={6} xs={12}>
                                        <TextField style={{display:discountType === option ?"block" : "none"}} name={option} label={option}  placeholder={option}  fullWidth onBlur={handleBlur} onChange={handleChange} value={values[option] || ""} error={!!touched[option] && !!errors[option]} helperText={touched[item.name] && errors[option]} />
                                    </Grid>
                                  </Grid>
                                      )
                                })}

                              </Grid>
                                )
                                
                              }else if (item.type == 'selectBox')//one
                              {
                                {/* console.log(item.values[0].children , "val") */}
                               

                                const getChild = (itemo ) =>{
                                  {/* console.log(itemo , "itemo") */}
                                // setChecked((old)=>[...old, {idDoc:itemo.idDoc ,state:false}])
                                  return itemo.children.map((itemo , i)=>(
                                       <Box key={i} sx={{ display: 'flex', flexDirection: 'column', ml: 3 }}>     
                                        <FormControlLabel
                                        label={itemo.name}
                                        control={<Checkbox 
                                          checked={typeof(checked2[0]) !== 'undefined' && checked2[0] != null && typeof(checked2[0][itemo.name]) !== 'undefined' && checked2[0][itemo.name] != null ? checked2[0][itemo.name] : checked[0]}
                                          onChange={(ev)=>handleChange2(ev , i,itemo)} />}
                                        />
                                             {typeof(itemo.children ) !== 'undefined' && itemo.children != null ?getChild(itemo):(<></>)}
                                      </Box>
                                      ))
                                  
                                }                              
                                    return (
                                          item.values.map((itemo , i )=>{
                                          return<Grid key={i} item sm={12} xs={12}>
                                              
                                            <div>
                                              <FormControlLabel
                                                label={itemo.name}
                                                control={
                                                  <Checkbox
                                                  checked={typeof(checked2[0]) !== 'undefined' && checked2[0] != null && typeof(checked2[0][itemo.name]) !== 'undefined' && checked2[0][itemo.name] != null ? checked2[0][itemo.name] : checked[0]}
                                                  // indeterminate={checked2[i] !== checked2[i]}
                                                    onChange={(ev)=>handleChange2(ev , i,itemo)}
                                                  />
                                                }
                                              />
                                             {typeof(itemo.children ) !== 'undefined' && itemo.children != null ?getChild(itemo):(<></>)}
                                                
                                            </div>
                                        </Grid>}
                                   ) 
                                 )
                              }
                    
                  
                          })}  


                          
                          {/* <DatePicker
                              disableFuture
                              label="Responsive"
                              openTo="year"
                              views={['year', 'month', 'day']}
                              value={ExpireValue}
                              onChange={(newValue) => {
                                setExpireValue(newValue);
                              }}
                              renderInput={(params) => <TextField {...params} />}
                            />   */}
                        </Grid>

                       <Button variant="contained" color="success" style={{color:"white"}} type="submit" sx={{mt: "25px"}}>Save item </Button>
<br />

                  </form>}

        </Formik>

      </Card>

    </div>;
};

// const initialValues = { //  change 5 init value 
//   name: "",
//   parent: ""
// };
// const checkoutSchema = yup.object().shape({
//   name: yup.string().required("required"),
//   parent: yup.string().required("required"),
// });
export default EditCat;