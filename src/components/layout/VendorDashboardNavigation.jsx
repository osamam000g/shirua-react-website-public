import FlexBox from "components/FlexBox";
import Dashboard from "@mui/icons-material/Dashboard";
import NoteAdd from "@mui/icons-material/NoteAdd";
import Settings from "@mui/icons-material/Settings";
import ShoppingCart from "@mui/icons-material/ShoppingCart";
import LoyaltyIcon from '@mui/icons-material/Loyalty';
import PersonIcon from '@mui/icons-material/Person';
import PersonAddAltIcon from '@mui/icons-material/PersonAddAlt';
import ShoppingBagIcon from '@mui/icons-material/ShoppingBag';

import { useRouter } from "next/router";
import React from "react";
import { DashboardNavigationWrapper, StyledDashboardNav } from "./DashboardStyle";

const VendorDashboardNavigation = () => {
  const {
    pathname
  } = useRouter();
  return <DashboardNavigationWrapper sx={{
    px: "0px",
    pb: "1.5rem",
    color: "grey.900"
  }}>
      {linkList.map(item => <StyledDashboardNav style={{marginLeft:item.isChild?"40px":"0"
       , paddingTop:item.isChild?"0":"20px"
       , 
        //  marginBottom: item.isChild?"1px solid rgb(212, 212, 212)":"none" ,
         borderTop: item.isChild?"none":"1px solid rgb(212, 212, 212)"}} isCurrentPath={pathname.includes(item.href)} href={item.href} key={item.title}>
          <FlexBox alignItems="center">
        {  item.isChild? <></> : <item.icon className="nav-icon" fontSize="small" color="inherit" sx={{mr: "10px"}} />}

            <span>{item.title}</span>
          </FlexBox>
          <span>{item.count}</span>
        </StyledDashboardNav>)}
    </DashboardNavigationWrapper>;
};

const linkList = [{
  href: "/vendor/dashboard",
  title: "Dashboard",
  icon: Dashboard,
  isChild:false
}, {
  href: "/vendor/products",
  title: "Products",
  icon: ShoppingBagIcon,
  isChild:false

  // count: 300
}, {
  href: "/vendor/add-product",
  title: "Add New Product",
  icon: "ShoppingBagIcon",
  isChild:true

}, {
  href: "/vendor/categories",
  title: "Manage Categories",
  icon: NoteAdd,
  isChild:false

  // count: 300

},{
  href: "/vendor/add-category",
  title: "Add New Category",
  icon: NoteAdd,
  isChild:true

},{
  href: "/vendor/orders",
  title: "Orders",
  icon: ShoppingCart,
  isChild:false
}, {
  href: "/vendor/add-orders",
  title: "Add New Order",
  icon: ShoppingCart,
  isChild:true

}, {
  href: "/vendor/coupons",
  title: "coupons",
  icon: LoyaltyIcon,
  isChild:false

}, {
  href: "/vendor/add-coupons",
  title: "Add New coupon",
  icon: LoyaltyIcon,
  isChild:true

},{
  href: "/vendor/account-settings",
  title: "Account Settings",
  icon: Settings,
  isChild:false

},{
  href: "/vendor/users",
  title: "users",
  icon: PersonIcon,
  isChild:false

},{
  href: "/vendor/add-users",
  title: "add users",
  icon: PersonAddAltIcon,
  isChild:true

}];
export default VendorDashboardNavigation;