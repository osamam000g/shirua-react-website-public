import { Container, Grid } from "@mui/material";
import React, { useEffect, useState } from "react";
import Navbar from "../navbar/Navbar";
import AppLayout from "./AppLayout";
import VendorDashboardNavigation from "./VendorDashboardNavigation";

import { collection, onSnapshot } from "firebase/firestore";
import {  auth, db } from "../../../firebase"
import { onAuthStateChanged } from "firebase/auth";

const VendorDashboardLayout = ({
  children
}) => {
  const [IsUthUser, setIsUthUser] = useState([]);
  const [UserMetaData, setUserMetaData] = useState([]);
  const [PremissionW, setPremissionW] = useState("");


  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      console.log("enter")
      if (user) {
      console.log(" is user" , user)
        const uid = user.uid;
        return setIsUthUser(user)
        // changeUSerState(user)
       
     
      } else {
 
        console.log("no user") 
        return setIsUthUser([])
 
      }
 
 
    });
    
  },[])

  useEffect(()=>{
    onSnapshot(collection(db,"userd"),(users)=> users.docs.map((user)=>{
    if(user.data().userid ==  IsUthUser.uid ){
     console.log(user.data() , "as")
     setUserMetaData(user.data())
     if(user.data().role == "v"){
      setPremissionW("")
     }else{
      setPremissionW("ليس لديك صلاحيه ")
  
     }
    }
  }
  ))
  
  },[IsUthUser])

return <AppLayout navbar={<Navbar />}>
    <Container sx={{
    my: "2rem"
  }}>
 { typeof(IsUthUser.email ) !== 'undefined' && IsUthUser.email != null &&  UserMetaData.role  === "v"  ?
      <Grid container spacing={3}>
        <Grid item lg={3} xs={12} sx={{
        display: {
          xs: "none",
          sm: "none",
          md: "block"
        }
      }}>
          <VendorDashboardNavigation />
        </Grid>
        <Grid item lg={9} xs={12}>
          {children}
          {/* <children /> */}
        </Grid>
      </Grid>:<div  style={{textAlign:"center" , height:"210px" , paddingTop:"50px" , fontSize:"30px"}}> {PremissionW}  </div>


}
    </Container>
  </AppLayout>
  };

export default VendorDashboardLayout;