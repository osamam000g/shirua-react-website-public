import BazarButton from "components/BazarButton";
import Image from "components/BazarImage";
import CategoryMenu from "components/categories/CategoryMenu";
import FlexBox from "components/FlexBox";
import Category from "components/icons/Category";
import ShoppingBagOutlined from "components/icons/ShoppingBagOutlined";
import MiniCart from "components/mini-cart/MiniCart";
import Login from "components/sessions/Login";
import { useAppContext } from "contexts/app/AppContext";
import KeyboardArrowDown from "@mui/icons-material/KeyboardArrowDown";
import PersonOutline from "@mui/icons-material/PersonOutline";
import { Badge, Box, Container, Dialog, Drawer, IconButton, styled, useMediaQuery } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { layoutConstant } from "utils/constants";
import clsx from "clsx";
import Link from "next/link";
import React, { useCallback, useEffect, useState } from "react";
import SearchBox from "../search-box/SearchBox"; // component props interface
import {  onAuthStateChanged, signOut } from "firebase/auth";

import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import Divider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import PersonAdd from '@mui/icons-material/PersonAdd';
import Settings from '@mui/icons-material/Settings';
import Logout from '@mui/icons-material/Logout';



import { auth, db } from "../../../firebase"
import { useRouter } from "next/router";
import { collection, onSnapshot } from "firebase/firestore";
// styled component
export const HeaderWrapper = styled(Box)(({
  theme
}) => ({
  position: "relative",
  zIndex: 1,
  height: layoutConstant.headerHeight,
  background: theme.palette.background.paper,
  transition: "height 250ms ease-in-out",
  [theme.breakpoints.down("sm")]: {
    height: layoutConstant.mobileHeaderHeight
  }
}));

const Header = ({
  isFixed,
  className
}) => {
  const [sidenavOpen, setSidenavOpen] = useState(false);
  const [IsUthUser, setIsUthUser] = useState([]);
  const [UserMetaData, setUserMetaData] = useState([]);
  const [dialogOpen, setDialogOpen] = useState(false);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("xs"));
  const router = useRouter();

  const toggleSidenav = () => setSidenavOpen(!sidenavOpen);

  const toggleDialog = () => setDialogOpen(!dialogOpen);

  const {
    state,
    dispatch
  } = useAppContext();
  const {
    cartList
  } = state.cart;
  const {
    userData
  } = state.user;


// const userFromRedux = state.user.userData

// const changeUSerState =   useCallback((user) => () => {
//   console.log("enter");
//   dispatch({
//     type: "TOGGLE_USER",
//     payload: {
//       user: user
//     }
//   });
// }, []);
const [language, setLanguage] = useState("");
useEffect(() => {
  const lango = localStorage.getItem("language")
  setLanguage(lango)
// console.log("r",UserMetaData.role)
})

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        // const uid = user.uid;
      return  setIsUthUser(user)     
      } else {
        return  setIsUthUser([])
      }
    });  
  },[])
  useEffect(()=>{
    onSnapshot(collection(db,"userd"),(users)=> users.docs.map((user)=>{
    if(user.data().userid ==  IsUthUser.uid ){
     setUserMetaData(user.data())
    }
  }
  )) 
 },[IsUthUser])
 
 
  const [anchorElDropDown, setanchorElDropDown] = React.useState(null);
  const openVarDropDown = Boolean(anchorElDropDown);
  const handleClick = (event) => {
    setanchorElDropDown(event.currentTarget);
  };
  const handleCloseDropDown = () => {
    setanchorElDropDown(null);
  };



  const cartHandle = <Badge badgeContent={cartList.length} color="primary">
      <Box component={IconButton} ml={2.5} bgcolor="grey.200" p={1.25} onClick={toggleSidenav}>
        <ShoppingBagOutlined />
      </Box>
    </Badge>;
  return <HeaderWrapper className={clsx(className)}>
      <Container sx={{
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      height: "100%"
    }}>
        <FlexBox alignItems="center" mr={2} minWidth="170px" sx={{
        display: {
          xs: "none",
          md: "flex"
        }
      }}>
          <Link href="/">
            <a>
              <Image height={70} mb={0} mt={2} src="/assets/images/logo1.png" alt="logo" />
            </a>
          </Link>

          {isFixed && <CategoryMenu>
              <FlexBox color="grey.600" alignItems="center" ml={2}>
                <BazarButton color="inherit">
                  <Category fontSize="small" color="inherit" />
                  <KeyboardArrowDown fontSize="small" color="inherit" />
                </BazarButton>
              </FlexBox>
            </CategoryMenu>}
        </FlexBox>

        <FlexBox justifyContent="center" flex="1 1 0">
          <SearchBox />
        </FlexBox>

        <FlexBox alignItems="center" sx={{
        display: {
          xs: "none",
          md: "flex"
        }
      }}>

<div>
  <Tooltip title={language == "AR"  ? "اعدادات الحساب" :"Account settings"}>
            <IconButton
              onClick={handleClick}
              size="small"
              sx={{ ml: 2 , mr:2 }}  //to be changed
              aria-controls={openVarDropDown ? 'account-menu' : undefined}
              aria-haspopup="true"
              aria-expanded={openVarDropDown ? 'true' : undefined}
            >
              {/* <Avatar sx={{ width: 32, height: 32 }}>M</Avatar> */}
              <PersonOutline  sx={{ width: 32, height: 32 }} />
            </IconButton>
  </Tooltip>
  <Menu anchorEl={anchorElDropDown} id="account-menu" open={openVarDropDown} onClose={handleCloseDropDown} onClick={handleCloseDropDown} 
  PaperProps={{
          elevation: 0,
          sx: {overflow: 'visible',filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
          mt: 1.5,
          '& .MuiAvatar-root': {width: 32,height: 32,ml: -0.5,mr: 1, ml:1,}, //to be changed
           '&:before': {content: '""',display: 'block',position: 'absolute',top: 0,right: 14,left:14, //to be changed
           width: 10,height: 10,bgcolor: 'background.paper',transform: 'translateY(-50%) rotate(45deg)',zIndex: 0,},
          },
        }}
        transformOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}>
        <MenuItem  onClick={typeof(IsUthUser.email ) !== 'undefined' && IsUthUser.email != null? ()=>router.push( UserMetaData.role  === "c" || typeof( UserMetaData.role  ) == 'undefined' || UserMetaData.role == null ?"/profile" : "/vendor/dashboard") : toggleDialog }> <Avatar /> {typeof(IsUthUser.email ) !== 'undefined' && IsUthUser.email != null? "mohamed" : "Login"} </MenuItem>
        {typeof(IsUthUser.email ) !== 'undefined' && IsUthUser.email != null? <Divider /> : <span></span>}
        
       { typeof(IsUthUser.email ) !== 'undefined' && IsUthUser.email != null? 
        <MenuItem onClick={()=>signOut(auth).then(() => {router.reload() })}>
          <ListItemIcon>
            <Logout fontSize="small" />
          </ListItemIcon>
          {language == "AR"  ? "تسجيل الخروج" : ""}
          {language == "EN"  ? "lOGOUT" : ""}
          
        </MenuItem> :(<span></span>)}

      </Menu>
      
 </div>

          {/* <Box component={IconButton} ml={2} p={1.25} bgcolor="grey.200" onClick={typeof(IsUthUser.email ) !== 'undefined' && IsUthUser.email != null? ()=> router.push("/profile")  :  toggleDialog}>
            <PersonOutline />
          </Box> */}
          {cartHandle}
        </FlexBox>

        {/* <Dialog open={dialogOpen} fullWidth={isMobile} scroll="body" onClose={IsUthUser.length == 0 ? toggleDialog : null }> */}
        <Dialog open={ dialogOpen } fullWidth={isMobile} scroll="body" onClose={ toggleDialog }>
          <Login />
        </Dialog>
        {/* //to be changed */}
        <Drawer open={sidenavOpen} anchor={language == "AR"  ? 'left' :'right'}  onClose={toggleSidenav}> 
          <MiniCart/>
        </Drawer>
      </Container>
    </HeaderWrapper>;
};

export default Header;