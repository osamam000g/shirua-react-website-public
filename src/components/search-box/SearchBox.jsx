/* eslint-disable react-hooks/exhaustive-deps */
import BazarMenu from "components/BazarMenu";
import FlexBox from "components/FlexBox";
import KeyboardArrowDownOutlined from "@mui/icons-material/KeyboardArrowDownOutlined";
import SearchOutlined from "@mui/icons-material/SearchOutlined";
import { Box, Card, MenuItem, TextField } from "@mui/material";
import TouchRipple from "@mui/material/ButtonBase";
import { styled } from "@mui/material/styles";
import { debounce } from "@mui/material/utils";
import Link from "next/link";
import React, { useCallback, useEffect, useRef, useState } from "react"; // styled components
import { collection ,onSnapshot } from "firebase/firestore";
import { db } from "../../../firebase";
import { useRouter } from 'next/router'
// also used in the GrocerySearchBox component

export const SearchOutlinedIcon = styled(SearchOutlined)(({
  theme
}) => ({
  color: theme.palette.grey[600],
  marginRight: 6,
  marginLeft:6
})); // also used in the GrocerySearchBox component

export const SearchResultCard = styled(Card)(() => ({
  position: "absolute",
  top: "100%",
  paddingTop: "0.5rem",
  paddingBottom: "0.5rem",
  width: "100%",
  zIndex: 99
}));
const DropDownHandler = styled(FlexBox)(({
  theme
}) => ({
  borderTopRightRadius: 300, //to be changed
  // borderTopLeftRadius: 300, //to be changed
  borderBottomRightRadius: 300,
  // borderBottomLeftRadius: 300,
  whiteSpace: "pre",
  borderLeft: `1px solid ${theme.palette.text.disabled}`,
  [theme.breakpoints.down("xs")]: {
    display: "none"
  }
}));

const SearchBox = () => {
  const [category, setCategory] = useState("");
  const [resultList, setResultList] = useState([]);
  const parentRef = useRef();
const router = useRouter()

  // const [categoriesF, setcategoriesF] = useState([]);
  // useEffect(()=>{
  //   onSnapshot(collection(db,"categories"),(categoriesF)=> setcategoriesF(categoriesF.docs))
  // },[])

  const categoriesF = [{name:'All', slug:'All' , nameinarabic:"الكل" , url:"" },{ name:'Electronic', nameinarabic:"الكترونيات" , url:"" ,   },{ name:'Camera & Photo', slug:'Camera & Photo' , nameinarabic:"كاميرات" , url:"" ,   },{ name:'Car & Vehicle Electronics', slug:'Car & Vehicle Electronics' , nameinarabic:"اكسسوارات السياره" , url:"" ,   },{ name:'Computers & Accessories', slug:'Computers & Accessories' , nameinarabic:"الكمبيوتر" , url:"" ,   },{ name:'eBook Readers & Accessories', slug:'eBook Readers & Accessories' , nameinarabic:"قارات الكتب " , url:"" ,   },{ name:'Headphones, Earbuds & Accessories', slug:'Headphones, Earbuds & Accessories' , nameinarabic:"سماعات" , url:"" ,   },{ name:'Hi-Fi & Home Audio', slug:'Hi-Fi & Home Audio' , nameinarabic:"" , url:"الصوتيات" ,   },{ name:'Home Theater, TV & Video', slug:'Home Theater, TV & Video' , nameinarabic:"التلفزيون" , url:"" ,   },{ name:'Household Batteries & Chargers', slug:'Household Batteries & Chargers' , nameinarabic:"الشواحن و البطاريات" , url:"" ,   },{ name:'Mobile Phones & Communication', slug:'Mobile Phones & Communication' , nameinarabic:"الهواتف" , url:"" ,   },
  // { name:'Home Applications', slug:'Home Applications' , nameinarabic:"اجهزه المنزل" , url:"" ,   },{ name:'Dishwashers & Dish Dryers', slug:'Dishwashers & Dish Dryers' , nameinarabic:"غسالات اطباق" , url:"" ,   },{ name:'Heating & Cooling', slug:'Heating & Cooling' , nameinarabic:"تكيفات" , url:"" ,   },{ name:'Microwaves & Countertop Ovens', slug:'Microwaves & Countertop Ovens' , nameinarabic:"ميكروويف" , url:"" ,   },{ name:'Refrigerators, Freezers', slug:'Refrigerators, Freezers' , nameinarabic:"ثلاجات" , url:"" ,   },{ name:'Small Appliances', slug:'Small Appliances' , nameinarabic:"الاجهزه الصغيره" , url:"" ,   },{ name:'Washers & Dryers', slug:'Washers & Dryers' , nameinarabic:"غسالات ملابس" , url:"" ,   },
]

 const handleKeyDown = (e) =>{
  if (e.key === 'Enter') {
    const value = e.target?.value
    console.log(e.target.value  ,"v")
    if(!value){
    }else{
      // router.push(`/product/search/0/cat/0/1000/hp`)   
      router.push(`/product/search/${value}/${category}/0/0/0`)   

    }
  }
 }
  const handleCategoryChange = cat => () => {
    console.log(cat)
    setCategory(cat);
  };

  // const search = debounce(e => {
  //   const value = e.target?.value;
  //   if (!value) setResultList([]);else setResultList(dummySearchResult);
  // }, 200);
  // const hanldeSearch = useCallback(event => {
  //   event.persist();
  //   search(event);
  // }, []);

  const handleDocumentClick = () => {
    setResultList([]);
  };

  useEffect(() => {
    window.addEventListener("click", handleDocumentClick);
    return () => {
      window.removeEventListener("click", handleDocumentClick);
    };
  }, []);

  const [language, setLanguage] = useState("");
useEffect(() => {
  const lango = localStorage.getItem("language")
  setLanguage(lango)
})
useEffect(()=>{
  setCategory(language == "AR" ?"كل التصنيفات" : "All Categories")
},[])
  const categoryDropdown = <BazarMenu direction="left" handler={<DropDownHandler  alignItems="center"  bgcolor="grey.100"  px={3} color="grey.700" component={TouchRipple}>
          {/* <Box mr={0.5}>{language == "AR" ?"كل التصنيفات" : "All Categories"}</Box> */}
          <Box mr={0.5}>{category}</Box>

          <KeyboardArrowDownOutlined fontSize="small" color="inherit" />
        </DropDownHandler>}>
      {language == "AR" ?
      categoriesF.map(item => item.name ?  <MenuItem key={item.slug} onClick={handleCategoryChange(item.name)}>
          {item.nameinarabic}
        </MenuItem> : ()=> null ):
         categoriesF.map(item => <MenuItem key={item.slug} onClick={handleCategoryChange(item.name)}>
         {item.name}
       </MenuItem>)
        
        }
    </BazarMenu>;
  return <Box position="relative" flex="1 1 0" maxWidth="670px" mx="auto" {...{
    ref: parentRef
  }}>
      <TextField variant="outlined" onKeyDown={handleKeyDown} placeholder= {language == "AR" ? "ابحث عن " : "Searching for..."}  fullWidth 
      // onChange={hanldeSearch} 
      InputProps={{
      sx: {
        height: 44,
        borderRadius: 300,
        paddingRight: 0,
        paddingLeft: 0,
        color: "grey.700",
        overflow: "hidden",
        "&:hover .MuiOutlinedInput-notchedOutline": {
          borderColor: "primary.main"
        }
      },
      endAdornment: categoryDropdown,
      startAdornment: <SearchOutlinedIcon fontSize="small" />
    }} />

      {!!resultList.length && <SearchResultCard elevation={2}>
          {resultList.map((item ,i)=> <Link href={`/product/search/${item}`} key={i} passHref>
              <MenuItem key={i*2+i}>{item}</MenuItem>
            </Link>)}
        </SearchResultCard>}
    </Box>;
};

const categories = ["All Categories", "Car", "Clothes", "Electronics", "Laptop", "Desktop", "Camera", "Toys"];
const categoriesAr = ["كل التصنيفات ", "سيارات", "Clothes", "Electronics", "Laptop", "Desktop", "Camera", "Toys"];
const dummySearchResult = ["Macbook Air 13", "Asus K555LA", "Acer Aspire X453", "iPad Mini 3"];
export default SearchBox;