import { Box, styled } from "@mui/material";
import React, { useEffect, useState } from "react"; // styled component

const Wrapper = styled(Box)(({
  theme
}) =>{
  const [language, setLanguage] = useState("");
useEffect(() => {
  const lango = localStorage.getItem("language")
  setLanguage(lango)
// console.log("r",UserMetaData.role)
})
  return ({
  display: "none",
  position: "absolute",
  // left: "100%",
  // right: "auto",
  left:language =="AR" ?  "auto" :"100%",
  right: language  == "AR" ? "100%" : "auto",
  top: 0,
  zIndex: 99,
  "& .title-link, & .child-link": {
    color: "inherit",
    fontWeight: 600,
    display: "block",
    padding: "0.5rem 0px"
  },
  "& .child-link": {
    fontWeight: 400
  },
  "& .mega-menu-content": {
    padding: "0.5rem 0px",
    marginLeft: "1rem",
    borderRadius: 4,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[3],
    transition: "all 250ms ease-in-out"
  }
})}
);

const StyledMegaMenu = ({
  children
}) => {

  return <Wrapper className="mega-menu">{children}</Wrapper>;
};

export default StyledMegaMenu;