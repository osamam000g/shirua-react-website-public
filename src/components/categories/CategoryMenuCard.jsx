// import navigations from "data/navigations";
import { Box, styled } from "@mui/material";
import React, { useEffect, useState } from "react";
import CategoryMenuItem from "./CategoryMenuItem";
import MegaMenu1 from "./mega-menu/MegaMenu1";
import MegaMenu2 from "./mega-menu/MegaMenu2"; // component props interface

import BabyBoy from "components/icons/BabyBoy";
import BabyGirl from "components/icons/BabyGirl";
import Car from "components/icons/Car";
import Dress from "components/icons/Dress";
import Food from "components/icons/Food";
import Gift from "components/icons/Gift";
import Laptop from "components/icons/Laptop";
import MakeUp from "components/icons/MakeUp";
import Man from "components/icons/Man";
import Microphone from "components/icons/Microphone";
import MotorBike from "components/icons/MotorBike";
import Pets from "components/icons/Pets";
import PlantPot from "components/icons/PlantPot";
import TeddyBear from "components/icons/TeddyBear";
import Woman from "components/icons/Woman";
// styled component
const Wrapper = styled(Box)(({
  theme,
  position,
  open
}) => ({
  position: position || "unset",
  padding: "0.5rem ",
  left: 0,
  right: "auto",
  top: position === "absolute" ? "calc(100% + 0.7rem)" : "0.5rem",
  borderRadius: 4,
  transform: open ? "scaleY(1)" : "scaleY(0)",
  transformOrigin: "top",
  backgroundColor: theme.palette.background.paper,
  boxShadow: theme.shadows[2],
  transition: "all 250ms ease-in-out",
  zIndex: 98
}));

const CategoryMenuCard = ({
  open,
  position
}) => {
  const megaMenu = {
    MegaMenu1,
    MegaMenu2
  };
  const [language, setLanguage] = useState("");
useEffect(() => {
  const lango = localStorage.getItem("language")
  setLanguage(lango)
// console.log("r",UserMetaData.role)
})
  return <Wrapper open={open} position={position}>
      {navigations.map((item ,i) => {
      let MegaMenu = megaMenu[item.menuComponent];
      return <CategoryMenuItem title={language =="AR" ? item.nameinarabic :item.title} href={`/product/search/%20/${item.slug}/0/0/0`} icon={item.icon} caret={!!item.menuData} key={item.nameinarabic}>
            <MegaMenu data={item.menuData || {}} />
          </CategoryMenuItem>;
    })}
    </Wrapper>;
};

CategoryMenuCard.defaultProps = {
  position: "absolute"
};
// { title: 'Electronic', slug: 'Electronic',imgUrl: '/assets/images/brands/Electronic.jpg' ,nameinarabic:"الكترونيات"  ,href: "/",}
// ,{ title: 'Washers & Dryers',       slug: 'Washers & Dryers', imgUrl: '/assets/images/brands/Washers & Dryers.jpg'  ,nameinarabic:"غساله ملابس"}
// , { title: 'Small Appliances',      slug: 'Small Appliances',imgUrl: '/assets/images/brands/Small Appliances.jpg'  ,nameinarabic:"اجهزه صغيره"}
// , { title: 'Refrigerators',      slug: 'Refrigerators, Freezers',imgUrl: '/assets/images/brands/Refrigerators, Freezers.jpg' ,nameinarabic:"ثلاجات" }
// , { title: 'Microwaves ',imgUrl: '/assets/images/brands/Microwaves & Countertop Ovens.jpg' ,nameinarabic:"ميكرويف" }
// , { title: 'Heating & Cooling',      slug: 'Heating & Cooling',imgUrl: '/assets/images/brands/Heating & Cooling.jpg'  ,nameinarabic:"تكيفات"}
// , { title: 'Dishwashers',       slug: 'Dishwashers & Dish Dryers', imgUrl: '/assets/images/brands/Dishwashers & Dish Dryers.jpg'  ,nameinarabic:"غسالات اطباق"}
// , { title: 'TV & Video',      slug: 'Home Theater, TV & Video',imgUrl: '/assets/images/brands/TV & Video.jpg' ,nameinarabic:"تلفزيونات" }
// , { title: 'Headphones',      slug: 'Headphones, Earbuds & Accessoriesectronics',imgUrl: '/assets/images/brands/Headphones.jpg'  ,nameinarabic:"سماعات"}
// , { title: 'Computers',      slug: 'Computers & Accessories',imgUrl: '/assets/images/brands/Computers & Accessories.jpg'  ,nameinarabic:"كمبيوتر"}
// , { title: 'Camera',       slug: 'Camera & Photo', imgUrl: '/assets/images/brands/Camera & Photo.jpg' ,nameinarabic:"كاميرات" }

const navigations = [{
  icon: Laptop,
  title: 'Electronic', slug: 'Electronic' ,nameinarabic:"الكترونيات"  ,href: "/",
  menuComponent: "MegaMenu1",
  menuData: {
    categories: [{
      title: 'Washers',slug: 'Washers & Dryers'  ,nameinarabic:"غساله ملابس",
      subCategories: [{
        title: 'Small Appliances',slug: 'Small Appliances' ,nameinarabic:"اجهزه صغيره",
        href: "/product/search/shirt",
        imgUrl: "/assets/images/products/categories/shirt.png"
      }, {
       title: 'Refrigerators',      slug: 'Refrigerators, Freezers',imgUrl: '/assets/images/brands/Refrigerators, Freezers.jpg' ,nameinarabic:"ثلاجات" ,
       href: "/product/search/t-shirt",
        imgUrl: "/assets/images/products/categories/t-shirt.png"
      }, {
        title: 'Microwaves ',imgUrl: '/assets/images/brands/Microwaves & Countertop Ovens.jpg' ,nameinarabic:"ميكرويف" ,
        href: "/product/search/pant",
        imgUrl: "/assets/images/products/categories/pant.png"
      }, {
        title: 'Heating & Cooling',      slug: 'Heating & Cooling',imgUrl: '/assets/images/brands/Heating & Cooling.jpg'  ,nameinarabic:"تكيفات",
        href: "/product/search/underwear",
        imgUrl: "/assets/images/products/categories/t-shirt.png"
      }]
    }, 
    {
      title: "Accessories",
      nameinarabic: "Man Clothes",
     href:"",
      subCategories: [{
        title: "Belt",
      nameinarabic: "Man Clothes",

       href:"",
        imgUrl: "/assets/images/products/categories/belt.png"
      }, {
        title: "Hat",
      nameinarabic: "Man Clothes",

       href:"",
        imgUrl: "/assets/images/products/categories/hat.png"
      }, {
        title: "Watches",
      nameinarabic: "Man Clothes",

       href:"",
        imgUrl: "/assets/images/products/categories/watch.png"
      }, {
        title: "Sunglasses",
      nameinarabic: "Man Clothes",

       href:"",
        imgUrl: "/assets/images/products/categories/sunglass.png"
      }]
    },
       {
        title: "Shoes",
        nameinarabic: "Man Clothes",

       href:"",
        subCategories: [{
          title: "Sneakers",
        nameinarabic: "Man Clothes",

         href:"",
          imgUrl: "/assets/images/products/categories/sneaker.png"
        }, {
          title: "Sandals",
        nameinarabic: "Man Clothes",

         href:"",
          imgUrl: "/assets/images/products/categories/sandal.png"
        }, {
          title: "Formal",
        nameinarabic: "Man Clothes",

         href:"",
          imgUrl: "/assets/images/products/categories/shirt.png"
        }, {
          title: "Casual",
        nameinarabic: "Man Clothes",

         href:"",
          imgUrl: "/assets/images/products/categories/t-shirt.png"
        }]
      },
       {
        title: "Bags",
        nameinarabic: "Man Clothes",
       href:"",
        subCategories: [{
          title: "Backpack",
        nameinarabic: "Man Clothes",
         href:"",
          imgUrl: "/assets/images/products/categories/backpack.png"
        }, {
          title: "Crossbody Bags",
        nameinarabic: "Man Clothes",
         href:"",
          imgUrl: "/assets/images/products/categories/bag.png"
        }, {
          title: "Side Bags",
         href:"",
          imgUrl: "/assets/images/products/categories/mini-bag.png"
        }, {
          title: "Slides",
         href:"",
          imgUrl: "/assets/images/products/categories/belt.png"
        }]
      },
       {
        title: "Woman Clothes",
       href:"",
        subCategories: [{
          title: "Shirt",
         href:"",
          imgUrl: "/assets/images/products/categories/shirt.png"
        }, {
          title: "T- shirt",
         href:"",
          imgUrl: "/assets/images/products/categories/t-shirt.png"
        }, {
          title: "Pant",
         href:"",
          imgUrl: "/assets/images/products/categories/pant.png"
        }, {
          title: "Underwear",
         href:"",
          imgUrl: "/assets/images/products/categories/t-shirt.png"
        }]
      },
       {
        title: "Accessories",
       href:"",
        subCategories: [{
          title: "Belt",
         href:"",
          imgUrl: "/assets/images/products/categories/belt.png"
        }, {
          title: "Hat",
         href:"",
          imgUrl: "/assets/images/products/categories/hat.png"
        }, {
          title: "Watches",
         href:"",
          imgUrl: "/assets/images/products/categories/watch.png"
        }, {
          title: "Sunglasses",
         href:"",
          imgUrl: "/assets/images/products/categories/sunglass.png"
        }]
      },
       {
        title: "Shoes",
       href:"",
        subCategories: [{
          title: "Sneakers",
         href:"",
          imgUrl: "/assets/images/products/categories/sneaker.png"
        }, {
          title: "Sandals",
         href:"",
          imgUrl: "/assets/images/products/categories/sandal.png"
        }, {
          title: "Formal",
         href:"",
          imgUrl: "/assets/images/products/categories/shirt.png"
        }, {
          title: "Casual",
         href:"",
          imgUrl: "/assets/images/products/categories/t-shirt.png"
        }]
      }, 
      {
        title: "Bags",
       href:"",
        subCategories: [{
          title: "Backpack",
         href:"",
          imgUrl: "/assets/images/products/categories/backpack.png"
        }, {
          title: "Crossbody Bags",
         href:"",
          imgUrl: "/assets/images/products/categories/bag.png"
        }, {
          title: "Side Bags",
         href:"",
          imgUrl: "/assets/images/products/categories/mini-bag.png"
        }, {
          title: "Slides",
         href:"",
          imgUrl: "/assets/images/products/categories/belt.png"
        }]
      }
  ],
    rightImage: {
      imgUrl: "/assets/images/brands/Headphones.jpg",
     href:""
    }
  }
}, 
// {
//   icon: Laptop,
//   title: "Electronics",
//  href:"",
//   menuComponent: "MegaMenu1",
//   menuData: {
//     categories: [{
//       title: "Man Clothes",
//      href:"",
//       subCategories: [{
//         title: "Shirt",
//        href:"",
//         imgUrl: "/assets/images/products/categories/shirt.png"
//       }, {
//         title: "T- shirt",
//        href:"",
//         imgUrl: "/assets/images/products/categories/t-shirt.png"
//       }, {
//         title: "Pant",
//        href:"",
//         imgUrl: "/assets/images/products/categories/pant.png"
//       }, {
//         title: "Underwear",
//        href:"",
//         imgUrl: "/assets/images/products/categories/t-shirt.png"
//       }]
//     }, {
//       title: "Accessories",
//      href:"",
//       subCategories: [{
//         title: "Belt",
//        href:"",
//         imgUrl: "/assets/images/products/categories/belt.png"
//       }, {
//         title: "Hat",
//        href:"",
//         imgUrl: "/assets/images/products/categories/hat.png"
//       }, {
//         title: "Watches",
//        href:"",
//         imgUrl: "/assets/images/products/categories/watch.png"
//       }, {
//         title: "Sunglasses",
//        href:"",
//         imgUrl: "/assets/images/products/categories/sunglass.png"
//       }]
//     }, {
//       title: "Shoes",
//      href:"",
//       subCategories: [{
//         title: "Sneakers",
//        href:"",
//         imgUrl: "/assets/images/products/categories/sneaker.png"
//       }, {
//         title: "Sandals",
//        href:"",
//         imgUrl: "/assets/images/products/categories/sandal.png"
//       }, {
//         title: "Formal",
//        href:"",
//         imgUrl: "/assets/images/products/categories/shirt.png"
//       }, {
//         title: "Casual",
//        href:"",
//         imgUrl: "/assets/images/products/categories/t-shirt.png"
//       }]
//     }, {
//       title: "Bags",
//      href:"",
//       subCategories: [{
//         title: "Backpack",
//        href:"",
//         imgUrl: "/assets/images/products/categories/backpack.png"
//       }, {
//         title: "Crossbody Bags",
//        href:"",
//         imgUrl: "/assets/images/products/categories/bag.png"
//       }, {
//         title: "Side Bags",
//        href:"",
//         imgUrl: "/assets/images/products/categories/mini-bag.png"
//       }, {
//         title: "Slides",
//        href:"",
//         imgUrl: "/assets/images/products/categories/belt.png"
//       }]
//     }, {
//       title: "Woman Clothes",
//      href:"",
//       subCategories: [{
//         title: "Shirt",
//        href:"",
//         imgUrl: "/assets/images/products/categories/shirt.png"
//       }, {
//         title: "T- shirt",
//        href:"",
//         imgUrl: "/assets/images/products/categories/t-shirt.png"
//       }, {
//         title: "Pant",
//        href:"",
//         imgUrl: "/assets/images/products/categories/pant.png"
//       }, {
//         title: "Underwear",
//        href:"",
//         imgUrl: "/assets/images/products/categories/t-shirt.png"
//       }]
//     }, {
//       title: "Accessories",
//      href:"",
//       subCategories: [{
//         title: "Belt",
//        href:"",
//         imgUrl: "/assets/images/products/categories/belt.png"
//       }, {
//         title: "Hat",
//        href:"",
//         imgUrl: "/assets/images/products/categories/hat.png"
//       }, {
//         title: "Watches",
//        href:"",
//         imgUrl: "/assets/images/products/categories/watch.png"
//       }, {
//         title: "Sunglasses",
//        href:"",
//         imgUrl: "/assets/images/products/categories/sunglass.png"
//       }]
//     }, {
//       title: "Shoes",
//      href:"",
//       subCategories: [{
//         title: "Sneakers",
//        href:"",
//         imgUrl: "/assets/images/products/categories/sneaker.png"
//       }, {
//         title: "Sandals",
//        href:"",
//         imgUrl: "/assets/images/products/categories/sandal.png"
//       }, {
//         title: "Formal",
//        href:"",
//         imgUrl: "/assets/images/products/categories/shirt.png"
//       }, {
//         title: "Casual",
//        href:"",
//         imgUrl: "/assets/images/products/categories/t-shirt.png"
//       }]
//     }, {
//       title: "Bags",
//      href:"",
//       subCategories: [{
//         title: "Backpack",
//        href:"",
//         imgUrl: "/assets/images/products/categories/backpack.png"
//       }, {
//         title: "Crossbody Bags",
//        href:"",
//         imgUrl: "/assets/images/products/categories/bag.png"
//       }, {
//         title: "Side Bags",
//        href:"",
//         imgUrl: "/assets/images/products/categories/mini-bag.png"
//       }, {
//         title: "Slides",
//        href:"",
//         imgUrl: "/assets/images/products/categories/belt.png"
//       }]
//     }],
//     bottomImage: {
//       imgUrl: "/assets/images/promotion/offer-5.png",
//      href:""
//     }
//   }
// },
//  {
//   icon: MotorBike,
//   title: "Bikes",
//  href:"",
//   menuComponent: "MegaMenu2",
//   menuData: [{
//     icon: Man,
//     title: "Man",
//    href:"",
//     megaMenu: "MegaMenu1",
//     menuData: {
//       categories: [{
//         title: "Man Clothes",
//        href:"",
//         subCategories: [{
//           title: "Shirt",
//          href:"",
//           imgUrl: "/assets/images/products/categories/shirt.png"
//         }, {
//           title: "T- shirt",
//          href:"",
//           imgUrl: "/assets/images/products/categories/t-shirt.png"
//         }, {
//           title: "Pant",
//          href:"",
//           imgUrl: "/assets/images/products/categories/pant.png"
//         }, {
//           title: "Underwear",
//          href:"",
//           imgUrl: "/assets/images/products/categories/t-shirt.png"
//         }]
//       }, {
//         title: "Accessories",
//        href:"",
//         subCategories: [{
//           title: "Belt",
//          href:"",
//           imgUrl: "/assets/images/products/categories/belt.png"
//         }, {
//           title: "Hat",
//          href:"",
//           imgUrl: "/assets/images/products/categories/hat.png"
//         }, {
//           title: "Watches",
//          href:"",
//           imgUrl: "/assets/images/products/categories/watch.png"
//         }, {
//           title: "Sunglasses",
//          href:"",
//           imgUrl: "/assets/images/products/categories/sunglass.png"
//         }]
//       }, {
//         title: "Shoes",
//        href:"",
//         subCategories: [{
//           title: "Sneakers",
//          href:"",
//           imgUrl: "/assets/images/products/categories/sneaker.png"
//         }, {
//           title: "Sandals",
//          href:"",
//           imgUrl: "/assets/images/products/categories/sandal.png"
//         }, {
//           title: "Formal",
//          href:"",
//           imgUrl: "/assets/images/products/categories/shirt.png"
//         }, {
//           title: "Casual",
//          href:"",
//           imgUrl: "/assets/images/products/categories/t-shirt.png"
//         }]
//       }, {
//         title: "Bags",
//        href:"",
//         subCategories: [{
//           title: "Backpack",
//          href:"",
//           imgUrl: "/assets/images/products/categories/backpack.png"
//         }, {
//           title: "Crossbody Bags",
//          href:"",
//           imgUrl: "/assets/images/products/categories/bag.png"
//         }, {
//           title: "Side Bags",
//          href:"",
//           imgUrl: "/assets/images/products/categories/mini-bag.png"
//         }, {
//           title: "Slides",
//          href:"",
//           imgUrl: "/assets/images/products/categories/belt.png"
//         }]
//       }]
//     }
//   }, {
//     icon: Woman,
//     title: "Woman",
//    href:"",
//     megaMenu: 2
//   }, {
//     icon: BabyBoy,
//     title: "Baby Boy",
//    href:"",
//     megaMenu: 3
//   }, {
//     icon: BabyGirl,
//     title: "Baby Girl",
//    href:"",
//     megaMenu: "MegaMenu1"
//   }]
// },
//  {
//   icon: PlantPot,
//   title: "Home & Garden",
//  href:"",
//   menuComponent: "MegaMenu1",
//   menuData: {
//     categories: [{
//       title: "Man Clothes",
//      href:"",
//       subCategories: [{
//         title: "Shirt",
//        href:"",
//         imgUrl: "/assets/images/products/categories/shirt.png"
//       }, {
//         title: "T- shirt",
//        href:"",
//         imgUrl: "/assets/images/products/categories/t-shirt.png"
//       }, {
//         title: "Pant",
//        href:"",
//         imgUrl: "/assets/images/products/categories/pant.png"
//       }, {
//         title: "Underwear",
//        href:"",
//         imgUrl: "/assets/images/products/categories/t-shirt.png"
//       }]
//     }, {
//       title: "Accessories",
//      href:"",
//       subCategories: [{
//         title: "Belt",
//        href:"",
//         imgUrl: "/assets/images/products/categories/belt.png"
//       }, {
//         title: "Hat",
//        href:"",
//         imgUrl: "/assets/images/products/categories/hat.png"
//       }, {
//         title: "Watches",
//        href:"",
//         imgUrl: "/assets/images/products/categories/watch.png"
//       }, {
//         title: "Sunglasses",
//        href:"",
//         imgUrl: "/assets/images/products/categories/sunglass.png"
//       }]
//     }, {
//       title: "Shoes",
//      href:"",
//       subCategories: [{
//         title: "Sneakers",
//        href:"",
//         imgUrl: "/assets/images/products/categories/sneaker.png"
//       }, {
//         title: "Sandals",
//        href:"",
//         imgUrl: "/assets/images/products/categories/sandal.png"
//       }, {
//         title: "Formal",
//        href:"",
//         imgUrl: "/assets/images/products/categories/shirt.png"
//       }, {
//         title: "Casual",
//        href:"",
//         imgUrl: "/assets/images/products/categories/t-shirt.png"
//       }]
//     }, {
//       title: "Bags",
//      href:"",
//       subCategories: [{
//         title: "Backpack",
//        href:"",
//         imgUrl: "/assets/images/products/categories/backpack.png"
//       }, {
//         title: "Crossbody Bags",
//        href:"",
//         imgUrl: "/assets/images/products/categories/bag.png"
//       }, {
//         title: "Side Bags",
//        href:"",
//         imgUrl: "/assets/images/products/categories/mini-bag.png"
//       }, {
//         title: "Slides",
//        href:"",
//         imgUrl: "/assets/images/products/categories/belt.png"
//       }]
//     }, {
//       title: "Woman Clothes",
//      href:"",
//       subCategories: [{
//         title: "Shirt",
//        href:"",
//         imgUrl: "/assets/images/products/categories/shirt.png"
//       }, {
//         title: "T- shirt",
//        href:"",
//         imgUrl: "/assets/images/products/categories/t-shirt.png"
//       }, {
//         title: "Pant",
//        href:"",
//         imgUrl: "/assets/images/products/categories/pant.png"
//       }, {
//         title: "Underwear",
//        href:"",
//         imgUrl: "/assets/images/products/categories/t-shirt.png"
//       }]
//     }, {
//       title: "Accessories",
//      href:"",
//       subCategories: [{
//         title: "Belt",
//        href:"",
//         imgUrl: "/assets/images/products/categories/belt.png"
//       }, {
//         title: "Hat",
//        href:"",
//         imgUrl: "/assets/images/products/categories/hat.png"
//       }, {
//         title: "Watches",
//        href:"",
//         imgUrl: "/assets/images/products/categories/watch.png"
//       }, {
//         title: "Sunglasses",
//        href:"",
//         imgUrl: "/assets/images/products/categories/sunglass.png"
//       }]
//     }, {
//       title: "Shoes",
//      href:"",
//       subCategories: [{
//         title: "Sneakers",
//        href:"",
//         imgUrl: "/assets/images/products/categories/sneaker.png"
//       }, {
//         title: "Sandals",
//        href:"",
//         imgUrl: "/assets/images/products/categories/sandal.png"
//       }, {
//         title: "Formal",
//        href:"",
//         imgUrl: "/assets/images/products/categories/shirt.png"
//       }, {
//         title: "Casual",
//        href:"",
//         imgUrl: "/assets/images/products/categories/t-shirt.png"
//       }]
//     }, {
//       title: "Bags",
//      href:"",
//       subCategories: [{
//         title: "Backpack",
//        href:"",
//         imgUrl: "/assets/images/products/categories/backpack.png"
//       }, {
//         title: "Crossbody Bags",
//        href:"",
//         imgUrl: "/assets/images/products/categories/bag.png"
//       }, {
//         title: "Side Bags",
//        href:"",
//         imgUrl: "/assets/images/products/categories/mini-bag.png"
//       }, {
//         title: "Slides",
//        href:"",
//         imgUrl: "/assets/images/products/categories/belt.png"
//       }]
//     }]
//   }
// }, 
// {
//   icon: Gift,
//   title: "Gifts",
//  href:"",
//   menuComponent: "MegaMenu2",
//   menuData: [{
//     icon: Dress,
//     title: "Fashion",
//    href:""
//   }, {
//     icon: Laptop,
//     title: "Electronics",
//    href:""
//   }, {
//     icon: PlantPot,
//     title: "Home & Garden",
//    href:""
//   }, {
//     icon: MotorBike,
//     title: "Bikes",
//    href:""
//   }, {
//     icon: Gift,
//     title: "Gifts",
//    href:""
//   }, {
//     icon: Microphone,
//     title: "Music",
//    href:""
//   }, {
//     icon: MakeUp,
//     title: "Health & Beauty",
//    href:""
//   }, {
//     icon: Pets,
//     title: "Pets",
//    href:""
//   }, {
//     icon: TeddyBear,
//     title: "Baby Toys",
//    href:""
//   }, {
//     icon: Food,
//     title: "Groceries",
//    href:""
//   }, {
//     icon: Car,
//     title: "Automotive",
//    href:""
//   }]
// },
//  {
//   icon: Microphone,
//   title: "Music",
//  href:"",
//   menuComponent: "MegaMenu1"
// },
//  {
//   icon: MakeUp,
//   title: "Health & Beauty",
//  href:"",
//   menuComponent: "MegaMenu1"
// },
//  {
//   icon: Pets,
//   title: "Pets",
//  href:"",
//   menuComponent: "MegaMenu1"
// }, 
// {
//   icon: TeddyBear,
//   title: "Baby Toys",
//  href:"",
//   menuComponent: "MegaMenu1"
// },
//  {
//   icon: Food,
//   title: "Groceries",
//  href:"",
//   menuComponent: "MegaMenu1"
// }, 
// {
//   icon: Car,
//   title: "Automotive",
//  href:"",
//   menuComponent: "MegaMenu1"
// }
];

export default CategoryMenuCard;