import { cartInitialState, cartReducer } from "./cartReducer";
import combineReducers from "./combineReducers";
import { layoutInitialState, layoutReducer } from "./layoutReducer";
import { userInitialState, userReducer } from "./userReducer";
// import { userInitialState, userReducer } from "./userReducer";
export const initialState = {
  layout: layoutInitialState,
  cart: cartInitialState,
  user:userInitialState
};
export const rootReducer = combineReducers({
  layout: layoutReducer,
  cart: cartReducer,
  user:userReducer
});