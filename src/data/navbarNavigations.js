const navbarNavigations = [{
  title: 'Electronic',
  slug: 'Electronic',
  titleAr: "الكترونيات",
  url:"/s",
  // child: [
  //         {
  //           title: "Super Store",
  //           url: "/superstore-shop"
  //         }, 
  //         {
  //           title: "Furniture",
  //         titleAr: "اثاث",
  //           url: "/furniture-shop"
  //         }, 
  //         {
  //           title: "Grocery-v1",
  //         titleAr: "بقاله",
  //           url: "/grocery1"
  //         }, 
  //         //  
  //     ]
}, {
  title: 'Camera & Photo',
  slug: 'Camera & Photo',
  titleAr: "كاميرات",
  url:"/s",
}, {
  title: 'Car & Vehicle Electronics',
  slug: 'Car & Vehicle Electronics',
  titleAr: "اكسسوارات السياره",
  url:"/s",
}, {
  title: 'Computers & Accessories',
  slug: 'Computers & Accessories',
  titleAr: "الكمبيوتر",
  url:"/s",
}, {
  title: 'eBook Readers & Accessories',
  slug: 'eBook Readers & Accessories',
  titleAr: "قارات الكتب ",
  url:"/s",
},
//  {
//   title: 'Headphones, Earbuds & Accessories',
//   slug: 'Headphones, Earbuds & Accessories',
//   titleAr: "سماعات",
//   url:"/s",
// }, {
//   title: 'Hi-Fi & Home Audio',
//   slug: 'Hi-Fi & Home Audio',
//   titleAr: "",
//   url: "الصوتيات",
// }, {
//   title: 'Home Theater, TV & Video',
//   slug: 'Home Theater, TV & Video',
//   titleAr: "التلفزيون",
//   url:"/s",
// }, {
//   title: 'Household Batteries & Chargers',
//   slug: 'Household Batteries & Chargers',
//   titleAr: "الشواحن و البطاريات",
//   url:"/s",
// }, {
//   title: 'Mobile Phones & Communication',
//   slug: 'Mobile Phones & Communication',
//   titleAr: "الهواتف",
//   url:"/s",
// }
];
// const navbarNavigations = [
//   {
//     title: "Home",
//     titleAr: "الرئيسيه",
//     child: [
//       {
//         title: "Super Store",
//         url: "/superstore-shop"
//       }, 
//       {
//         title: "Furniture",
//       titleAr: "اثاث",
//         url: "/furniture-shop"
//       }, 
//       {
//         title: "Grocery-v1",
//       titleAr: "بقاله",
//         url: "/grocery1"
//       }, 
//       //  
//   ]
// },
//  {
//     title: "Offers",
//     titleAr: "خصوماتخ",
//     child: 
//     [
//       {
//         title: "Sale Page",
//         titleAr: "خصومات",
//         child: 
//         [
//           {
//           title: "Version 1",
//           titleAr: "خصومات2",

//             url: "/sale-page-1"
//           },
//           {
//             title: "Version 2",
//             titleAr: "خص2ومات",
//             url: "/sale-page-2"
//           }
//       ]
//     }, 
//   ]
// }, 
//   // {
//   //   title: "Vendor",
//   //   child: [{
//   //     title: "All vendors",
//   //     url: "/shops"
//   //   }, {
//   //     title: "Vendor store",
//   //     url: "/shop/34324"
//   //   }]
//   // }, 
//   // {
//   //   title: "Shop",
//   //   child: [{
//   //     title: "Search product",
//   //     url: "/product/search/mobile phone"
//   //   }, {
//   //     title: "Single product",
//   //     url: "/product/34324321"
//   //   }, {
//   //     title: "Cart",
//   //     url: "/cart"
//   //   }, {
//   //     title: "Checkout",
//   //     url: "/checkout"
//   //   }, {
//   //     title: "Alternative Checkout",
//   //     url: "/checkout-alternative"
//   //   }, {
//   //     title: "Order confirmation",
//   //     url: "/order-confirmation"
//   //   }]
//   // }

// // {
// //   title: "User Account",
// //   child: [{
// //     title: "Orders",
// //     child: [{
// //       title: "Order List",
// //       url: "/orders"
// //     }, {
// //       title: "Order Details",
// //       url: "/orders/5452423"
// //     }]
// //   }, {
// //     title: "Profile",
// //     child: [{
// //       title: "View Profile",
// //       url: "/profile"
// //     }, {
// //       title: "Edit Profile",
// //       url: "/profile/edit"
// //     }
// //   ]
// //   },
// //    {
// //     title: "Address",
// //     child: [{
// //       title: "Address List",
// //       url: "/address"
// //     }, {
// //       title: "Add Address",
// //       url: "/address/512474"
// //     }]
// //   },
// //    {
// //     title: "Support tickets",
// //     child: [{
// //       title: "All tickets",
// //       url: "/support-tickets"
// //     }, {
// //       title: "Ticket details",
// //       url: "/support-tickets/512474"
// //     }]
// //   }, 
// //   {
// //     title: "Wishlist",
// //     url: "/wish-list"
// //   }]
// // },
// //  {
// //   title: "Vendor Account",
// //   child: [{
// //     title: "Dashboard",
// //     url: "/vendor/dashboard"
// //   }, {
// //     title: "Products",
// //     child: [{
// //       title: "All products",
// //       url: "/vendor/products"
// //     }, {
// //       title: "Add/Edit product",
// //       url: "/vendor/products/248104"
// //     }]
// //   }, 
// //   {
// //     title: "Orders",
// //     child: [{
// //       title: "All orders",
// //       url: "/vendor/orders"
// //     }, {
// //       title: "Order details",
// //       url: "/vendor/orders/248104"
// //     }]
// //   },
// //    {
// //     title: "Profile",
// //     url: "/vendor/account-settings"
// //   }]
// // }, 
// // {
// //   title: "Back to Demos",
// //   url: "/"
// // }

// ];
export default navbarNavigations;

