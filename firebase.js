import { initializeApp } from 'firebase/app';
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";



const firebaseConfig = {
  apiKey: "AIzaSyCM7_ibPxJMGvIA5J4dpZAF1L8mPViprR4",
  authDomain: "shirua2.firebaseapp.com",
  projectId: "shirua2",
  storageBucket: "shirua2.appspot.com",
  messagingSenderId: "473285827970",
  appId: "1:473285827970:web:aee034fe4bfb1aa603c39b",
  measurementId: "G-5GPG76R6YM"
};


const firebaseApp = initializeApp(firebaseConfig)
const auth = getAuth(firebaseApp);
const db = getFirestore(firebaseApp);
const storage = getStorage(firebaseApp);
export { auth , db , storage} ;
// Initialize Firebase
// const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);