import DropZone from "components/DropZone";
import DeliveryBox from "components/icons/DeliveryBox";
import DashboardPageHeader from "components/layout/DashboardPageHeader";
import VendorDashboardLayout from "components/layout/VendorDashboardLayout";
import { Alert, Button, Card, CircularProgress, Grid, MenuItem, Stack, TextField } from "@mui/material";
import { Formik } from "formik";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import * as yup from "yup";
import { db} from "../../../firebase"
import { addDoc, collection, onSnapshot, updateDoc , getDoc , doc } from "firebase/firestore";
import { useRouter } from "next/router";
import AddFactory from "components/0data/AddFactory"


const OrderDetails =  () => {
  

  const [categories, setcategories] = useState([]);
  const [categories1, setcategories1] = useState([]);

  useEffect(()=>{
    // onSnapshot(collection(db,"categories"),(categories)=> setcategories(categories.docs))
    const getFun = async () =>{
      const ref = doc(db, "categories", "8ACMKJCMEuweyzv2RAPj");
      const docSnap = await getDoc(ref);
      if (docSnap.exists()) {
    console.log(docSnap.data(), "sasaasasasassa")
        setcategories1((prev)=>[...prev,docSnap.data()  ] )
      }
    }
    getFun()

  },[])

  
//   const [showLoading, setshowLoading] = useState(false);
//   const [showSuccessMessage, setshowSuccessMessage] = useState(false);

// const router = useRouter()
//   const handleFormSubmit = async (values , resetForm) => {
//     setshowLoading(true)
//     // console.log(values);
//     const collectionSet = collection(db, "categories") //collection type change 1
//     const payloadSet = {  //collection payload change 2 
//       name: values.name,
//       parent: values.category,
//     }
//     const docRef = await addDoc( collectionSet ,payloadSet );

//      if(docRef.id){
//       await updateDoc(docRef, {
//         "idDoc": docRef.id,
//     });
//        console.log(docRef.id , "id")
//         setshowLoading(false)
//         setshowSuccessMessage(true)
//         resetForm()
//      }
//   };

  return <VendorDashboardLayout>

            <AddFactory 
                collectionType="categories" collectionLink="/vendor/categories" 
                itemsFieldsValues={[
                  {name:"name" , label:"name" , placeholder:"category name" , type:"text" , values:[] , yupReqired:true , yupType:"string"},
{name:"nameinarabic" , label:"اسم التصنيف بالعربي" , placeholder:"اسم التصنيف بالعربي" , type:"text" , values:[] , yupReqired:true , yupType:"string"},     
{name:"parent" , label:"parent" , placeholder:"category parent" , type:"selectBox" , values:categories1 , yupReqired:true , yupType:"selectBox"},


                ]} />
{/* change 3 collection link  / title   */}
      {/* <DashboardPageHeader title="Add Category" icon={DeliveryBox} button={<Link href="/vendor/categories" passHref> 
            <Button color="primary" sx={{
        bgcolor: "primary.light",
        px: "2rem"
      }}>
              Back to categories List
            </Button>
          </Link>} /> */}
       {/* { showLoading ?  <CircularProgress /> :<span></span>}
      {  showSuccessMessage ?  <Alert severity="success" onClose={() => {setshowSuccessMessage(false)}}> Saved </Alert> :<span></span>}

      <Card sx={{
      p: "30px"
    }}> */}
        {/* <Formik initialValues={initialValues} validationSchema={checkoutSchema} onSubmit={(values ,  { resetForm })=>handleFormSubmit(values ,   resetForm )}>
          {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          resetForm
        }) => <form onSubmit={handleSubmit}>
              <Grid container spacing={3}>
                <Grid item sm={6} xs={12}>
                  <TextField name="name" label="Name" placeholder="Name" fullWidth onBlur={handleBlur} onChange={handleChange} value={values.name || ""} error={!!touched.name && !!errors.name} helperText={touched.name && errors.name} />
                </Grid>
                {/* change 4 item valuen fileds map  */}
                {/* <Grid item sm={6} xs={12}> 
                  <TextField name="parent" label="Select parent Category" placeholder="parent Category"
                   fullWidth select onBlur={handleBlur} onChange={handleChange} value={values.parent || ""}
                    error={!!touched.parent && !!errors.parent} helperText={touched.parent && errors.parent}>
                    <MenuItem value="none">none</MenuItem>
                    <MenuItem value="electronics">Electronics</MenuItem>
                    <MenuItem value="fashion">Fashion</MenuItem>
                  </TextField>
                </Grid> */}
      
              {/* </Grid>
              <Button variant="contained" color="primary" type="submit" sx={{
            mt: "25px"
          }}>
                Save item 
              </Button>
            </form>}
        </Formik> */}
      {/* </Card> */} 
    </VendorDashboardLayout>;
};

const initialValues = { //  change 5 init value 
  name: "",
  parent: ""
};
const checkoutSchema = yup.object().shape({
  name: yup.string().required("required"),
  parent: yup.string().required("required"),
});
export default OrderDetails;