import DropZone from "components/DropZone";
import DeliveryBox from "components/icons/DeliveryBox";
import DashboardPageHeader from "components/layout/DashboardPageHeader";
import VendorDashboardLayout from "components/layout/VendorDashboardLayout";
import { Button, Card, CircularProgress, Grid, MenuItem, TextField , Snackbar , IconButton  , CloseIcon  } from "@mui/material";
import { Formik } from "formik";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import * as yup from "yup";

import AddFactory from "components/0data/AddFactory";
import {  createUserWithEmailAndPassword, onAuthStateChanged , getAuth, reauthenticateWithCredential  } from "firebase/auth";
import { auth, db } from "../../../firebase"
import { addDoc, collection } from "firebase/firestore";
// import admin from "../../../lib/firebase-admin";


const OrderDetails = () => {
  const [IsUthUser, setIsUthUser] = useState([]);
  const [toastopen, settoastopen] = useState(false);
  const [ToastMessage, setToastMessage] = useState("");

  const handletoastClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    settoastopen(false);
  };
  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        const uid = user.uid;
      return  setIsUthUser(uid)      
      } else {
       return  setIsUthUser([])
      }
 

    });
    
  },[])
  const [showLoading, setshowLoading] = useState(false);


  const addDataToUser = async (user , values) => {
    const collectionSet = collection(db, "userd")
    const payloadSet = {
      fullname: values.name,
      email: values.email,
      phone : values.phone,
      userid:user.uid,
      role:values.role,
      isVendorActive:values.isactive == "true" ?true :false,  
      storename:values.storename,
      address:values.address
    }
   await addDoc( collectionSet ,payloadSet );
  
  }

  const handleFormSubmit = async (values , resetForm) => 
  {
    setshowLoading(true)
  
    console.log(JSON.stringify({ "uid":"IsUthUser"}));

    const response = await fetch('/api/users',{
      method:'POST',
      body: JSON.stringify({ uid: IsUthUser , values:values}),
      headers:{
        'Content-Type':'application/json'
      },
    })
    const collections = await response.json();
    console.log(collections , "data from api   ")
    if(collections.error){
      settoastopen(true)
      setToastMessage(collections.error)
      setshowLoading(false)

    }else{
      settoastopen(true)
      setToastMessage("user Created Successfully")
        resetForm()
        setshowLoading(false)
    }

  };

  return <VendorDashboardLayout>

      <DashboardPageHeader title="Add User" icon={DeliveryBox} button={<Link href="/vendor/users" passHref>
            <Button color="primary" sx={{
        bgcolor: "primary.light",
        px: "2rem"
      }}>
              Back to users List
            </Button>
          </Link>} />
          { showLoading ?  <CircularProgress /> :<span></span>}
             <Snackbar open={toastopen} autoHideDuration={6000} onClose={handletoastClose} message={ToastMessage}  />
      <Card sx={{
      p: "30px"
    }}>
        <Formik initialValues={initialValues} validationSchema={checkoutSchema} onSubmit={(values ,  { resetForm })=>handleFormSubmit(values ,   resetForm )}>
          {({values,errors,touched,handleChange,handleBlur,handleSubmit
        }) => <form onSubmit={handleSubmit}>
              <Grid container spacing={3}>
                <Grid item sm={6} xs={12}>
                  <TextField name="name" label="Name" placeholder="Name" fullWidth onBlur={handleBlur} onChange={handleChange} value={values.name || ""} error={!!touched.name && !!errors.name} helperText={touched.name && errors.name} />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField name="email" label="Email" placeholder="Email" fullWidth onBlur={handleBlur} onChange={handleChange} value={values.email || ""} error={!!touched.email && !!errors.email} helperText={touched.email && errors.email} />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField name="phone" label="Phone Number" placeholder="EX: 01287*****" fullWidth onBlur={handleBlur} onChange={handleChange} value={values.phone || ""} error={!!touched.phone && !!errors.phone} helperText={touched.phone && errors.phone} />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField name="password" type="password" label="Password " placeholder="*******" fullWidth onBlur={handleBlur} onChange={handleChange} value={values.password || ""} error={!!touched.password && !!errors.password} helperText={touched.password && errors.password} />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField name="isactive" label="Is Active Vendor" placeholder="Is Active Vendor" fullWidth select onBlur={handleBlur} onChange={handleChange} value={values.isactive || ""} error={!!touched.isactive && !!errors.isactive} helperText={touched.isactive && errors.isactive}>
                    <MenuItem value="true">Yes</MenuItem>
                    <MenuItem value="false">No</MenuItem>
                  </TextField>
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField name="role" label="Role" placeholder="Role" fullWidth select onBlur={handleBlur} onChange={handleChange} value={values.role || ""} error={!!touched.role && !!errors.role} helperText={touched.role && errors.role}>
                    <MenuItem value="c">Customer</MenuItem>
                    <MenuItem value="v">Vendor</MenuItem>
                    <MenuItem value="a">Admin</MenuItem>
                  </TextField>
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField name="storename" label="Store Name" placeholder="Store Name" fullWidth onBlur={handleBlur} onChange={handleChange} value={values.storename || ""} error={!!touched.storename && !!errors.storename} helperText={touched.storename && errors.storename} />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField name="address" label="address" placeholder="address" fullWidth onBlur={handleBlur} onChange={handleChange} value={values.address || ""} error={!!touched.address && !!errors.address} helperText={touched.address && errors.address} />
                </Grid>
             
              </Grid>
              <Button variant="contained" color="primary" type="submit" sx={{
            mt: "25px"
          }}>
                Add User
              </Button>
            </form>}
        </Formik>
      </Card>
    </VendorDashboardLayout>;
};
const changedEmail = "os@is.com"+new Date().getMilliseconds()+"m"
const initialValues = {
  name: "m",
  phone: "136546465",
  email: changedEmail,
  password: "icekiller@1",
  role: "v",
  isactive: "true",
  storename: "name",
  address: "address"
};
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const checkoutSchema = yup.object().shape({
  name: yup.string().required("required"),
  email: yup.string().email("invalid email").required("${path} is required"),
  phone: yup.string().matches(phoneRegExp, 'Phone number is not valid'),
  password: yup.string()
  .required('No password provided.') 
  .min(8, 'Password is too short - should be 8 chars minimum.')
  .matches(/[a-zA-Z]/, 'Password can only contain Latin letters.'),
  role: yup.string().required("required"),
  isactive: yup.string().required("required"),
  storename:  yup.string(),
  address:  yup.string(),
});
export default OrderDetails;