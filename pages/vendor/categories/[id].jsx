import DeliveryBox from "components/icons/DeliveryBox";
import DashboardPageHeader from "components/layout/DashboardPageHeader";
import VendorDashboardLayout from "components/layout/VendorDashboardLayout";
import { Alert, Button, Card, CircularProgress, Grid, MenuItem, TextField } from "@mui/material";
import { Formik } from "formik";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import * as yup from "yup";
import { collection, deleteDoc, doc, onSnapshot, updateDoc } from "firebase/firestore";
import { db } from "../../../firebase";
 
import DeleteIcon from '@mui/icons-material/Delete';
import SendIcon from '@mui/icons-material/Send';

import EditFactory from "components/0data/EditFactory"
const OrderDetails = () => {

//   const [showLoading, setshowLoading] = useState(true);
//   const [showSuccessMessage, setshowSuccessMessage] = useState(false);
//   const [categories, setcategories] = useState([]);
//   const [thisItem, setthisItem] = useState([]);
//   const [MessageWord, setMessageWord] = useState([]);
  

  const router = useRouter();
//   // const isMount = useIsMount();

  const {
    id
  } = router.query;

//   useEffect(()=>{
//     onSnapshot(collection(db,"categories"),(categories)=> setcategories(categories.docs))
//   },[])
//   useEffect(()=>{
    
//     for (var i = 0; i < categories.length; i++) {
//       if(i + 1 === categories.length){ // IF LAST ITEM 

//         console.log(i+1,"i")
//         console.log(categories.length,"rowLentgh")

//         if( categories[i].data().idDoc != id){
//           console.log(id , "idsd")
//           console.log(categories[i].data().idDoc , "idsw")
//           setTimeout(()=>{
//             router.push("/vendor/categories")
//           },[1000])

//           console.log(thisItem  , " redirect")           
//         }else{
//           setthisItem(categories[i].data())
//           setTimeout(()=>{
//               setshowLoading(false)
//           },[1000])
//           console.log("LAST item found", i+1)
//           break;

//         }
//       }else{
//         console.log(i+1,"i"+i+1)
//         if( categories[i].data().idDoc == id){
//           setthisItem(categories[i].data())
//           setTimeout(()=>{
//               setshowLoading(false)
//           },[1000])
//           console.log("first item", i)
//           break;

//         }
//       }
      
//     }


   
//   },[categories])

// const deleteItem = async ()=> {
//   setshowLoading(true)

//   const itemRef = doc(db, 'categories', id);
//   await deleteDoc(itemRef).then(()=>{
//     setMessageWord("categroy deleted successfully")
//     setshowLoading(false)
//     setshowSuccessMessage(true)
//     setTimeout(()=>{
//       // router.push("/vendor/categories")
//     },[2000])
//   })

 
//   }
  
//   const handleFormSubmit = async values => {
//     console.log(values);

//     setshowLoading(true)

//     const itemRef = doc(db, 'categories', id);
//     await updateDoc(itemRef,{
//       name:values.name,
//       parent:values.category
//     }).then(()=>{
//       setMessageWord("categroy updated successfully")
//       setshowLoading(false)
//       setshowSuccessMessage(true)
//       setTimeout(()=>{
//         // router.push("/vendor/categories")
//       },[2000])
//     })
//   };

  return <VendorDashboardLayout>
  <EditFactory id={id} collectionType="categories" collectionLink="/vendor/categories" 
    itemsFieldsValues={[
{name:"name" , label:"name" , placeholder:"category name" , type:"text" , values:[] , yupReqired:true , yupType:"string"},
{name:"parent" , label:"parent" , placeholder:"category parent" , type:"select" , values:["electronics" , "none" ,"fashion"] , yupReqired:true , yupType:"string"},
{name:"nameinarabic" , label:"اسم التصنيف بالعربي" , placeholder:"اسم التصنيف بالعربي" , type:"text" , values:[] , yupReqired:true , yupType:"string"},                               
 
    ]}
  />
      {/* <DashboardPageHeader title="Edit category" icon={DeliveryBox} button={<Link href="/vendor/categories" passHref>
      <Button variant="outlined" endIcon={<SendIcon />}>
           Back  to Categories list
            </Button>
          </Link>} />
          <DashboardPageHeader title=""   button={
              <Button onClick={deleteItem} color="primary" sx={{
        bgcolor: "primary.light",
        px: "2rem"
      }} endIcon={<DeleteIcon />}>
                Delete
              </Button>
           } />

          { showLoading ?  <CircularProgress /> :<span></span>}
      {  showSuccessMessage ?  <Alert severity="success" onClose={() => {setshowSuccessMessage(false)}}> {MessageWord} </Alert> :<span></span>}


      <Card sx={{
      p: "30px"
    }}>
        <Formik enableReinitialize  initialValues={{
  name: thisItem.name,
  category: thisItem.parent
}} validationSchema={checkoutSchema} onSubmit={handleFormSubmit}>
          {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit
        }) => <form onSubmit={handleSubmit}>
              <Grid container spacing={3}>
                <Grid item sm={6} xs={12}>
                  <TextField name="name" label="Name" placeholder="Name" fullWidth onBlur={handleBlur} onChange={handleChange} value={values.name || ""} error={!!touched.name && !!errors.name} helperText={touched.name && errors.name} />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField name="category" label="Select Parent Category" placeholder="Parent Category" fullWidth select onBlur={handleBlur} onChange={handleChange} value={values.category || ""} error={!!touched.category && !!errors.category} helperText={touched.category && errors.category}>
                    <MenuItem value="electronics">Electronics</MenuItem>
                    <MenuItem value="none">none</MenuItem>
                    <MenuItem value="fashion">Fashion</MenuItem>
                  </TextField>
                </Grid>
          
              </Grid>
              <Button variant="contained" color="primary" type="submit" sx={{
            mt: "25px"
          }}>
                Save category
              </Button>
            </form>}
        </Formik>
      </Card> */}
    </VendorDashboardLayout>;
};

// const initialValues = {
//   name: "",
//   category: ""
// };
// const checkoutSchema = yup.object().shape({
//   name: yup.string().required("required"),
//   category: yup.string().required("required"),
// });
export default OrderDetails;