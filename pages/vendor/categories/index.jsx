import FlexBox from "components/FlexBox";
import DeliveryBox from "components/icons/DeliveryBox";
import DashboardPageHeader from "components/layout/DashboardPageHeader";
import VendorDashboardLayout from "components/layout/VendorDashboardLayout";
import TableRow from "components/TableRow";
import { H5 } from "components/Typography";
import East from "@mui/icons-material/East";
import { Avatar, IconButton, Pagination, Typography } from "@mui/material";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { collection, doc, getDoc, getDocs ,onSnapshot } from "firebase/firestore";
import { db } from "../../../firebase";
import EditCat from "components/0data/CatListViewEdit"


const Categories = () => {
  // const [categories, setcategories] = useState([]);
  
  // useEffect(()=>{
  //   onSnapshot(collection(db,"categories"),(categories)=> setcategories(categories.docs))
  // },[])

  // useEffect(()=>{
    
  //   categories.map((cat)=>{
  //       console.log(cat.data())
  //   }) 
  // },[categories])

    const [categories1, setcategories1] = useState([]);

  useEffect(()=>{
    // onSnapshot(collection(db,"categories"),(categories)=> setcategories(categories.docs))
    const getFun = async () =>{
      const ref = doc(db, "categories", "8ACMKJCMEuweyzv2RAPj");
      const docSnap = await getDoc(ref);
      if (docSnap.exists()) {
    console.log(docSnap.data(), "sasaasasasassa")
        setcategories1((prev)=>[...prev,docSnap.data()  ] )
      }
    }
    getFun()

  },[])

  

  return <VendorDashboardLayout>
      <DashboardPageHeader
      //  title="categories" icon={DeliveryBox} 

       />

      {/* <CategoryList  categories={categories} dataLink="categories/"
  headcells={[
    {id: 'name',numeric: false,disablePadding: true,label: 'Category Name',},
  {id: 'parent',numeric: false,disablePadding: false,label: 'Parent Categoty',
  }]} /> */}


<EditCat 
                collectionType="categories" collectionLink="/vendor/categories" 
                itemsFieldsValues={[
                  {name:"name" , label:"name" , placeholder:"category name" , type:"text" , values:[] , yupReqired:true , yupType:"string"},
{name:"nameinarabic" , label:"اسم التصنيف بالعربي" , placeholder:"اسم التصنيف بالعربي" , type:"text" , values:[] , yupReqired:true , yupType:"string"},     
{name:"parent" , label:"parent" , placeholder:"category parent" , type:"selectBox" , values:categories1 , yupReqired:true , yupType:"selectBox"},


                ]} />

    </VendorDashboardLayout>;
};

const productList = [{
  orderNo: "1050017AS",
  stock: 30,
  price: 350,
  href: "/vendor/categories/5452423"
}, {
  orderNo: "1050017AS",
  stock: 20,
  price: 500,
  href: "/vendor/categories/5452423"
}, {
  orderNo: "1050017AS",
  stock: 2,
  price: 700,
  href: "/vendor/categories/5452423"
}, {
  orderNo: "1050017AS",
  stock: 25,
  price: 300,
  href: "/vendor/categories/5452423"
}, {
  orderNo: "1050017AS",
  stock: 1,
  price: 700,
  href: "/vendor/categories/5452423"
}];
export default Categories;