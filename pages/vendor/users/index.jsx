import FlexBox from "components/FlexBox";
import DeliveryBox from "components/icons/DeliveryBox";
import DashboardPageHeader from "components/layout/DashboardPageHeader";
import VendorDashboardLayout from "components/layout/VendorDashboardLayout";
import TableRow from "components/TableRow";
import { H5 } from "components/Typography";
import East from "@mui/icons-material/East";
import { Avatar, IconButton, Pagination, Typography } from "@mui/material";
import Link from "next/link";
import React, { useEffect, useState } from "react";

import { collection ,onSnapshot } from "firebase/firestore";
import { db } from "../../../firebase";
import CategoryList from "components/0data/CaregoryList"

const Products = () => {

  const [categories, setcategories] = useState([]);
  
  useEffect(()=>{
    onSnapshot(collection(db,"userd"),(categories)=> setcategories(categories.docs))
  },[])

  useEffect(()=>{
    
    categories.map((cat)=>{
        console.log(cat.data())
    })
  },[categories])

  return <VendorDashboardLayout>
     
      <CategoryList  categories={categories} dataLink="users/"
  headcells={[
    {id: 'fullname',numeric: false,disablePadding: true,label: 'Full Name',},
    {id: 'phone',numeric: false,disablePadding: false,label: 'phone',},
    {id: 'email',numeric: true,disablePadding: false,label: 'Email',},
    // {id: 'isVendorActive',numeric: false,disablePadding: false,label: 'isVendorActive',},
    ]} />
    </VendorDashboardLayout>;
};


export default Products;