import FlexBox from "components/FlexBox";
import DashboardPageHeader from "components/layout/DashboardPageHeader";
import VendorDashboardLayout from "components/layout/VendorDashboardLayout";
import TableRow from "components/TableRow";
import { H5, H6 } from "components/Typography";
import Delete from "@mui/icons-material/Delete";
import ShoppingBag from "@mui/icons-material/ShoppingBag";
import { Alert, Autocomplete, Avatar, Button, Card, CircularProgress, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, Grid, IconButton, MenuItem, Paper, Select, TextField, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { format } from "date-fns";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { addDoc, collection, doc, getDoc, onSnapshot, serverTimestamp, updateDoc } from "firebase/firestore";
import { db } from "../../../firebase";

const OrderDetails = () => {
// let total =0;
  const router = useRouter();
  const {id} = router.query;
const [ID, setID] = useState([]);

useEffect(() => {
  if (router.isReady) {
    setID(id)
  }
}, [router.isReady])


const [thisOrder, setthisOrder] = useState([]);


  
  const [categories, setcategories] = useState([]);
  const [showLoading, setshowLoading] = useState(false);
  const [showSuccessMessage, setshowSuccessMessage] = useState(false);
  
  const [ShippingFee, setShippingFee] = useState(0);
  const [Discount, setDiscount] = useState(0);
  const [total, settotal] = useState(0);
  const [subtotal, setsubtotal] = useState(0);
  const [CustomerNote, setCustomerNote] = useState("");
  const [ShippingAdress, setShippingAdress] = useState("");
  const [ProductArr, setProductArr] = useState([]);
  const [Status1, setStatus1] = useState("Processing");
  const [VendorEmail, setVendorEmail] = useState("");
  const [CustomerPhone, setCustomerPhone] = useState("");
  const [CustomerEmail, setCustomerEmail] = useState("");
  const [CustomerName, setCustomerName] = useState("");




  const [selectedItem, setselectedItem] = useState([]);
  const [size, setsize] = useState([]);
  const [color, setcolor] = useState([]);
  const [quantity, setquantity] = useState([]);
  const [open, setOpen] = useState(false);

  // const [age, setAge] = React.useState('');

  const getFun = async (ido) =>{
    if(typeof(ido) !== 'undefined' && ido != null  ){
      console.log(ido , "IDDDDDDDDDDDDDDDD")

        const ref = doc(db, "orders", ido);
        const docSnap = await getDoc(ref);

        if (docSnap.exists()) {
        console.log(docSnap.data(), "sasaasasasassa")
          setthisOrder((prev)=>[docSnap.data()] )
        setCustomerName(docSnap.data().name)   
        setCustomerPhone(docSnap.data().CustomerPhone)
        setVendorEmail(docSnap.data().VendorEmail)
        setStatus1(docSnap.data().Status1)
        // docSnap.data().ProductArr.map((item)=> setProductArr((prev)=>[...prev , item])  )
        Object.keys(docSnap.data().ProductArr).map((item)=>{
        console.log(docSnap.data().ProductArr[item] , "len")
        setProductArr((prev)=>[...prev , docSnap.data().ProductArr[item]])
        })

        setShippingAdress(docSnap.data().ShippingAdress)
        setCustomerNote(docSnap.data().CustomerNote)
        setCustomerEmail(docSnap.data().CustomerEmail)
        setsubtotal(docSnap.data().subtotal)
        settotal(docSnap.data().total)
        setDiscount(docSnap.data().Discount)
        setShippingFee(docSnap.data().ShippingFee)

    }
  }
}

  useEffect(()=>{  
  // const {id} = router.query;
  getFun(id)
  
  },[ID])
  
  useEffect(()=>{  
    console.log(thisOrder, "sasaasasasassa")
    

  

  },[thisOrder])


  const handleChangeQuantity = (event) => {
    setquantity(event.target.value);
  };
  const handleChangeSize = (event) => {
    setsize(event.target.value);
  };
  const handleChangeColor = (event) => {
    setcolor(event.target.value);
  };
  const getPayloadSet = () =>{
        let arr = [];
        let proArr = {};
        const obj = {};

        obj["VendorEmail"] = VendorEmail
        obj["ShippingFee"] = ShippingFee
        obj["Discount"] = Discount
        obj["total"] = total
        obj["subtotal"] = subtotal
        obj["CustomerNote"] = CustomerNote
        obj["ShippingAdress"] = ShippingAdress

        obj["CustomerPhone"] = CustomerPhone
        obj["CustomerEmail"] = CustomerEmail
        obj["name"] = CustomerName

        obj["ProductArr"] = {}
         ProductArr.map((pro , i)=>{
          obj["ProductArr"][i] = pro
          })

         
        // arr.push(proArr)

        console.log("pro" ,ProductArr )
        obj["Status1"] = Status1
        arr.push(obj  )
        console.log(arr , "arr")
        console.log(arr[0] , "arr[0]")

        return arr[0] 

  }
const handelOrderSubmition = async () =>{
  
  setshowLoading(true)
    const ref = doc(db, "orders", ID);
    const payloadSet = getPayloadSet()

    await updateDoc(ref, payloadSet);

      setshowLoading(false)
      setshowSuccessMessage(true)
 

          // setShippingFee(0);
          // setDiscount(0);
          // settotal(0);
          // setsubtotal(0);
          // setCustomerNote("");
          // setShippingAdress("");
          // setProductArr([]);
          // setStatus1("Processing");
          // setVendorEmail("");
          // setCustomerPhone("");
          // setCustomerEmail("");
          // setCustomerName("");

               
          
   
  
}
  const handleClickOpen = (item) => {
    setOpen(true);
    setselectedItem(item)
  };

  const handleClose = () => {
    setOpen(false);
  };


  useEffect(()=>{
    onSnapshot(collection(db,"products"),(categories)=> {
      categories.docs.map((cat)=>{
          setcategories((prev)=>[...prev , cat])
      })
    
    })
  },[])

  // useEffect(()=>{
    
  //   categories.map((cat)=>{
  //       console.log(cat.data())
  //       setcat2((prev)=>[...prev , cat.data()])
  //   })
  // },[categories])




  const addProducts =  () =>{
    // console.log(product)
    const arr = []
    const obj = {}
    obj["size"] = size
    obj["color"] = color
    obj["quantity"] = quantity
    obj["productData"] = selectedItem
    console.log(arr , "arr")

    console.log(obj, "obj")
    arr.push(obj)
    console.log(arr , "arr")
    setProductArr((prev)=>[...prev , arr[0]])
    setselectedItem([]);
    setsize([]);
    setcolor([]);
    setquantity([]);

     handleClose()

  }


  useEffect(()=>{
    let toty = 0 ;
    ProductArr.map((item , i) =>{
      toty =  toty + (parseInt(item.productData.saleprice) > 1  ? item.productData.saleprice * item.quantity : item.productData.regularprice * item.quantity )
       }) 
       setsubtotal((prev)=> toty  )
       settotal((prev)=> toty - Discount + ShippingFee )

  },[ProductArr , ShippingFee , Discount])

  return <VendorDashboardLayout>
        <Dialog  fullWidth open={open} onClose={handleClose}>
        <DialogTitle>Product name : {selectedItem.name} </DialogTitle>
        <DialogContent>

<h3>Quantity</h3>

          <TextField autoFocus margin="dense" id="name" label="Quantity" type="number" fullWidth variant="standard" onChange={handleChangeQuantity}
           />
<h3>Color</h3>
         
          <Select
          fullWidth
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={color}
              label="Color"
              onChange={handleChangeColor}
            >
              <MenuItem value="red">Red</MenuItem>
              <MenuItem value="blue">Blue</MenuItem>
              <MenuItem value="green">Green</MenuItem>
            </Select>
<h3>Size</h3>
            <Select
              fullWidth
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={size}
              label="Size"
              onChange={handleChangeSize}
            >
              <MenuItem value="large">large</MenuItem>
              <MenuItem value="xlarge">xlarge</MenuItem>
              <MenuItem value="small">small</MenuItem>
            </Select>
        </DialogContent>
        <DialogActions>
          <Button onClick={addProducts}>ADD</Button>
          <Button onClick={handleClose}>Cancel</Button>
        </DialogActions>
      </Dialog>


      <DashboardPageHeader title="Order Details" icon={ShoppingBag} button={<Link href="/vendor/orders" passHref>
<Button color="primary" sx={{
        bgcolor: "primary.light",
        px: "2rem"
      }}>
              Back to Order List
            </Button>
          </Link>} />
          { showLoading ?  <CircularProgress /> :<span></span>}
          {  showSuccessMessage ?  <Alert severity="success" onClose={() => {setshowSuccessMessage(false)}}> Saved </Alert> :<span></span>}

      <Card sx={{
      p: "0px",
      mb: "30px"
    }}>
        <TableRow elevation={0} sx={{
        bgcolor: "grey.200",
        p: "12px",
        borderRadius: "0px !important"
      }}>
          <FlexBox flex="0 0 0 !important" m={0.75} alignItems="center" whiteSpace="pre">
            <Typography fontSize="14px" color="grey.600" mr={0.5}>
              Order ID:
            </Typography>
            <Typography fontSize="14px">{id}</Typography>
          </FlexBox>
          <FlexBox className="pre" m={0.75} alignItems="center">
            <Typography fontSize="14px" color="grey.600" mr={0.5}>
              Placed on:
            </Typography>
            <Typography fontSize="14px">
              {format(new Date(), "dd MMM, yyyy")}
            </Typography>
          </FlexBox>

          <Box maxWidth="160px">
            <TextField label="Order Status" value={Status1} onChange={(ev)=>  setStatus1(ev.target.value)} placeholder="Order Status" select fullWidth>
              {orderStatusList.map(item => <MenuItem value={item.value} key={item.value}>
                  {item.label}
                </MenuItem>)}
            </TextField>
          </Box>
        </TableRow>

        <Box p="1rem 1.5rem 10px">
          {/* <TextField label="Add Product" fullWidth /> */}
          <Paper sx={{ width: '40%', mb: 2 }}>
                <Autocomplete
                  id="free-solo-demo"
                  search
                  options={categories} //===============change 3 [Keep as it is]
                  getOptionLabel={(option) => {
                  // // Value selected with enter, right from the input
                  // if (typeof option === 'string') {
                  //   return option;
                  // }
                  // // Add "xxx" option created dynamically
                  // if (option.inputValue) {
                  //   return option.inputValue;
                  // }
                  // Regular option
                  return   option.data().name || " "; //================change 4
                }}
                  renderInput={(params) => <TextField {...params} label="search" />}
                  renderOption={(props , option)=>(
                      <span key={option.data().idDoc} 
                       onClick={()=> handleClickOpen(option.data())}
                       >{/* //================change 5 */}
                      
                          <a>
                            <H6 ml={1} borderBottom="1px solid" borderColor="grey.900">
                              {option.data().name} {/* //================change 6*/}
                            </H6>
                          </a>
                        </span>
                    )}
                />
      </Paper>
        </Box>
        {/* .map((cat)=>{
                    console.log(cat.data(), "catname")
                     return cat.data().name || ""}) */}

        <Box py={1}>
          {ProductArr.map((item , i) => <FlexBox px={2} py={1} flexWrap="wrap"  alignItems="center" key={i}>
              <FlexBox flex="2 2 260px" m={0.75} alignItems="center">
                <Avatar src={item.productData.images ? item.productData.images[0] : "/assets/images/products/imagetree.png"} sx={{
              height: 64,
              width: 64
            }} />
                <Box ml={2.5}>
                  <H6 my="0px">{item.productData.name}</H6>
                  <FlexBox alignItems="center"  m={1}>
                    <Typography fontSize="14px" color="grey.600">
                  Price:    {parseInt(item.productData.saleprice) > 1  ? item.productData.saleprice : item.productData.regularprice} LE 
                    </Typography>
                  </FlexBox>
                  {/* <FlexBox alignItems="center"> */}
                    <Box Width="100%" m={1} >
                    <Typography  fontSize="14px" color="grey.600">
                      Quantity: {item.quantity} 
                      </Typography>
                    </Box>
                  {/* </FlexBox> */}
                </Box>
              </FlexBox>
              {/* <FlexBox flex="1 1 260px" m={0} alignItems="center"> */}
                <Typography fontSize="14px" color="grey.600">
                  Product properties: {item.color}, {item.size}
                </Typography>
              {/* </FlexBox> */}
             
              <Typography fontSize="20px" ml={2} >
                  Total:    {parseInt(item.productData.saleprice) > 1  ? item.productData.saleprice * item.quantity : item.productData.regularprice * item.quantity} LE 
                    </Typography>
                    <FlexBox flex="0 0 0 !important" m={0.75} alignItems="center">
                <IconButton>
                  <Delete fontSize="small" />
                </IconButton>
                
              </FlexBox>
            </FlexBox>)}
        </Box>
      </Card>

      <Grid container spacing={3}>
        <Grid item lg={6} md={6} xs={12}>
        <Card sx={{
          p: "20px 30px",
          mb: "1.5rem",
          mt: "1.5rem"
        }}>
            <H5 mt={0} mb={2}>
              Customer Name
            </H5>
            <TextField value={CustomerName} type="text" placeholder="Mohamed Osama" onChange={(ev)=>{ ev.target.value ? setCustomerName( ev.target.value ) : "" }}   fullWidth sx={{
            borderRadius: "10px"
          }} />
          </Card>

         <Card sx={{
          p: "20px 30px",
          mb: "1.5rem"
        }}>
            <H5 mt={0} mb={2}>
              Customer Email
            </H5>
            <TextField value={CustomerEmail} placeholder="Ex: MohamedOsama@customer.com" type="email" onChange={(ev)=>{ ev.target.value ? setCustomerEmail( ev.target.value ) : "" }}   fullWidth sx={{
            borderRadius: "10px"
          }} />
          </Card>
          <Card sx={{
          p: "20px 30px",
          mb: "1.5rem"
        }}>
            <H5 mt={0} mb={2}>
              Customer Phone 
            </H5>
            <TextField value={CustomerPhone} placeholder="Ex: 0128 *** ****" type="text" onChange={(ev)=>{ ev.target.value ? setCustomerPhone( ev.target.value ) : "" }}   fullWidth sx={{
            borderRadius: "10px"
          }} />
          </Card>

          <Card sx={{
          p: "20px 30px",
          mb: "1.5rem"
        }}>
            <H5 mt={0} mb={2}>
              Shipping Address
            </H5>
            <TextField value={ShippingAdress} onChange={(ev)=>{ ev.target.value ? setShippingAdress( ev.target.value ) : "" }} multiline rows={5}  fullWidth sx={{
            borderRadius: "10px"
          }} />
          </Card>

          <Card sx={{
          p: "20px 30px"
        }}>
            <H5 mt={0} mb={2}>
              Customer&apos;s Note
            </H5>
            <TextField value={CustomerNote}  onChange={(ev)=>{ ev.target.value ? setCustomerNote( ev.target.value ) : "" }} multiline rows={5} fullWidth sx={{
            borderRadius: "10px"
          }} />
          </Card>

          <Card sx={{
          p: "20px 30px",
          mb: "1.5rem"
        }}>
            <H5 mt={0} mb={2}>
              Vendor Email
            </H5>
            <TextField value={VendorEmail} type="email" placeholder="Ex: MohamedOsama@gmail.com" onChange={(ev)=>{ ev.target.value ? setVendorEmail( ev.target.value ) : "" }}   fullWidth sx={{
            borderRadius: "10px"
          }} />
          </Card>


        </Grid>
        <Grid item lg={6} md={6} xs={12}>
          <Card sx={{
          p: "20px 30px",
          mb: "1.5rem"
        }}>
            <H5 mt={0} mb={2}>
              Total Summary
            </H5>
            <FlexBox justifyContent="space-between" alignItems="center" mb={1}>
              <Typography fontSize="14px" color="grey.600">
                Subtotal:
              </Typography>
              <H6 my="0px">L.E {subtotal}</H6>
            </FlexBox>
            <FlexBox justifyContent="space-between" alignItems="center" mb={1}>
              <Typography fontSize="14px" color="grey.600">
                Shipping fee:
              </Typography>
              <FlexBox alignItems="center" maxWidth="100px" ml={1} mt={0.5}>
                <Typography mr={1}>L.E {ShippingFee? ShippingFee: 0 } </Typography>
                <TextField value={ShippingFee} onChange={(ev)=>{ ev.target.value ? setShippingFee(parseInt(ev.target.value)) : 0 }} type="number" fullWidth />
              </FlexBox>
            </FlexBox>
            <FlexBox justifyContent="space-between" alignItems="center" mb={1}>
              <Typography fontSize="14px" color="grey.600">
                Discount:
              </Typography>
              <FlexBox alignItems="center" maxWidth="100px" ml={1} mt={0.5}>
                <Typography mr={1}>L.E  {Discount? Discount: 0 } </Typography>
                <TextField value={Discount} onChange={(ev)=>{ ev.target.value ? setDiscount(parseInt(ev.target.value)) : 0 }} type="number" fullWidth />
              </FlexBox>
            </FlexBox>

            <Divider sx={{
            mb: "0.5rem"
          }} />

            <FlexBox justifyContent="space-between" alignItems="center" mb={2}>
              <H6 my="0px">Total</H6>
              <H6 my="0px">{total}</H6>
            </FlexBox>
            <Typography fontSize="14px">Paid by Credit/Debit Card</Typography>
          </Card>

          <Button variant="contained" onClick={handelOrderSubmition} color="primary" sx={{
          ml: "auto",
          display: "block"
        }}>
            Save Changes
          </Button>
        </Grid>
      </Grid>

      
    </VendorDashboardLayout>;
};

const orderStatusList = [{
  label: "Processing",
  value: "Processing"
}, {
  label: "Pending",
  value: "Pending"
}, {
  label: "Delivered",
  value: "Delivered"
}, {
  label: "Cancelled",
  value: "Cancelled"
}];
export default OrderDetails;