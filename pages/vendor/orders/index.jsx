import FlexBox from "components/FlexBox";
import DeliveryBox from "components/icons/DeliveryBox";
import DashboardPageHeader from "components/layout/DashboardPageHeader";
import VendorDashboardLayout from "components/layout/VendorDashboardLayout";
import TableRow from "components/TableRow";
import { H5 } from "components/Typography";
import East from "@mui/icons-material/East";
import { Avatar, IconButton, Pagination, Typography } from "@mui/material";
import Link from "next/link";
import React, { useEffect, useState } from "react";

import { collection ,onSnapshot } from "firebase/firestore";
import { db } from "../../../firebase";
import CategoryList from "components/0data/CaregoryList"

const Orders = () => {

  const [categories, setcategories] = useState([]);
  
  useEffect(()=>{
    onSnapshot(collection(db,"orders"),(categories)=> setcategories(categories.docs))
  },[])

  useEffect(()=>{
    
    categories.map((cat)=>{
        console.log(cat.data())
    })
  },[categories])

  return <VendorDashboardLayout>
          <CategoryList  categories={categories} dataLink="orders/"
  headcells={[
    {id: 'idDoc',numeric: false,disablePadding: false,label: 'ID',},
    {id: 'name',numeric: false,disablePadding: true,label: 'Customer Name',},
    {id: 'Status1',numeric: false,disablePadding: false,label: 'Status',},
    {id: 'total',numeric: true,disablePadding: false,label: 'total',},
    ]} />
      
    </VendorDashboardLayout>;
};

export default Orders;