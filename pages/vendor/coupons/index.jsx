import FlexBox from "components/FlexBox";
import DeliveryBox from "components/icons/DeliveryBox";
import DashboardPageHeader from "components/layout/DashboardPageHeader";
import VendorDashboardLayout from "components/layout/VendorDashboardLayout";
import TableRow from "components/TableRow";
import { H5 } from "components/Typography";
import East from "@mui/icons-material/East";
import { Avatar, IconButton, Pagination, Typography } from "@mui/material";
import Link from "next/link";
import React, { useEffect, useState } from "react";

import { collection ,onSnapshot } from "firebase/firestore";
import { db } from "../../../firebase";
import CategoryList from "components/0data/CaregoryList"

const Products = () => {

  const [categories, setcategories] = useState([]);
  
  useEffect(()=>{
    onSnapshot(collection(db,"coupons"),(categories)=> setcategories(categories.docs))
  },[])

  useEffect(()=>{
    
    categories.map((cat)=>{
        console.log(cat.data())
    })
  },[categories])

  return <VendorDashboardLayout>

      <CategoryList  categories={categories} dataLink="coupons/"
  headcells={[
    {id: 'name',numeric: false,disablePadding: true,label: 'coupon Name',},
    {id: 'discountType',numeric: false,disablePadding: false,label: 'Discount Type',},
    {id: 'code',numeric: false,disablePadding: false,label: 'coupon code',},
    // {id: 'stock',numeric: true,disablePadding: false,label: 'stock',},
    // {id: 'vendor',numeric: true,disablePadding: false,label: 'vendor',},
    ]} />
    </VendorDashboardLayout>;
};

const productList = [{
  orderNo: "1050017AS",
  stock: 30,
  price: 350,
  href: "/vendor/products/5452423"
}, {
  orderNo: "1050017AS",
  stock: 20,
  price: 500,
  href: "/vendor/products/5452423"
}, {
  orderNo: "1050017AS",
  stock: 2,
  price: 700,
  href: "/vendor/products/5452423"
}, {
  orderNo: "1050017AS",
  stock: 25,
  price: 300,
  href: "/vendor/products/5452423"
}, {
  orderNo: "1050017AS",
  stock: 1,
  price: 700,
  href: "/vendor/products/5452423"
}];
export default Products;