import DropZone from "components/DropZone";
import DeliveryBox from "components/icons/DeliveryBox";
import DashboardPageHeader from "components/layout/DashboardPageHeader";
import VendorDashboardLayout from "components/layout/VendorDashboardLayout";
import { Button, Card, Grid, MenuItem, TextField } from "@mui/material";
import { Formik } from "formik";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import * as yup from "yup";
import EditFactory from "components/0data/EditFactory"
import { collection, doc, getDoc, onSnapshot } from "firebase/firestore";
import { db } from "../../../firebase";

const OrderDetails = () => {
  const brands = ['1Zpresso',
  '2B',
  '2k Games',
  '4Gamers',
  'A4Tech',
  'ADATA',
  'AKAI',
  'AMD',
  'AOC',
  'APC',
  'ASTRO',
  'ASUS',
  'ATA',
  'Acer',
  'Activision',
  'Advantek',
  'Akel',
  'Alcatel',
  'Anker',
  'Apple',
  'Aqua Jet',
  'Ariete',
  'Arion',
  'Ariston',
  'Aruba',
  'Arzum',
  'Astra',
  'Atari',
  'Atlantic',
  'Atlas',
  'Aula',
  'Aurora',
  'Avantree',
  'Awei',
  'B&O',
  'BaByliss',
  'Baltgaz',
  'Barcode',
  'Beats',
  'Beko',
  'Belkin',
  'BenQ',
  'Benex',
  'Bergen',
  'Beurer',
  'Bialetti',
  'Bionik',
  'Bionime',
  'Bissell',
  'BitFenix',
  'Bixilon',
  'Black + Decker',
  'Black And White',
  'Borofone',
  'Bosch',
  'Bose',
  'Boya',
  'Brandt',
  'Braun',
  'Bravo',
  'British Berkefeld',
  'Brother',
  'Buddy',
  'CAME-TV',
  'CISCO',
  'Cager',
  'Caixun',
  'Canon',
  'Carbon',
  'Cardoo',
  'Carnival',
  'Carrier',
  'Case Logic',
  'Castle',
  'Celebrat',
  'Cerruti',
  'City',
  'Claber',
  'Comfast',
  'Contex',
  'Cooler Master',
  'Corn',
  'Cougar',
  'Covery',
  'Crane',
  'Crown',
  'Crucial',
  'CrushGrind',
  'Czur',
  'D-Link',
  'DC',
  'DMH',
  'DSP',
  'Dahua',
  'Danata',
  'Darago',
  'Dark Flash',
  'Dell',
  'Delonghi',
  'Devia',
  'Dhd',
  'Dingling',
  'Dish TV',
  'Dob',
  'Dobe',
  'Dremel',
  'E-train',
  'EA',
  'EKSA',
  'ETI',
  'Echosat',
  'Ecomatic',
  'Electronic Arts',
  'Elica',
  'Elios',
  'Energizer',
  'Entercise',
  'Epson',
  'Etrain',
  'Ezviz',
  'F&D',
  'Fagor',
  'Falcon',
  'Ferrari',
  'Fibaro',
  'First',
  'Fit',
  'Flamngo',
  'Fluxtek',
  'Flyon',
  'Fora',
  'Fox Sat',
  'Franke',
  'Fresh',
  'Fujifilm',
  'G-Tab',
  'G.Tec',
  'GAT',
  'GMax',
  'Galaxy',
  'Genai',
  'General',
  'General Tech',
  'Genius',
  'Genki Mesin',
  'Genwex',
  'Gigabyte',
  'Go Pro',
  'Gold',
  'Golden',
  'Golden Falcon',
  'Goldi Mistral',
  'Google',
  'Grand',
  'Grandstream',
  'Grouhy',
  'Haier',
  'Hapilin',
  'Havit',
  'Heller',
  'High Way',
  'Hikoki',
  'Hikvision',
  'Hitachi',
  'Hitch',
  'Hoho',
  'Home',
  'Honor',
  'Hood',
  'Hoover',
  'Horion',
  'Horizon',
  'House of Marley',
  'Hp',
  'Huawei',
  'Human Traction',
  'HyperX',
  'I Tech Solution',
  'I luxe',
  'IMOU',
  'IQ&T',
  'Iconz',
  'Ideal',
  'IeTop',
  'Imax',
  'Incase',
  'Indesit',
  'Infinix',
  'Ingco',
  'Insomniac',
  'Instax',
  'Intel',
  'JAC',
  'JBL',
  'Jabra',
  'Jespc',
  'Joyroom',
  'Jsaux',
  'KB',
  'Kaspersky',
  'Keendex',
  'Kemei',
  'Kenwood',
  'Kingston',
  'Kioxia',
  'Kiriazi',
  'Kkmoon',
  'Koldair',
  'Konami',
  'KontrolFreek',
  'Kumtel',
  'Lavvento',
  'LG',
  'La Germania',
  'Lane',
  'Ldnio',
  'Leadstar',
  'Lenovo',
  'Linksys',
  'Logitech',
  'Long Life',
  'MG',
  'MPT',
  'MSI',
  'Magefesa',
  'Manfrotto',
  'Manhattan',
  'Matrix',
  'Maza',
  'Media Star',
  'MediaTech',
  'Memo',
  'Merlin',
  'MiLi',
  'Microsoft',
  'Midea',
  'Miele',
  'Mienta',
  'MikroTik',
  'Modecom',
  'Modex',
  'Modio',
  'Moon Light',
  'More',
  'Moser',
  'Motospeed',
  'Moulinex',
  'Mtouch',
  'My Arcade',
  'NACON',
  'NCS',
  'NI',
  'NOVOVENT',
  'Natural Sky',
  'Neewer',
  'Nekteck',
  'Nesma',
  'Netac',
  'Netgear',
  'Next',
  'Nikai',
  'Nikon',
  'Nillkin',
  'Nintendo',
  'Nokia',
  'Nova',
  'OKI',
  'OTL',
  'Olympic Electric',
  'Onikuma',
  'Oppo',
  'Oraimo',
  'Other',
  'Ozone',
  'PES',
  'PUBG Corp',
  'Panasonic',
  'Pangao',
  'Passap',
  'Pedrini',
  'Philips',
  'Pins',
  'PluGuard',
  'Porodo',
  'Porsh',
  'Premiere',
  'Premium',
  'Prestige',
  'ProLink',
  'Proform',
  'Promate',
  'Prorack',
  'Purity',
  'QCY',
  'REDLINE',
  'Rapoo',
  'Razer',
  'Redragon',
  'Relax and Spin Tone',
  'Remax',
  'Remington',
  'Ricoh',
  'Rifland',
  'Rivacase',
  'Riversong',
  'Rock House',
  'Rockrose',
  'Rockstar',
  'Rode',
  'Roman',
  'Rossmax',
  'Rovo',
  'Rozia',
  'Rush Brush',
  'S Smart',
  'SBS',
  'SEM',
  'SPEEDLINK',
  'SSmart',
  'Sades',
  'Samsung',
  'Sandisk',
  'Sary',
  'Sasho',
  'Saturn',
  'Seagate',
  'Senator',
  'Sencor',
  'Sennheiser',
  'Sharp',
  'Silicon Power',
  'Sinbo',
  'Skyworth',
  'Smart',
  'Smeg',
  'Smile',
  'Sndway',
  'Sokany',
  'Solac',
  'Sollatek',
  'Sonai',
  'Sonar',
  'Sonashi',
  'Sonifer',
  'Sonoff',
  'Sony',
  'Soul',
  'Speed',
  'Spirit Of Gamer',
  'Sprint',
  'Spy',
  'Squarc Cnix',
  'Star com',
  'Starget',
  'Stiebel Eltron',
  'Strong',
  'Syinix',
  'TCL',
  'TP-Link',
  'Tank',
  'Targus',
  'Taurus',
  'Technical Controls',
  'Techno Zone',
  'Tefal',
  'Televas',
  'Tenda',
  'Thrive',
  'Tiger',
  'Tingwode',
  'Tornado',
  'Toshiba',
  'Total',
  'Touch',
  'Touch El Zenouky',
  'Transcend',
  'Tronsmart',
  'Truman',
  'Turboair',
  'ULTRA',
  'Ubiquiti',
  'Ubisoft',
  'Ultimate Ears',
  'Ultralife',
  'Unionaire',
  'Unitex',
  'Universal',
  'Uno',
  'VAIO',
  'VGR',
  'VT',
  'Veito',
  'Venezia',
  'Vidvie',
  'View',
  'View Sound',
  'ViewSonic',
  'Vire',
  'Vivo',
  'WIWU',
  'Wacaco',
  'Wahl',
  'Waves',
  'Wax Way',
  'Weifeng',
  'Western Digital',
  'White Point',
  'White Shark',
  'White Whale',
  'Wild Wolf',
  'X-tech',
  'XO',
  'XPG',
  'Xenon',
  'Xiaomi',
  'Xiizone',
  'Xprinter',
  'Xzone',
  'Yes-Original',
  'Yison',
  'Yookie',
  'ZYC1',
  'Zada',
  'Zahran',
  'Zanussi',
  'Zebra',
  'Zero',
  'Zhiyun',
  'frrep',
  'i-Cook Built-in',
  'iCone',
  'iKU',
  'iTel',
  'realme',
  'yk design',
  'Sky Data Show',
]
  const router = useRouter();
  const {
    id
  } = router.query;


  const [categories, setcategories] = useState([]);
  const [categories1, setcategories1] = useState([]);
  useEffect(()=>{  
    const getFun = async () =>{
      const ref = doc(db, "categories", "8ACMKJCMEuweyzv2RAPj");
      const docSnap = await getDoc(ref);
      if (docSnap.exists()) {
    console.log(docSnap.data(), "sasaasasasassa")
        setcategories1((prev)=>[...prev,docSnap.data()  ] )
      }
    }
    getFun()

  },[])

  // useEffect(()=>{
  //   onSnapshot(collection(db,"categories"),(categories)=> setcategories(categories.docs))
  // },[])
  // useEffect(()=>{

  //   categories.map((cat)=>{
  //     setcategories1((prev)=>[...prev,cat.data().name])
  //   })
  // },[categories])

  // const handleFormSubmit = async values => {
  //   console.log(values);
  // };

  // console.log(id);
  return <VendorDashboardLayout>

<EditFactory id={id} collectionType="products" collectionLink="/vendor/products" 
    itemsFieldsValues={[
      {name:"name" , label:"name" , placeholder:"product name" , type:"text" , values:[] , yupReqired:true , yupType:"string"},
{name:"nameinarabic" , label:"اسم المنتج  بالعربي" , placeholder:"اسم المنتج بالعربي" , type:"text" , values:[] , yupReqired:true , yupType:"string"},                               

      {name:"description" , label:"description" , placeholder:"description " , type:"textArea" , values:[] , yupReqired:true , yupType:"string"},
      {name:"descriptioninarabic" , label:"تفاصيل المنتج بالعربي" , placeholder:"تفاصيل المنتج بالعربي" , type:"textArea" , values:[] , yupReqired:true , yupType:"string"},

      {name:"images" , label:"images" , placeholder:"images " , type:"images" , values:[] , yupReqired:true , yupType:"images"},
      {name:"regularprice" , label:"regular price" , placeholder:"regular price " , type:"number" , values:[] , yupReqired:true , yupType:"number"},
      {name:"saleprice" , label:"sale price" , placeholder:"sale price " , type:"number" , values:[] , yupReqired:true , yupType:"number"},
      {name:"stock" , label:"stock" , placeholder:"stock " , type:"number" , values:[] , yupReqired:true , yupType:"number"},

      {name:"color" , label:"Color" , placeholder:"Color" , type:"select" , values:[ "no","yes" ] , yupReqired:false , yupType:"string"},                               
      {name:"size" , label:"Size" , placeholder:"Size" , type:"select" , values:[ "no","yes" ] , yupReqired:false , yupType:"string"},                               
      {name:"brand" , label:"Brand" , placeholder:"Brand" , type:"select" , values:brands , yupReqired:false , yupType:"string"},                               
{name:"commision" , label:"Seller Product Commision" , placeholder:"Seller Product Commision" , type:"select" , values:["5%" , "10%","20%" , "30%" ,"40%" , "50%"] , yupReqired:false , yupType:"string"},                               
{name:"affiliate" , label:"Affiliate Commision" , placeholder:"Affiliate Commision" , type:"select" , values:[ "5%","10%","20%" , "30%" ,"40%" , "50%"] , yupReqired:false , yupType:"string"},                               


      {name:"category" , label:"category" , placeholder:"category " , type:"selectBox" , values:categories1 , yupReqired:true , yupType:"selectBox"},

      // {name:"parent" , label:"parent" , placeholder:"category parent" , type:"select" , values:["electronics" , "none" ,"fashion"] , yupReqired:true , yupType:"string"}
    ]}
  />
      {/* <DashboardPageHeader title="Edit Product" icon={DeliveryBox} button={<Link href="/vendor/products" passHref>
            <Button color="primary" sx={{
        bgcolor: "primary.light",
        px: "2rem"
      }}>
              Back to Product List
            </Button>
          </Link>} />

      <Card sx={{
      p: "30px"
    }}>
        <Formik initialValues={initialValues} validationSchema={checkoutSchema} onSubmit={handleFormSubmit}>
          {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit
        }) => <form onSubmit={handleSubmit}>
              <Grid container spacing={3}>
                <Grid item sm={6} xs={12}>
                  <TextField name="name" label="Name" placeholder="Name" fullWidth onBlur={handleBlur} onChange={handleChange} value={values.name || ""} error={!!touched.name && !!errors.name} helperText={touched.name && errors.name} />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField name="category" label="Select Category" placeholder="Category" fullWidth select onBlur={handleBlur} onChange={handleChange} value={values.category || ""} error={!!touched.category && !!errors.category} helperText={touched.category && errors.category}>
                    <MenuItem value="electronics">Electronics</MenuItem>
                    <MenuItem value="fashion">Fashion</MenuItem>
                  </TextField>
                </Grid>
                <Grid item xs={12}>
                  <DropZone onChange={files => {
                console.log(files);
              }} />
                </Grid>
                <Grid item xs={12}>
                  <TextField name="description" label="Description" placeholder="Description" rows={6} multiline fullWidth onBlur={handleBlur} onChange={handleChange} value={values.description || ""} error={!!touched.description && !!errors.description} helperText={touched.description && errors.description} />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField name="stock" label="Stock" placeholder="Stock" fullWidth onBlur={handleBlur} onChange={handleChange} value={values.stock || ""} error={!!touched.stock && !!errors.stock} helperText={touched.stock && errors.stock} />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField name="tags" label="Tags" placeholder="Tags" fullWidth onBlur={handleBlur} onChange={handleChange} value={values.tags || ""} error={!!touched.tags && !!errors.tags} helperText={touched.tags && errors.tags} />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField name="price" label="Regular Price" placeholder="Regular Price" type="number" fullWidth onBlur={handleBlur} onChange={handleChange} value={values.price || ""} error={!!touched.price && !!errors.price} helperText={touched.price && errors.price} />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField name="sale_price" label="Sale Price" placeholder="Sale Price" type="number" fullWidth onBlur={handleBlur} onChange={handleChange} value={values.sale_price || ""} error={!!touched.sale_price && !!errors.sale_price} helperText={touched.sale_price && errors.sale_price} />
                </Grid>
              </Grid>
              <Button variant="contained" color="primary" type="submit" sx={{
            mt: "25px"
          }}>
                Save product
              </Button>
            </form>}
        </Formik>
      </Card> */}
    </VendorDashboardLayout>;
};

const initialValues = {
  name: "",
  stock: "",
  price: "",
  sale_price: "",
  description: "",
  tags: "",
  category: ""
};
const checkoutSchema = yup.object().shape({
  name: yup.string().required("required"),
  category: yup.string().required("required"),
  description: yup.string().required("required"),
  stock: yup.number().required("required"),
  price: yup.number().required("required"),
  sale_price: yup.number().required("required"),
  tags: yup.object().required("required")
});
export default OrderDetails;