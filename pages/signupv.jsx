import FlexBox from "components/FlexBox";
import Signup from "components/sessions/Signup";
import React from "react";

const SignUpPageV = () => {
  return <FlexBox flexDirection="column" minHeight="100vh" alignItems="center" justifyContent="center">
      <Signup isVendor={true} />
    </FlexBox>;
};

export default SignUpPageV;