const  admin = require("firebase-admin");
const  serviceAccount = require("../../shirua2-firebase-adminsdk-k49l7-74bd6ba5ce.json");


if(!admin.apps.length){admin.initializeApp({credential: admin.credential.cert(serviceAccount),});}

export default async function  handler(req, res) {
  // ADD USER ===================================================
    if(req.method === "POST"){
         const { uid  , values} = req.body
        const cityRef =  admin.firestore().collection('userd');
        const doc = await cityRef.get();

        doc.forEach(doc => {
          if(doc.data().userid ==  uid )
          {
            if(doc.data().role == "a")
            {
             res.status(200).json({ message :"cannot access" })
            }else
            {
            admin.auth().createUser({password: values.password,email: values.email,})
            .then((userRecord) => 
            {
              const data = {fullname: values.name,email: values.email,phone : values.phone,
                            userid:userRecord.uid,role:values.role,isVendorActive:false};
              const cityRef2 =   admin.firestore().collection('userd').add(data);
              res.status(200).json({ uid ,values , cityRef2 })
            })
            .catch((error) => {
              console.log('Error creating new user:', error.errorInfo.message);
                res.status(200).json({ error: error.errorInfo.message})
            })
          }
          }
        });
      }  
  // Delete USER ===================================================
    if(req.method === "DELETE")
    {
        const { uid , id } = req.body
        console.log('Added document with ID: ', id  );
        console.log('this user id: ', uid  );
        const cityRef =  admin.firestore().collection('userd');
        const doc = await cityRef.get();

        const deleteMetaData = async (ido) =>{
          const cityRef =  admin.firestore().collection('userd');
          const doc = await cityRef.get();

          doc.forEach(doc2 => {
            if(doc2.data().userid ==  ido )
            {
              console.log('found: ' ,doc2.id);
              admin.firestore().collection('userd').doc(doc2.id).delete()
              console.log("idDoc", doc2.id );
              console.log("userId",ido );
              console.log("message","success" );
              return   res.status(200).json({ idDoc: doc2.id , userId:ido  , message:"success" })
            }
          })
        }
        doc.forEach(doc => {
          if(doc.data().userid ==  uid )
          {
            if(doc.data().role == "a")
            {
              res.status(200).json({ message :"cannot access" })
            }else
            { 
              admin.auth().deleteUser(id).then(() => {
                console.log('Successfully deleted  user: ' ,id);
                deleteMetaData(id)
              })
              .catch((error) => {
                console.log('Error creating new user:', error);
                res.status(200).json({ error: error})
              })
            }
          }
        });
      }
      if(req.method === "PUT"){
          const { uid , id , values } = req.body
          console.log('Successfully update  idDoc: ' ,id);
          console.log('this user id: ' ,uid);
          const cityRef =  admin.firestore().collection('userd');
          const doc = await cityRef.get();
          const updateUserFunc = (values , ido) =>{
                  doc.forEach(doc2 => {
                    if(doc2.data().userid ==  ido )
                    {
                      const data = {fullname: values.fullname,email: values.email,phone : values.phone,role:values.role,
                        isVendorActive:values.isVendorActive == "true"? true:false 
                      ,bankname:values.bankname,
                      accountname:values.accountname,
                      bankcode:values.bankcode,
                      iban:values.iban,
                      swift:values.swift
                    
                    };  
                      console.log('found: ' ,doc2.id);
                      admin.firestore().collection('userd').doc(doc2.id).update(data)
                      console.log("idDoc", doc2.id );
                      console.log("userId",ido );
                      console.log("message","success" );
                      return   res.status(200).json({ idDoc: doc2.id , userId:ido  , message:"success" })
                    }
                  })
                  console.log(values, "val")
          }
          doc.forEach(doc => {
            if(doc.data().userid ==  uid )
            {
              if(doc.data().role == "a")
              {
                res.status(200).json({ message :"cannot access" })
              }else
              {    
                updateUserFunc(values , id)
           
              }
            }
          });


        // res.status(200).json({ message :"cannot access" })

      }

      if(req.method === "GET"){
        res.status(200).json({ "empty":""})

      }

  }