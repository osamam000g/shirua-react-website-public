import FlexBox from "components/FlexBox";
import NavbarLayout from "components/layout/NavbarLayout";
import ProductCard1List from "components/products/ProductCard1List";
import ProductCard9List from "components/products/ProductCard9List";
import ProductFilterCard from "components/products/ProductFilterCard";
import Sidenav from "components/sidenav/Sidenav";
import { H5, Paragraph } from "components/Typography";
import useWindowSize from "hooks/useWindowSize";
import Apps from "@mui/icons-material/Apps";
import FilterList from "@mui/icons-material/FilterList";
import ViewList from "@mui/icons-material/ViewList";
import { Card, Grid, IconButton, MenuItem, TextField } from "@mui/material";
import { Box } from "@mui/system";
import React, { useCallback, useEffect, useState } from "react";
import { useRouter } from "next/router";
import { collection, onSnapshot } from "firebase/firestore";
import { db } from "../../../firebase";

const ProductSearchResult = () => {
  const [view, setView] = useState("grid");
  const width = useWindowSize();
  const isTablet = width < 1024;
  const router = useRouter();
  const [name, setname] = useState(0);
  const [cat, setcat] = useState("");
  const [pe, setpe] = useState("");
  const [ps, setps] = useState("");
  const [brand, setbrand] = useState("");
  // /name/cat/ps/pe/brand
  //if 0 mean is not excits 


  const categoriesF = [
    {
      name: 'Electronic', nameinarabic: "الكترونيات",
      children: [{ name: 'Camera & Photo', slug: 'Camera & Photo', nameinarabic: "كاميرات", url: "" }, { name: 'Car & Vehicle Electronics', slug: 'Car & Vehicle Electronics', nameinarabic: "اكسسوارات السياره", url: "", }, { name: 'Computers & Accessories', slug: 'Computers & Accessories', nameinarabic: "الكمبيوتر", url: "", }, { name: 'eBook Readers & Accessories', slug: 'eBook Readers & Accessories', nameinarabic: "قارات الكتب ", url: "", }, { name: 'Headphones, Earbuds & Accessories', slug: 'Headphones, Earbuds & Accessories', nameinarabic: "سماعات", url: "", }, { name: 'Hi-Fi & Home Audio', slug: 'Hi-Fi & Home Audio', nameinarabic: "", url: "الصوتيات", }, { name: 'Home Theater, TV & Video', slug: 'Home Theater, TV & Video', nameinarabic: "التلفزيون", url: "", }, { name: 'Household Batteries & Chargers', slug: 'Household Batteries & Chargers', nameinarabic: "الشواحن و البطاريات", url: "", }, { name: 'Mobile Phones & Communication', slug: 'Mobile Phones & Communication', nameinarabic: "الهواتف", url: "", }]
    },
    {
      name: 'Home Applications', slug: 'Home Applications', nameinarabic: "اجهزه المنزل",
      children: [{ name: 'Dishwashers & Dish Dryers', slug: 'Dishwashers & Dish Dryers', nameinarabic: "غسالات اطباق", url: "", }, { name: 'Heating & Cooling', slug: 'Heating & Cooling', nameinarabic: "تكيفات", url: "", }, { name: 'Microwaves & Countertop Ovens', slug: 'Microwaves & Countertop Ovens', nameinarabic: "ميكروويف", url: "", }, { name: 'Refrigerators, Freezers ', slug: 'Refrigerators, Freezers', nameinarabic: "ثلاجات", url: "", }, { name: 'Small Appliances', slug: 'Small Appliances', nameinarabic: "الاجهزه الصغيره", url: "", }, { name: 'Washers & Dryers', slug: 'Washers & Dryers', nameinarabic: "غسالات ملابس", url: "", }]
    },
  ]
  // get  all  product  [done]
  // filter 
  // clear  
  // pagnation 
  const [ProductsF, setProductsF] = useState([]);
  const [filterdProduct, setfilterdProduct] = useState([]);
  const [Length, setLength] = useState(0);

  useEffect(() => {
    if (!router.isReady) return;
    const { id } = router.query

    setname(typeof (id[0]) !== 'undefined' && id[0] != null ? id[0] : 0)
    setcat(typeof (id[1]) !== 'undefined' && id[1] != null ? id[1] : 0)
    setps(typeof (id[2]) !== 'undefined' && id[2] != null ? id[2] : 0)
    setpe(typeof (id[3]) !== 'undefined' && id[3] != null ? id[3] : 0)
    setbrand(typeof (id[4]) !== 'undefined' && id[4] != null ? id[4] : 0)
    console.log("id", id)

    onSnapshot(collection(db, "products"), (categories) => { setProductsF(categories.docs) })

  }, [router.isReady, router.query.id]);

  useEffect(() => {
    // filter 
    var filterdArr = []
    //filter by name
    if(name != 0){
      var PATTERN = name.toString().toLowerCase().trim(), filteredByName = ProductsF.filter(function (str) { return str.data().name.toLowerCase().trim().includes(PATTERN); });
      filteredByName.map((item,i)=> console.log(item.data(), `filtered name ${i}`))
    }else{
      var PATTERN = " ", filteredByName = ProductsF.filter(function (str) { return str.data().name.toLowerCase().trim().includes(PATTERN); });
      filteredByName.map((item,i)=> console.log(item.data(), `filtered name ${i}`))
    }

   
    // filter by cat or parent cat 
    if (cat != 0 && cat.toString() != "All Categories" && cat.toString() != "All"   ) {
         console.log(cat , "All") 

        var PATTERN2 = cat.toString().toLowerCase().trim(), filteredByCat = filteredByName.filter(function (str) { return str.data().category.name.toLowerCase().trim().includes(PATTERN2); })
        filteredByCat.map((item)=>filterdArr.push(item.data()))
        console.log(filterdArr , "filterdArr after cat ")
        
        var ChildCatName = [] // you can filter with this and child arr 
        


        filteredByName.map((pro)=>{ 
        console.log( "e pro "  )
        console.log( "1 " ,pro.data().category.name.toString().toLowerCase().trim() , cat.toString().toLowerCase().trim()  )

        if(pro.data().category.name.toString().toLowerCase().trim() == cat.toString().toLowerCase().trim() ){
          filterdArr.map((filterItem)=> pro.data().name == filterItem.name  ?   ()=> null :  filterdArr.push(pro.data()) ) 
        } 

        categoriesF.map((parent) =>{
          console.log( "e C F"   )

          if (parent.children && parent.children.length != 0 && parent.name.toString().toLowerCase().trim() ==  cat.toString().toLowerCase().trim()) {//map s
            console.log( "have Ch" , parent.children.length   )
            parent.children.map((child) =>
            {

              if (child.slug.toString().toLowerCase().trim() == pro.data().category.name.toString().toLowerCase().trim()) 
              {
                console.log( "1 " ,child.name.toString().toLowerCase().trim() , pro.data().category.name.toString().toLowerCase().trim() )
                if(filterdArr.length < 1 || filterdArr == undefined){
                  // console.log("filteredByName" , filteredByName)
                  filteredByName.map((item,i)=> pro.data().name.toString().toLowerCase().trim() == item.data().name.toString().toLowerCase().trim()  ? filterdArr.push(pro.data()) : ()=>null )
                  console.log("filteredByName" , filteredByName.length)

                  
                }else{

             filteredByName.map((item,i)=> pro.data().name.toString().toLowerCase().trim() == item.data().name.toString().toLowerCase().trim()  ? filterdArr.map((filterItem)=> pro.data().name == filterItem.name  ?   ()=> null :  filterdArr.push(pro.data()) )   : ()=>null )

                    
               console.log("filteredByName" , filteredByName.length)

                }

              }
            })
          }
          }
        )//map end 

      })
      ChildCatName.map((name0)=>{
        var PATTERN3 = name0.toString().toLowerCase(), filteredByCatparent = filteredByName.filter(function (str) { return str.data().category.name.toLowerCase().includes(PATTERN3) }) 
        filteredByCatparent.map((item)=> filterdArr.map((filterItem)=> item.data().name == filterItem.name  ?   ()=> null :  filterdArr.push(item.data()) ) )
      })
    
    console.log(filterdArr , "search in  cat child ")    
    }else{
      filteredByName.map((item)=>filterdArr.push(item.data()))
    }

    if(brand != 0 ){
      var PATTERN5 = brand.toString().toLowerCase(), filteredByBrand = filterdArr.filter(function (str) { return str.brand.toLowerCase().includes(PATTERN5); });
      filterdArr = filteredByBrand
    console.log(filterdArr , "brand filter ")    

    }
    if(pe != 0 ){
      // var PATTERN6 = pe.toString().toLowerCase(), filteredByBrand = filterdArr.filter(function (str) { return str.regularprice.toLowerCase().includes(PATTERN6); });
      // filterdArr = filteredByBrand
    var newARR2 =   filterdArr.filter(function(x) {
    console.log("x.regularprice" ,x.regularprice)
    console.log("ps" , ps)
    console.log("pe" , pe)

        return parseInt(x.regularprice)  > parseInt(ps)  && parseInt(x.regularprice)  < parseInt(pe);
    });
    filterdArr = newARR2
    console.log(filterdArr ,"a range fil")
    }
    console.log(cat , "All" , "All Categories") 


    setfilterdProduct(filterdArr)




    setLength(filterdArr.length )
    // filteredByCat.map((pro) => { console.log(pro.data(), "f") })
  }, [ProductsF])

  const toggleView = useCallback(v => () => {
    setView(v);
  }, []);


  return <NavbarLayout>
    <Box pt={2.5}>
      <Card sx={{
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-between",
        alignItems: "center",
        mb: "55px",
        p: {
          xs: "1.25rem 1.25rem 0.25rem",
          sm: "1rem 1.25rem",
          md: "0.5rem 1.25rem"
        }
      }} elevation={1}>
        <div>
          <H5>Searching for {name != 0 ? name : " "}</H5>
          <Paragraph color="grey.600">{Length} results found</Paragraph>
        </div>
        <FlexBox alignItems="center" flexWrap="wrap" my="0.5rem">
          <FlexBox alignItems="center" flex="1 1 0">
            {/* <Paragraph color="grey.600" mr={2} whiteSpace="pre">
              Short by:
            </Paragraph>
            <TextField variant="outlined" size="small" placeholder="Short by" select defaultValue={sortOptions[0].value} fullWidth sx={{
              flex: "1 1 0",
              mr: "1.75rem",
              minWidth: "150px"
            }}>
              {sortOptions.map(item => <MenuItem value={item.value} key={item.value}>
                {item.label}
              </MenuItem>)}
            </TextField> */}
          </FlexBox>

          <FlexBox alignItems="center" my="0.25rem">
            <Paragraph color="grey.600" mr={1}>
              View:
            </Paragraph>
            <IconButton onClick={toggleView("grid")}>
              <Apps color={view === "grid" ? "primary" : "inherit"} fontSize="small" />
            </IconButton>
            {/* <IconButton onClick={toggleView("list")}>
              <ViewList color={view === "list" ? "primary" : "inherit"} fontSize="small" />
            </IconButton> */}

            {!!isTablet && <Sidenav handle={<IconButton>
              <FilterList fontSize="small" />
            </IconButton>}>
              <ProductFilterCard brands={brands} categoriesF={categoriesF} />
            </Sidenav>}
          </FlexBox>
        </FlexBox>
      </Card>

      <Grid container spacing={3}>
        <Grid item lg={3} xs={12} sx={{
          "@media only screen and (max-width: 1024px)": {
            display: "none"
          }
        }}>
          <ProductFilterCard brands={brands} categoriesF={categoriesF} />
        </Grid>

        <Grid item lg={9} xs={12}>
          {view === "grid" ? <ProductCard1List filterdProduct={filterdProduct} /> : <ProductCard9List filterdProduct={filterdProduct} />} 
        </Grid>
      </Grid>
    </Box>
  </NavbarLayout>;
};

const sortOptions = [{
  label: "Relevance",
  value: "Relevance"
}, {
  label: "Date",
  value: "Date"
}, {
  label: "Price Low to High",
  value: "Price Low to High"
}, {
  label: "Price High to Low",
  value: "Price High to Low"
}];
const brands = ['1Zpresso',
  '2B',
  '2k Games',
  '4Gamers',
  'A4Tech',
  'ADATA',
  'AKAI',
  'AMD',
  'AOC',
  'APC',
  'ASTRO',
  'ASUS',
  'ATA',
  'Acer',
  'Activision',
  'Advantek',
  'Akel',
  'Alcatel',
  'Anker',
  'Apple',
  'Aqua Jet',
  'Ariete',
  'Arion',
  'Ariston',
  'Aruba',
  'Arzum',
  'Astra',
  'Atari',
  'Atlantic',
  'Atlas',
  'Aula',
  'Aurora',
  'Avantree',
  'Awei',
  'B&O',
  'BaByliss',
  'Baltgaz',
  'Barcode',
  'Beats',
  'Beko',
  'Belkin',
  'BenQ',
  'Benex',
  'Bergen',
  'Beurer',
  'Bialetti',
  'Bionik',
  'Bionime',
  'Bissell',
  'BitFenix',
  'Bixilon',
  'Black + Decker',
  'Black And White',
  'Borofone',
  'Bosch',
  'Bose',
  'Boya',
  'Brandt',
  'Braun',
  'Bravo',
  'British Berkefeld',
  'Brother',
  'Buddy',
  'CAME-TV',
  'CISCO',
  'Cager',
  'Caixun',
  'Canon',
  'Carbon',
  'Cardoo',
  'Carnival',
  'Carrier',
  'Case Logic',
  'Castle',
  'Celebrat',
  'Cerruti',
  'City',
  'Claber',
  'Comfast',
  'Contex',
  'Cooler Master',
  'Corn',
  'Cougar',
  'Covery',
  'Crane',
  'Crown',
  'Crucial',
  'CrushGrind',
  'Czur',
  'D-Link',
  'DC',
  'DMH',
  'DSP',
  'Dahua',
  'Danata',
  'Darago',
  'Dark Flash',
  'Dell',
  'Delonghi',
  'Devia',
  'Dhd',
  'Dingling',
  'Dish TV',
  'Dob',
  'Dobe',
  'Dremel',
  'E-train',
  'EA',
  'EKSA',
  'ETI',
  'Echosat',
  'Ecomatic',
  'Electronic Arts',
  'Elica',
  'Elios',
  'Energizer',
  'Entercise',
  'Epson',
  'Etrain',
  'Ezviz',
  'F&D',
  'Fagor',
  'Falcon',
  'Ferrari',
  'Fibaro',
  'First',
  'Fit',
  'Flamngo',
  'Fluxtek',
  'Flyon',
  'Fora',
  'Fox Sat',
  'Franke',
  'Fresh',
  'Fujifilm',
  'G-Tab',
  'G.Tec',
  'GAT',
  'GMax',
  'Galaxy',
  'Genai',
  'General',
  'General Tech',
  'Genius',
  'Genki Mesin',
  'Genwex',
  'Gigabyte',
  'Go Pro',
  'Gold',
  'Golden',
  'Golden Falcon',
  'Goldi Mistral',
  'Google',
  'Grand',
  'Grandstream',
  'Grouhy',
  'Haier',
  'Hapilin',
  'Havit',
  'Heller',
  'High Way',
  'Hikoki',
  'Hikvision',
  'Hitachi',
  'Hitch',
  'Hoho',
  'Home',
  'Honor',
  'Hood',
  'Hoover',
  'Horion',
  'Horizon',
  'House of Marley',
  'Hp',
  'Huawei',
  'Human Traction',
  'HyperX',
  'I Tech Solution',
  'I luxe',
  'IMOU',
  'IQ&T',
  'Iconz',
  'Ideal',
  'IeTop',
  'Imax',
  'Incase',
  'Indesit',
  'Infinix',
  'Ingco',
  'Insomniac',
  'Instax',
  'Intel',
  'JAC',
  'JBL',
  'Jabra',
  'Jespc',
  'Joyroom',
  'Jsaux',
  'KB',
  'Kaspersky',
  'Keendex',
  'Kemei',
  'Kenwood',
  'Kingston',
  'Kioxia',
  'Kiriazi',
  'Kkmoon',
  'Koldair',
  'Konami',
  'KontrolFreek',
  'Kumtel',
  'Lavvento',
  'LG',
  'La Germania',
  'Lane',
  'Ldnio',
  'Leadstar',
  'Lenovo',
  'Linksys',
  'Logitech',
  'Long Life',
  'MG',
  'MPT',
  'MSI',
  'Magefesa',
  'Manfrotto',
  'Manhattan',
  'Matrix',
  'Maza',
  'Media Star',
  'MediaTech',
  'Memo',
  'Merlin',
  'MiLi',
  'Microsoft',
  'Midea',
  'Miele',
  'Mienta',
  'MikroTik',
  'Modecom',
  'Modex',
  'Modio',
  'Moon Light',
  'More',
  'Moser',
  'Motospeed',
  'Moulinex',
  'Mtouch',
  'My Arcade',
  'NACON',
  'NCS',
  'NI',
  'NOVOVENT',
  'Natural Sky',
  'Neewer',
  'Nekteck',
  'Nesma',
  'Netac',
  'Netgear',
  'Next',
  'Nikai',
  'Nikon',
  'Nillkin',
  'Nintendo',
  'Nokia',
  'Nova',
  'OKI',
  'OTL',
  'Olympic Electric',
  'Onikuma',
  'Oppo',
  'Oraimo',
  'Other',
  'Ozone',
  'PES',
  'PUBG Corp',
  'Panasonic',
  'Pangao',
  'Passap',
  'Pedrini',
  'Philips',
  'Pins',
  'PluGuard',
  'Porodo',
  'Porsh',
  'Premiere',
  'Premium',
  'Prestige',
  'ProLink',
  'Proform',
  'Promate',
  'Prorack',
  'Purity',
  'QCY',
  'REDLINE',
  'Rapoo',
  'Razer',
  'Redragon',
  'Relax and Spin Tone',
  'Remax',
  'Remington',
  'Ricoh',
  'Rifland',
  'Rivacase',
  'Riversong',
  'Rock House',
  'Rockrose',
  'Rockstar',
  'Rode',
  'Roman',
  'Rossmax',
  'Rovo',
  'Rozia',
  'Rush Brush',
  'S Smart',
  'SBS',
  'SEM',
  'SPEEDLINK',
  'SSmart',
  'Sades',
  'Samsung',
  'Sandisk',
  'Sary',
  'Sasho',
  'Saturn',
  'Seagate',
  'Senator',
  'Sencor',
  'Sennheiser',
  'Sharp',
  'Silicon Power',
  'Sinbo',
  'Skyworth',
  'Smart',
  'Smeg',
  'Smile',
  'Sndway',
  'Sokany',
  'Solac',
  'Sollatek',
  'Sonai',
  'Sonar',
  'Sonashi',
  'Sonifer',
  'Sonoff',
  'Sony',
  'Soul',
  'Speed',
  'Spirit Of Gamer',
  'Sprint',
  'Spy',
  'Squarc Cnix',
  'Star com',
  'Starget',
  'Stiebel Eltron',
  'Strong',
  'Syinix',
  'TCL',
  'TP-Link',
  'Tank',
  'Targus',
  'Taurus',
  'Technical Controls',
  'Techno Zone',
  'Tefal',
  'Televas',
  'Tenda',
  'Thrive',
  'Tiger',
  'Tingwode',
  'Tornado',
  'Toshiba',
  'Total',
  'Touch',
  'Touch El Zenouky',
  'Transcend',
  'Tronsmart',
  'Truman',
  'Turboair',
  'ULTRA',
  'Ubiquiti',
  'Ubisoft',
  'Ultimate Ears',
  'Ultralife',
  'Unionaire',
  'Unitex',
  'Universal',
  'Uno',
  'VAIO',
  'VGR',
  'VT',
  'Veito',
  'Venezia',
  'Vidvie',
  'View',
  'View Sound',
  'ViewSonic',
  'Vire',
  'Vivo',
  'WIWU',
  'Wacaco',
  'Wahl',
  'Waves',
  'Wax Way',
  'Weifeng',
  'Western Digital',
  'White Point',
  'White Shark',
  'White Whale',
  'Wild Wolf',
  'X-tech',
  'XO',
  'XPG',
  'Xenon',
  'Xiaomi',
  'Xiizone',
  'Xprinter',
  'Xzone',
  'Yes-Original',
  'Yison',
  'Yookie',
  'ZYC1',
  'Zada',
  'Zahran',
  'Zanussi',
  'Zebra',
  'Zero',
  'Zhiyun',
  'frrep',
  'i-Cook Built-in',
  'iCone',
  'iKU',
  'iTel',
  'realme',
  'yk design',
  'Sky Data Show',
]
export default ProductSearchResult;