import FlexBox from "components/FlexBox";
import Login from "components/sessions/Login";
import React from "react";
import AppLayout from "components/layout/AppLayout";


const LoginPage = () => {
  return <AppLayout>

  <FlexBox flexDirection="column" minHeight="100vh" alignItems="center" justifyContent="center">
      <Login />
    </FlexBox>
  </AppLayout>
    ;
};

export default LoginPage;